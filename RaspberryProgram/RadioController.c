#define F_CPU 7150900L

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include "SoftUART.h"

// this code sets up counter1 for an 4kHz, 10bit, Phase Corrected PWM
// @ 16Mhz Clock
#define F_I2C 50000UL
#define TWBR_VALUE (((F_CPU) / (F_I2C) - 16) / 2)
#define RTC_RESET_POINTER   0xFF
#define DS1307_ADR  104

/* ������ ��������� */
#define FOSC F_CPU // Clock Speed
#define BAUD 9600  // Baud Rate
#define MYUBRR FOSC/16/BAUD-1

/* ��� ������ � ����� ������������ */
#define BUFFER_LENGTH 0x0B
#define FUNCTION_READ_CODE  0x03
#define FUNCTION_WRITE_CODE 0x0A
#define FUNCTION_GROUP_CODE 0x0F
#define READ_PACKET_LENGTH  0x07
#define WRITE_PACKET_LENGTH 0x0B
#define CODE_FUNCTION_INDEX 0x02

#define Hi(Int) (char) (Int>>8)
#define Low(Int) (char) (Int)

/* Error Codes */
#define BAD_REQUEST 66
#define BAD_CRC  67

#if ((TWBR_VALUE > 255) || (TWBR_VALUE == 0))
#error "TWBR value is not correct"
#endif

/*********       EEPROM Registers      *********/
#define EEPROM_BRIGHTNES					0x00
#define EEPROM_STATUS						0x01
#define EEPROM_LATITUDE						0x02
#define EEPROM_LONGITUDE					0x06
#define EEPROM_TIME							0x0A
#define EEPROM_ILLUMINATIOIN				0x0C
#define EEPROM_MONDAY_ENABLE_TIME			0x10
#define EEPROM_MONDAY_DISABLE_TIME			0x12
#define EEPROM_MONDAY_BRIGHTNESS_VALUE		0x14
#define EEPROM_TUEDAY_ENABLE_TIME			0x15
#define EEPROM_TUEDAY_DISABLE_TIME			0x17
#define EEPROM_TUEDAY_BRIGHTNESS_VALUE		0x19
#define EEPROM_WEDNESDAY_ENABLE_TIME		0x1A
#define EEPROM_WEDNESDAY_DISABLE_TIME		0x1C
#define EEPROM_WEDNESDAY_BRIGHTNESS_VALUE	0x1E
#define EEPROM_THURSDAY_ENABLE_TIME			0x1F
#define EEPROM_THURSDAY_DISABLE_TIME		0x21
#define EEPROM_THURSDAY_BRIGHTNESS_VALUE	0x23
#define EEPROM_FRIDAY_ENABLE_TIME			0x24
#define EEPROM_FRIDAY_DISABLE_TIME			0x26
#define EEPROM_FRIDAY_BRIGHTNESS_VALUE		0x28
#define EEPROM_SATURDAY_ENABLE_TIME			0x29
#define EEPROM_SATURDAY_DISABLE_TIME		0x2B
#define EEPROM_SATURDAY_BRIGHTNESS_VALUE	0x2D
#define EEPROM_SUNDAY_ENABLE_TIME			0x2E
#define EEPROM_SUNDAY_DISABLE_TIME			0x30
#define EEPROM_SUNDAY_BRIGHTNESS_VALUE		0x32
#define EEPROM_EVERY_DAY_ENABLE_TIME		0x33
#define EEPROM_EVERY_DAY_DISABLE_TIME		0x35
#define EEPROM_EVERY_DAY_BRIGHTNESS_VALUE	0x37
#define EEPROM_CUSTOM_ENABLE_TIME1			0x38
#define EEPROM_CUSTOM_ENABLE_TIME2			0x3A
#define EEPROM_CUSTOM_ENABLE_TIME3			0x3C
#define EEPROM_CUSTOM_ENABLE_TIME4			0x3E
#define EEPROM_CUSTOM_ENABLE_TIME5			0x40
#define EEPROM_CUSTOM_DISABLE_TIME1			0x42
#define EEPROM_CUSTOM_DISABLE_TIME2			0x44
#define EEPROM_CUSTOM_DISABLE_TIME3			0x46
#define EEPROM_CUSTOM_DISABLE_TIME4			0x48
#define EEPROM_CUSTOM_DISABLE_TIME5			0x4A
#define EEPROM_CUSTOM_BRIGHTNESS_VALUE1		0x4C
#define EEPROM_CUSTOM_BRIGHTNESS_VALUE2		0x4D
#define EEPROM_CUSTOM_BRIGHTNESS_VALUE3		0x4E
#define EEPROM_CUSTOM_BRIGHTNESS_VALUE4		0x4F
#define EEPROM_CUSTOM_BRIGHTNESS_VALUE5		0x50
#define EEPROM_ID							0x51
#define EEPROM_GROUP_ID1					0x53
#define EEPROM_GROUP_ID2					0x54
#define EEPROM_GROUP_ID3					0x55
#define EEPROM_GROUP_ID4					0x56
#define EEPROM_GROUP_ID5					0x57
#define EEPROM_GROUP_ID6					0x58
#define EEPROM_GROUP_ID7					0x59
#define EEPROM_GROUP_ID8					0x5A
#define EEPROM_GROUP_ID9					0x5B
#define EEPROM_GROUP_ID10					0x5C

/*********      Commands Registers     *********/
#define ON_OFF_REGISTER						0x5D
#define BRIGHTNES_REGISTER				    0x5F
#define SET_ID_REGISTER						0x5E
#define TIME_REGISTER						0x60
#define GPS_REGISTER						0x61
#define ILLUMINATION_REGISTER				0x62
#define ADD_LAMP_TO_GROUP_REGISTER			0x63
#define DELETE_LAMP_FROM_GROUP_REGISTER		0x64

uint8_t error_code = 0;
uint8_t state = 1;

// Packets for sending to raspberry - rtc, serial, adc, pwm, state, gps
unsigned char rtc_packet[]    = { '#', 'R', 'T', 'C', '-', ' ', ' ', '-', ' ', ' ', '/', ' ', ' ', '/', ' ', ' ', '/', ' ', ' ', '/', ' ', ' ', '/', ' ', ' ', '/', ' ', ' ', '&' };
unsigned char serial_packet[] = { '#', 'I', 'D', '-', ' ', ' ', '-', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '&'};
unsigned char adc_packet[]    = { '#', 'A', 'D', 'C', '-', ' ', ' ', '-', ' ', ' ', ' ', '&' };
unsigned char pwm_packet[]    = { '#', 'P', 'W', 'M', '-', ' ', ' ', '-', ' ', '&'};
unsigned char state_packet[]  = { '#', 'S', 'T', 'A', 'T', 'E', '-', ' ', ' ', '-', ' ', '&'};
unsigned char gps_packet[]    = { '#', 'G', 'P', 'S',  '-', ' ', ' ', '-', /* add for coordinates */ '&'};

//Variables for working with radio
unsigned char buffer[11];
unsigned char group_ids[10] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

uint16_t ID = 0;
uint8_t buffer_index = 0;
uint8_t buffer_length = 0; // buffer index for getting next element, break_counter for geting out from loop
uint8_t error_count = 0;
uint8_t command_code = 0;

char mychar[4];
struct gps_struct gps;

struct rtc_struct
{
	uint8_t day ;
	uint8_t year;
	uint8_t month;
	uint8_t date;
	uint8_t hour;
	uint8_t min;
	uint8_t sec;
};

struct gps_struct
{
	char* latitude;
	char* longitude;
};

//������������ ��������� ����������
ISR(USART_RXC_vect)
{
	uint8_t byte_read = UDR;
	{
		if (!((buffer_index == 0 || buffer_index == 1) && (byte_read == 0x0a ||  byte_read == 0x0d)))
		{
			buffer[buffer_index] = byte_read;
			buffer_index++;
		}
	}
}

void USART_Init(unsigned int ubrr)
{
	/*Set baud rate */
	UBRRH = (unsigned char)(ubrr >> 8);
	UBRRL = (unsigned char)ubrr;

	/* Enable receiver and transmitter */
	UCSRB = (1 << RXEN) | (1 << TXEN);

	UCSRB |= (1 << RXCIE); // Enable the USART Recieve Complete interrupt (USART_RXC)

	/* Set frame format: 8data, 1stop bit */
	UCSRC = (1 << URSEL) | (1 << UCSZ1) | (1 << UCSZ0);
}

void USART_Transmit(unsigned char data) //������� �������� ������
{
	while (!(UCSRA & (1 << UDRE))); //�������� ����������� ������ ������
	UDR = data; //������ �������� ������
}

unsigned char USART_Receive(void)
{
	/* Wait for data to be received */
	while (!(UCSRA & (1 << RXC)));
	/* Get and return received data from buffer */
	return UDR;
}

void USART_Flush(void)
{
	unsigned char dummy;
	while (UCSRA & (1 << RXC)) dummy = UDR;
}


void send_string(unsigned char s[]) // Send string to Radio module
{
	uint8_t i = 0;
	while (s[i] != 0x00)
	{
		USART_Transmit(s[i]);
		i++;
	}
}

unsigned short CalcCRC16(unsigned char* data, int length)
{
	unsigned short crc = 0x0000;
	for (int i = 0; i < length; i++)
	{
		crc ^= (unsigned short)(data[i] << 8);
		for (int j = 0; j < 8; j++)
		{
			if ((crc & 0x8000) > 0)
			crc = (unsigned short)((crc << 1) ^ 0x8005);
			else
			crc <<= 1;
		}
	}
	return crc;
}

void Init_GPIO() // PINS Initialization
{
	PORTB |= (1 << PINB0);
	DDRB  |= (1 << PINB1) | (1 << PINB0);
	DDRD |=(1 << PIND3) | (1 << PIND1) | (1 << PIND2);
	DDRC |= (1 <<PINC2);
	PORTD |= (1 << PIND0);
}

void set_brightness(unsigned char percents)
{
	volatile uint8_t  y = percents;
	
	float c = ((y / 10.0 )/ 9.3);
	volatile double k = (34.3028 * pow((double)c, 7) - 136.8364 * pow((double)c, 6) + 225.0255 * pow((double)c, 5) - 198.3830 * pow((double)c, 4) + 102.8863 * pow((double)c, 3) - 33.0504 * pow((double)c, 2) + 7.0759 * c - 0.0192) * 100;
	
	int temp = 0;

	if ((((long)k * 10)) > 1023) temp = 1023;
	else temp = (long)k * 10;

	if (percents == 0) OCR1A = 1023;
	else OCR1A = 1023 - temp;

	write_to_eeprom((void*)&percents, EEPROM_BRIGHTNES, 1);
}

void PWM_Init()
{
	DDRB |= (1 << PINB1);// is now an output

	TCCR1A |= (1 << COM1A1); // set none-inverting mode
	TCCR1A |= (1 << WGM11) | (1 << WGM10); // set 10bit phase corrected PWM Mode
	
	TCCR1B |= (1 << CS10); // set prescaler to 8 and starts PWM
}

void RTC_Init(void)
{
	TWBR = TWBR_VALUE;
	TWSR = 0;
}

uint8_t RTC_Get()
{
	uint8_t data;

	/* ��������� ��������� ����� */
	TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
	while(!(TWCR & (1 << TWINT)));

	/* ������ �� ���� ����� SLA-R */
	TWDR = (DS1307_ADR << 1) | 1;
	TWCR = (1 << TWINT) | (1 << TWEN);
	while(!(TWCR & (1 << TWINT)));

	/* ��������� ������ */
	TWCR = (1 << TWINT)|(1 << TWEN);
	while (!(TWCR & (1 << TWINT)));

	data = TWDR;

	/* ��������� ��������� ���� */
	TWCR = (1 << TWINT) | (1 << TWSTO)|(1 << TWEN);

	return data;
}

uint8_t bcd_high(int bcd)
{
	return (uint8_t)(bcd >> 4);
}

uint8_t bcd_low(int bcd)
{
	return (uint8_t)(bcd & 0xF);
}

void RTC_Set(uint8_t adr, uint8_t data)
{
	/* ��������� ��������� ����� */
	TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
	while(!(TWCR & (1 << TWINT)));

	/* ������ �� ���� ����� SLA-W */
	TWDR = (DS1307_ADR << 1) | 0;
	TWCR = (1 << TWINT)|(1 << TWEN);
	while(!(TWCR & (1 << TWINT)));

	/* �������� ����� �������� ds1307 */
	TWDR = adr;
	TWCR = (1 << TWINT)|(1 << TWEN);
	while(!(TWCR & (1 << TWINT)));

	/* �������� ������ ��� ���������� */
	if (data != RTC_RESET_POINTER)
	{
		/* ��� ����� �������� ������ � BCD ������� */
		data = ((data / 10) << 4) + data % 10;
		
		TWDR = data;
		TWCR = (1 << TWINT) | (1 << TWEN);
		while(!(TWCR & (1 << TWINT)));
	}

	/* ��������� ��������� ���� */
	TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN);
}


void RTC_SET(uint8_t sec, uint8_t min, uint8_t hour, uint8_t day, uint8_t date, uint8_t month, uint8_t year)
{
	RTC_Set(0, RTC_RESET_POINTER);
	_delay_ms(1);

	RTC_Set(0, sec);
	_delay_ms(1);

	RTC_Set(1, min);
	_delay_ms(1);

	RTC_Set(2, hour);
	_delay_ms(1);

	RTC_Set(3, day);
	_delay_ms(1);

	RTC_Set(4, date);
	_delay_ms(1);

	RTC_Set(5, month);
	_delay_ms(1);

	RTC_Set(6, year);
	_delay_ms(1);
}

struct rtc_struct RTC_GET()
{
	struct rtc_struct time;
	
	RTC_Set(0, RTC_RESET_POINTER);
	_delay_ms(1);

	time.sec = RTC_Get();
	time.min = RTC_Get();
	time.hour  = RTC_Get();
	time.day   = RTC_Get();
	time.date  = RTC_Get();
	time.month = RTC_Get();
	time.year  = RTC_Get();
	_delay_ms(1);

	return time;
}


void adc_init()
{
	ADMUX = (1 << REFS0);    // AVCC with external capacitor at AREF pin, set ADC channel 0
	ADCSRA = (1 << ADEN)  | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);   //ADEN=1 Conversion, ADFR=0, ADIF=0, Prescalar=128 Slow for Voltage Measurement
}

void int_to_char_array(int val)
{
	uint8_t i = 3;
	while (val != 0)
	{
		mychar[i] = (val % 10) + 0x30;
		val = val / 10;
		i--;
	}

	if (i >= 3) return;

	for (uint8_t j = 0; j <= i; j++)
	{
		mychar[j] = 0x30;
	}
}

int get_illumination()
{
	ADCSRA |= (1 << ADSC); // Start conversion
	while (ADCSRA & (1 << ADSC)); // wait for conversion to complete
	ADCSRA |= (1 << ADIF); //Clear ADIF Flag

	int_to_char_array(ADC);
	return ADC;
}

void Init()
{
	Init_GPIO(); // init I/O Ports
	RTC_Init(); // init Real Time Clock
	USART_Init(MYUBRR); // init USART
	PWM_Init(); // init PWM
	adc_init(); // init ADC
	softuart_init(); // init SUART
	sei(); // allow global interrupts

	uint8_t value = 0;
	read_from_eeprom(&value, EEPROM_BRIGHTNES, 1);
	if (value != 0xFF) set_brightness(value);
	else set_brightness(0);
}

void write_to_eeprom(void* data, uint8_t reg, uint8_t size)
{
	eeprom_write_block(data, reg, size);
}

void read_from_eeprom(void* data, uint8_t reg, uint8_t size)
{
	eeprom_read_block(data, reg, size);
}

void send_cmd(char cmd[])
{
	send_string(cmd);
	USART_Transmit(0x0D);
	USART_Transmit(0x0A);
}

void send_in_api_mode(char* cmd)
{
	uint8_t sendOk = 0;
	uint8_t tries = 0;

	while (!sendOk)
	{
		send_cmd("+++");
		_delay_ms(1000);
		send_cmd("ATAP 1");
		_delay_ms(1000);
		send_cmd("ATRO 100");
		_delay_ms(1000);
		send_cmd("ATCN");
		_delay_ms(1000);
		send_cmd("");
		//_delay_ms(200);

		USART_Flush();
		clear_buffer();

		for (uint8_t i = 0; cmd[i] != '&'; i++)
		{
			if (cmd[i] != 0x0A && cmd[i] != 0x0D && cmd[i] != ' ') USART_Transmit(cmd[i]);
		}
		USART_Transmit('&');
		_delay_ms(4000);
		
		for (uint8_t i = 0; i < BUFFER_LENGTH; i++)
		{
			if (buffer[i] == 'O' && buffer[i + 1] == 'K')
			{
				sendOk = 1;
				USART_Flush();
				break;
			}
		}

		if (!sendOk) tries = tries + 1;
		if (tries >= 5) hardware_reset();
	}
}

void send_rtc_packet(struct rtc_struct time)
{
	rtc_packet[5] = (ID >> 8);
	rtc_packet[6] = ID;
	rtc_packet[8] = bcd_high(time.date) + 0x30;
	rtc_packet[9] = bcd_low(time.date) + 0x30;
	rtc_packet[11] = bcd_high(time.day) + 0x30;
	rtc_packet[12] = bcd_low(time.day) + 0x30;
	rtc_packet[14] = bcd_high(time.hour) + 0x30;
	rtc_packet[15] = bcd_low(time.hour) + 0x30;
	rtc_packet[17] = bcd_high(time.min) + 0x30;
	rtc_packet[18] = bcd_low(time.min) + 0x30;
	rtc_packet[20] = bcd_high(time.month) + 0x30;
	rtc_packet[21] = bcd_low(time.month) + 0x30;
	rtc_packet[23] = bcd_high(time.sec) + 0x30;
	rtc_packet[24] = bcd_low(time.sec) + 0x30;
	rtc_packet[26] = bcd_high(time.year) + 0x30;
	rtc_packet[27] = bcd_low(time.year) + 0x30;

	send_in_api_mode((char*)rtc_packet);
}

void send_pwm_packet()
{
	uint8_t pwm = 0;
	read_from_eeprom((void*)&pwm, EEPROM_BRIGHTNES, 1);

	pwm_packet[5] = (ID >> 8);
	pwm_packet[6] = ID;
	pwm_packet[8] = pwm;

	send_in_api_mode((char*)pwm_packet);
}

void send_adc_packet(char val[])
{
	adc_packet[5] = (ID >> 8);
	adc_packet[6] = ID;
	adc_packet[8] = mychar[0];
	adc_packet[9] = mychar[1];
	adc_packet[10] = mychar[2];

	send_in_api_mode((char*)adc_packet);
}

void send_crc_packet(uint8_t high, uint8_t low, uint8_t tmp)
{
	unsigned char packet[12];
	packet[0] = '#';
	packet[1] = 'c';
	packet[2] = 'r';
	packet[3] = 'c';
	packet[4] = '-';
	packet[5] = (ID >> 8);
	packet[6] = ID;
	packet[7] = '-';
	packet[8] = tmp;
	packet[9] = high;
	packet[10] = low;
	packet[11] = '&';

	send_in_api_mode((char*)packet);
}

void send_state_packet(uint8_t state)
{
	state_packet[7] = (ID >> 8);
	state_packet[8] = ID;
	state_packet[10] = state + 0x30;

	send_in_api_mode((char*)state_packet);
}

void send_serial_number_packet()
{
	serial_packet[4] = (ID >> 8);
	serial_packet[5] = ID;
	
	send_cmd("+++");
	_delay_ms(1000);

	send_cmd("ATSH");
	clear_buffer();
	_delay_ms(1000);

	uint8_t k = 7;
	for (uint8_t i = 0; i < 11; i++)
	{
		if ((buffer[i] >= 0x30 && buffer[i] < 0x3A) || (buffer[i] >= 'A' || buffer[i] <= 'Z')) serial_packet[k++] = buffer[i];
	}

	send_cmd("ATSL");
	clear_buffer();
	_delay_ms(1000);

	for (uint8_t i = 0; i < 11; i++)
	{
		if ((buffer[i] >= 0x30 && buffer[i] < 0x3A) || (buffer[i] >= 'A' || buffer[i] <= 'Z')) serial_packet[k++] = buffer[i];
	}
	serial_packet[27] = '&';
	
	send_cmd("ATCN");
	_delay_ms(1000);

	send_in_api_mode(serial_packet);
}

int group_existing(uint8_t group_id)
{
	for (uint8_t i = 0; i < 10; i++)
	{
		if (group_ids[i] == group_id)
		{
			return 1;
		}
	}
	return 0;
}

void add_lamp_to_group(uint8_t group_id)
{
	if (group_existing(group_id)) return;

	for (uint8_t i = 0; i < 10; i++)
	{
		if (group_ids[i] == 0xFF)
		{
			group_ids[i] = group_id;
			break;
		}
	}
	write_to_eeprom((void*)&group_ids, EEPROM_GROUP_ID1, 10);
}

void delete_lamp_from_group(uint8_t group_id)
{
	for (uint8_t i = 0; i < 10; i++)
	{
		if (group_ids[i] == group_id)
		{
			group_ids[i] = 0xFF;
		}
	}
	write_to_eeprom((void*)&group_ids, EEPROM_GROUP_ID1, 10);
}

struct gps_struct read_gps()
{
	uint8_t readFlag = 0;

	while (!readFlag)
	{
		char gps_buffer[128];

		for (uint8_t i = 0; i < 128; i++) // clear softUart buffer
		{
			gps_buffer[i] = '\0';
		}

		char buf = softuart_getchar();

		while (buf != '$') // while start frame of packet didn't come
		{
			buf = softuart_getchar();
		}

		uint8_t i = 0;
		gps_buffer[i++] = buf;
		
		while (buf != '*') // read to the end of the packet
		{
			buf = softuart_getchar();
			if ((buf >= 33 && buf <= 126) || (buf == 0x0A || buf == 0x0D)) // validate input data
			{
				if (i == 256) i = 0;
				gps_buffer[i++] = buf;
				//USART_Transmit(buf);
			}
		}
		gps_buffer[i++] = buf;
		_delay_ms(100);

		i = 0;
		for (; gps_buffer[i] != '*'; i++)
		{
			USART_Transmit(gps_buffer[i]);
		}
		USART_Transmit(0x0a);
		USART_Transmit(0x0d);

		uint8_t  k = 0, m = 0, j = 0, l = 0, commaCount = 0;
		i = 0;

		for (; gps_buffer[i] != '*'; i++) // trying to parse received buffer to gps coordinates
		{
			if (i == 125) break;
			//USART_Transmit(gps_buffer[i]);

			if (gps_buffer[i] == 'G' && gps_buffer[i + 1] == 'G' && gps_buffer[i + 2] == 'A') // if we receive GGA ( Global Positioning System Fixed Data) packet
			{
				//USART_Transmit(gps_buffer[i]);
				while (gps_buffer[i] != '*') // while not the end of the frame
				{
					if (i == 125) break;

					if (gps_buffer[i] == ',') commaCount++;

					if (commaCount == 2) // third parameter in message is Latitude
					{
						k = i + 1;
						i = i + 1;

						while (gps_buffer[i] != ',') { i++; }
						if (i != k) m = i - 1;
						else m = i;

						int size = m - k + 1;

						commaCount += 2;

						if (size > 2)
						{
							char longitude[size + 1];
							for (j = k, l = 0; j < m + 1; j++, l++) longitude[l] = gps_buffer[j];
							longitude[size] = gps_buffer[i + 1];
							i += 3;

							for (int i = 0; i < size + 1; i++) USART_Transmit(longitude[i]);
							USART_Transmit(0x0a);
							USART_Transmit(0x0d);
						}
					}

					if (commaCount == 4) // fifth parameter in message is Longitude
					{
						k = i;

						while (gps_buffer[i] != ',') { i++; }
						m = i - 1;
						int size = m - k + 1;

						commaCount += 2;

						if (size > 2)
						{
							char latitude[size + 1];
							for (j = k, l = 0; j < m + 1; j++, l++) latitude[l] = gps_buffer[j];
							latitude[size] = gps_buffer[i + 1];
							i += 3;

							for (int i = 0; i < size + 1; i++) USART_Transmit(latitude[i]);
							USART_Transmit(0x0a);
							USART_Transmit(0x0d);
						}
					}
					i++;
				}
			}
		}
	}
	return gps;
}

void clear_buffer()
{
	for (uint8_t i = 0; i < BUFFER_LENGTH; i++) buffer[i] = 0;
	buffer_index = 0;
	buffer_length = 0;
}

void hardware_reset()
{
	PORTB &= ~(1 << PORTB0);
	_delay_ms(1000);
	PORTB |= (1 << PORTB0);
}

int main(void)
{
	//write_to_eeprom((void*)&ID, EEPROM_ID, 0xFFFF);
	read_from_eeprom((void*)&ID, EEPROM_ID, 2); // read device ID
	read_from_eeprom((void*)&group_ids, EEPROM_GROUP_ID1, 10); // read all groups
	uint8_t last_state = 0;
	
	uint8_t brightness = 0;
	int sleep_time = 1000;
	
	for (uint8_t i = 0; i < 100; i++) // delay for get all devices ready
	{
		_delay_ms(700);
	}

	/*  For testing every day mode  */
	/*uint8_t time_enable[] = { 14, 20 };
	uint8_t time_disable[] = { 14, 21 };
	uint8_t brigh = 24;
	uint8_t m = 2;
	write_to_eeprom(&m, EEPROM_STATUS, 1);
	write_to_eeprom(time_enable, EEPROM_EVERY_DAY_ENABLE_TIME, 2);
	write_to_eeprom(time_disable, EEPROM_EVERY_DAY_DISABLE_TIME, 2);
	write_to_eeprom(&brigh, EEPROM_EVERY_DAY_BRIGHTNESS_VALUE, 1);*/
	
	/*  For testing every day custom mode  */
	/*uint8_t time_enable[] = { 16, 0, 16, 2, 16, 4, 16, 6, 16, 8};
	uint8_t time_disable[] = { 16, 1, 16, 3, 16, 5, 16, 7, 16, 9};
	uint8_t brigh[] = { 10, 20, 30, 40, 50 };
	uint8_t m = 4;
	write_to_eeprom(&m, EEPROM_STATUS, 1);
	write_to_eeprom(time_enable, EEPROM_CUSTOM_ENABLE_TIME1, 10);
	write_to_eeprom(time_disable, EEPROM_CUSTOM_DISABLE_TIME1, 10);
	write_to_eeprom(brigh, EEPROM_CUSTOM_BRIGHTNESS_VALUE1, 5);*/

	/* For testing custom day mode */
	/*uint8_t thursday_time_enable[] = { 17, 21};
	uint8_t thursday_time_disable[] = { 17, 22};
	uint8_t thursday_brigh = 100;
	uint8_t m = 6;
	write_to_eeprom(&m, EEPROM_STATUS, 1);
	write_to_eeprom(thursday_time_enable, EEPROM_THURSDAY_ENABLE_TIME, 10);
	write_to_eeprom(thursday_time_disable, EEPROM_THURSDAY_DISABLE_TIME, 10);
	write_to_eeprom(&thursday_brigh, EEPROM_THURSDAY_BRIGHTNESS_VALUE, 5);*/
	
	while(1)
	{
		switch (state)
		{
			case 1: // ��������� �������������
			{
				Init();

				if (ID == 0xFFFF) send_serial_number_packet(); 
				state = 2;
				last_state = 1;
			} break;
			case 2: // ��������� ��������
			{
				USART_Flush();
				clear_buffer();
				sleep_time = 1000;
				state = 3;
				last_state = 2;
			} break;
			case 3: // ��������� ������
			{
				if ((buffer_index >= BUFFER_LENGTH) && (buffer_length == 0))
				{
					if ((int)buffer[CODE_FUNCTION_INDEX] == FUNCTION_READ_CODE)
					{
						buffer_length = READ_PACKET_LENGTH;
					}
					else if ((int)buffer[CODE_FUNCTION_INDEX] == FUNCTION_WRITE_CODE)
					{
						buffer_length = WRITE_PACKET_LENGTH;
					}
					else if ((int)buffer[CODE_FUNCTION_INDEX] == FUNCTION_GROUP_CODE)
					{
						buffer_length = WRITE_PACKET_LENGTH;
					}
					else
					{
						error_code = BAD_REQUEST;
						state = 13;
						last_state = 3;
						break;
					}
				}

				if ((buffer_index >= buffer_length) && (buffer_length > 0))
				{
					state = 10;
					buffer_index = 0; // reset for next packet
					last_state = 3;
				}
			} break;
			case 4: // ��������� ��������
			{
				switch(command_code)
				{
					case BAD_REQUEST :
					{
						sleep_time = 1000;
						state = 11;
					} break;
					case BAD_CRC :
					{
						sleep_time = 1000;
						state = 11;
					} break;
					last_state = 4;
				}
			} break;
			case 5: // ��������� ������ RTC
			{
				struct rtc_struct time = RTC_GET();
				send_rtc_packet(time);
				state = 11;
				last_state = 5;
			} break;
			case 6: // ��������� ��������� �������
			{
				set_brightness(brightness);
				state = 11;
				last_state = 6;
			} break;
			case 7: // ��������� ������ RTC
			{
				last_state = 7;
			} break;
			case 8: // ��������� ������ GPS ���������
			{
				last_state = 8;
			} break;
			case 9: // ��������� ������ ������������
			{
				get_illumination();
				send_adc_packet(mychar);
				last_state = 9;
				state = 11;
			} break;
			case 10: // ��������� ��������� ������
			{
				last_state = 10;
				unsigned char crcOK = 0;
				unsigned short crc2 = CalcCRC16(buffer, buffer_length - 2);
				unsigned char crcHi = Hi(crc2);
				unsigned char crcLow = Low(crc2);
				
				if ((buffer[CODE_FUNCTION_INDEX] == FUNCTION_READ_CODE) && (buffer_length == READ_PACKET_LENGTH)) // read packet
				{
					if ((crcHi == buffer[READ_PACKET_LENGTH - 2]) && (crcLow == buffer[READ_PACKET_LENGTH - 1])) // if crc compares
					crcOK = 1;
				}
				if ((buffer[CODE_FUNCTION_INDEX] == FUNCTION_WRITE_CODE) && (buffer_length == WRITE_PACKET_LENGTH)) // write packet
				{
					if ((crcHi == buffer[WRITE_PACKET_LENGTH - 2]) && (crcLow == buffer[WRITE_PACKET_LENGTH - 1])) // if crc compares
					crcOK = 1;
				}
				if ((buffer[CODE_FUNCTION_INDEX] == FUNCTION_GROUP_CODE) && (buffer_length == WRITE_PACKET_LENGTH)) // packet for group
				{
					if ((crcHi == buffer[WRITE_PACKET_LENGTH - 2]) && (crcLow == buffer[WRITE_PACKET_LENGTH - 1])) // if crc compares
					crcOK = 1;
				}

				if (crcOK)
				{
					uint16_t recID =  ((buffer[0] << 8) | buffer[1]);
					
					// ID == recID mean that command is sended for our lamp
					//recID == 0 mean that it is sended command to setID (we haven't ID yet)
					// group_existing(recID) mean that it is command for group
					if (ID == recID || recID == 0 || group_existing(recID))
					{
						unsigned char buf[11];
						for (int n = 0; n < 11; n++) buf[n] = buffer[n]; // copy buffer
						
						char arr[4];
						arr[0] = (WRITE_PACKET_LENGTH - 3);
						arr[1] = crcHi;
						arr[2] = crcLow;
						arr[3] = '&';
						send_in_api_mode((char*)arr);
						_delay_ms(1000);

						short regAddress = 0;
						regAddress = ((buf[3] << 8) | buf[4]);

						if ((buf[CODE_FUNCTION_INDEX] == FUNCTION_WRITE_CODE) && (regAddress == BRIGHTNES_REGISTER)) // command to change brightness of the lamp
						{
							brightness = buf[WRITE_PACKET_LENGTH - 3];
							state = 6;
						}
						else if ((buf[CODE_FUNCTION_INDEX] == FUNCTION_WRITE_CODE) && (regAddress == ON_OFF_REGISTER)) // command to on/off lamp
						{
							uint8_t value = buf[WRITE_PACKET_LENGTH - 3];

							if (value == 1) PORTD |= (1 << PORTD2);
							if (value == 0) PORTD &= !(1 << PORTD2);
							state = 11;
						}
						else if ((buf[CODE_FUNCTION_INDEX] == FUNCTION_WRITE_CODE) && (regAddress == SET_ID_REGISTER)) // command to set up id
						{
							uint16_t radioXor = 0;
							for (uint8_t i = 7; serial_packet[i] != '&'; i++)
							{
								if (serial_packet[i] != 0x0A && serial_packet[i] != 0x0D && serial_packet[i] != ' ')
								{
									radioXor += serial_packet[i];
								}
							}
							
							uint16_t receivedSerial = (buf[WRITE_PACKET_LENGTH - 5] << 8) | buf[WRITE_PACKET_LENGTH - 6];
							
							if (radioXor == receivedSerial)
							{
								ID = (buf[WRITE_PACKET_LENGTH - 4] << 8) | buf[WRITE_PACKET_LENGTH - 3];
								write_to_eeprom((void*)&ID, EEPROM_ID, 2);
							}
							state = 11;
						}
						else if ((buf[CODE_FUNCTION_INDEX] == FUNCTION_WRITE_CODE) && (regAddress == ADD_LAMP_TO_GROUP_REGISTER)) // command to add lamp to group
						{
							uint8_t group_id = buf[WRITE_PACKET_LENGTH - 3];
							add_lamp_to_group(group_id);
							state = 11;
						}
						else if ((buf[CODE_FUNCTION_INDEX] == FUNCTION_WRITE_CODE) && (regAddress == DELETE_LAMP_FROM_GROUP_REGISTER)) // command to delete lamp from group
						{
							uint8_t group_id = buf[WRITE_PACKET_LENGTH - 3];
							delete_lamp_from_group(group_id);
							state = 11;
						}
						else if ((buf[CODE_FUNCTION_INDEX] == FUNCTION_READ_CODE) && (regAddress == TIME_REGISTER)) // get rtc's values
						{
							state = 5;
						}
						else if ((buf[CODE_FUNCTION_INDEX] == FUNCTION_READ_CODE) && (regAddress == BRIGHTNES_REGISTER)) // getPWM_Value
						{
							send_pwm_packet();
							state = 11;
						}
						else if ((buf[CODE_FUNCTION_INDEX] == FUNCTION_READ_CODE) && (regAddress == ON_OFF_REGISTER))  // getLampState (PIND2 Value)
						{
							uint8_t status = (PIND & (1 << PIND2)) >> PIND2;
							send_state_packet(status);
							state = 11;
						}
						else if ((buf[CODE_FUNCTION_INDEX] == FUNCTION_READ_CODE) && (regAddress == ILLUMINATION_REGISTER)) // getIllumination (ADC Value)
						{
							state = 9;
						}
						else if ((buf[CODE_FUNCTION_INDEX] == FUNCTION_READ_CODE) && (regAddress == GPS_REGISTER))  // getCoordinates of the lamp
						{
							state = 8;
						}
						else if ((buf[CODE_FUNCTION_INDEX] == FUNCTION_GROUP_CODE) && (regAddress == BRIGHTNES_REGISTER))  // command to change brightness for group
						{
							brightness = buf[WRITE_PACKET_LENGTH - 3];
							state = 6;
						}
						else if ((buf[CODE_FUNCTION_INDEX] == FUNCTION_GROUP_CODE) && (regAddress == ON_OFF_REGISTER))  // command to on/off group
						{
							uint8_t value = buf[WRITE_PACKET_LENGTH - 3];

							if (value == 1) PORTD |= (1 << PORTD2);
							if (value == 0) PORTD &= !(1 << PORTD2);
							state = 11;
						}
						else state = 11;
					}
					else state = 11;
				}
				else
				{
					error_code = BAD_CRC;
					state = 13;
				}
			}
			break;
			case 11: // ��������� ���
			{
				while(sleep_time > 0)
				{
					_delay_ms(100);
					sleep_time -= 100;
				}
				
				state = 2;
				last_state = 11;
			} break;
			case 12: // ��������� ��������� ���������� �������
			{
				last_state = 12;
			} break;
			case 13: // ��������� ��������� ������
			{
				error_count = error_count + 1;
				USART_Flush();
				clear_buffer();

				switch (error_code)
				{
					case BAD_REQUEST:
					{
						command_code = BAD_REQUEST;
						state = 4;
						break;
					}
					case BAD_CRC:
					{
						command_code = BAD_CRC;
						state = 4;
						break;
					}
				}
				last_state = 13;
			} break;
			case 14: // ��������� ����������� ������
			{
				last_state = 14;
			} break;
			case 15: // ��������� ��������� ���������� �� UART
			{
				last_state = 15;
			} break;
			case 16: // ��������� ���������� ������
			{
				struct rtc_struct time;
				uint8_t mode = 0;
				
				uint8_t hour_enable = 0, min_enable = 0;
				uint8_t hour_disable = 0, min_disable = 0;
				uint8_t brigh = 0;

				read_from_eeprom((void*)&mode, EEPROM_STATUS, 1);
				mode &= 0x06; // get mode by multiplying om mask
				mode = mode >> 1;

				switch(mode)
				{
					case 1 : // ���������� �����
					{
						read_from_eeprom((void*)&hour_enable, EEPROM_EVERY_DAY_ENABLE_TIME, 1);
						read_from_eeprom((void*)&min_enable, EEPROM_EVERY_DAY_ENABLE_TIME + 1, 1);
						read_from_eeprom((void*)&hour_disable, EEPROM_EVERY_DAY_DISABLE_TIME, 1);
						read_from_eeprom((void*)&min_disable, EEPROM_EVERY_DAY_DISABLE_TIME + 1, 1);
						read_from_eeprom((void*)&brigh, EEPROM_EVERY_DAY_BRIGHTNESS_VALUE, 1);

						while (1)
						{
							time = RTC_GET();
							
							uint8_t h = (bcd_high(time.hour)) * 10 + (bcd_low(time.hour));
							uint8_t m = (bcd_high(time.min)) * 10 + (bcd_low(time.min));

							if (m == min_enable && h == hour_enable)
							{
								PORTD |= (1 << PORTD2);
								set_brightness(brigh);
							}
							if (m == min_disable && h == hour_disable)
							{
								PORTD &= !(1 << PORTD2);
								set_brightness(0);
							}
							_delay_ms(5000);
						}
					} break;
					case 2 : // ���������� ������ �����
					{
						uint8_t enable_time[10];
						uint8_t disable_time[10];
						uint8_t bright[5];

						read_from_eeprom((void*)enable_time, EEPROM_CUSTOM_ENABLE_TIME1, 10);
						read_from_eeprom((void*)disable_time, EEPROM_CUSTOM_DISABLE_TIME1, 10);
						read_from_eeprom((void*)bright, EEPROM_CUSTOM_BRIGHTNESS_VALUE1, 5);

						time = RTC_GET();

						uint8_t h = (bcd_high(time.hour)) * 10 + (bcd_low(time.hour));
						uint8_t m = (bcd_high(time.min)) * 10 + (bcd_low(time.min));

						for (uint8_t i = 0; i < 10; i += 2)
						{
							if (h == enable_time[i] && m == enable_time[i + 1])
							{
								PORTD |= (1 << PORTD2);
								set_brightness(bright[i / 2]);
							}
							if (h == disable_time[i] && m == disable_time[i + 1])
							{
								PORTD &= !(1 << PORTD2);
								set_brightness(0);
							}
						}
						_delay_ms(5000);
					} break;
					case 3 : // ������������ �����
					{
						while (1)
						{
							uint8_t day = 0;
							time = RTC_GET();

							day = (bcd_low(time.day));

							read_from_eeprom((void*)&hour_enable, EEPROM_MONDAY_ENABLE_TIME + ((day - 2) * 5), 1);
							read_from_eeprom((void*)&min_enable, EEPROM_MONDAY_ENABLE_TIME + ((day - 2) * 5) + 1, 1);
							read_from_eeprom((void*)&hour_disable, EEPROM_MONDAY_DISABLE_TIME + ((day - 2) * 5), 1);
							read_from_eeprom((void*)&min_disable, EEPROM_MONDAY_DISABLE_TIME + ((day - 2) * 5) + 1, 1);
							read_from_eeprom((void*)&brigh, EEPROM_MONDAY_BRIGHTNESS_VALUE+ ((day - 2) * 5), 1);

							uint8_t h = (bcd_high(time.hour)) * 10 + (bcd_low(time.hour));
							uint8_t m = (bcd_high(time.min)) * 10 + (bcd_low(time.min));

							if (m == min_enable && h == hour_enable)
							{
								PORTD |= (1 << PORTD2);
								set_brightness(brigh);
							}
							if (m == min_disable && h == hour_disable)
							{
								PORTD &= !(1 << PORTD2);
								set_brightness(0);
							}
							_delay_ms(5000);
						}
					} break;
					last_state = 16;
				}
			} break;
		}
	}
}