﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Net.Sockets;
using System.Xml;
using MySql.Data.MySqlClient;

namespace RaspberryLight
{
    public class ServerCommands
    {
        public String command = String.Empty;
        public int priority = 0;
        public int commandsCount = 0;
        public int commandStatus = 0;

        public ServerCommands()
        {
            this.command = String.Empty;
            this.priority = 0;
            this.commandsCount = 0;
            this.commandStatus = 0;
        }

        public ServerCommands(String command, int priority, int commandsCount)
        {
            this.command = command;
            this.priority = priority;
            this.commandsCount = commandsCount;
            this.commandStatus = 0;
        }

        public void setCommandsCount(int commandsCount)
        {
            this.commandsCount = commandsCount;
        }

        public void setCommandStatus(int commandStatus)
        {
            this.commandStatus = commandStatus;
        }
    }

    public class DataParameter
    {
        public String DriverFeature = String.Empty;
        public int EquipmentTypeID = 0;
        public int obisCode = 0;
        public int parameterNumber = 0;
        public int qualityID = 0;
        public DateTime Date = new DateTime();
        public double value = 0;

        public DataParameter()
        {
            DriverFeature = String.Empty;
            EquipmentTypeID = 0;
            obisCode = 0;
            parameterNumber = 0;
            qualityID = 0;
            Date = new DateTime();
            value = 0;
        }

        public DataParameter(String DriverFeature, int EquipmentTypeID, int obisCode, int parameterNumber, int qualityID, DateTime Date, double value)
        {
            this.DriverFeature = DriverFeature;
            this.EquipmentTypeID = EquipmentTypeID;
            this.obisCode = obisCode;
            this.parameterNumber = parameterNumber;
            this.qualityID = qualityID;
            this.Date = Date;
            this.value = value;
        }
    }

    class Program
    {
        // Локально или на малинке
        const int LOCALDEBAG = 0;
        const int USERASPBERRY = 1;
        static int USE_NOW = USERASPBERRY;

        private static SerialPort serial = new SerialPort("/dev/ttyAMA0", 9600, Parity.None, 8); // Com port for work with radio module
        private static int NIK_Address = 0, StationID = 0, Port = 0, EquipmentTypeID = 0; // Read from Configuration.xml
        private static List<ServerCommands> SetCommands = new List<ServerCommands>(); // List of commands that are sended to radio modules
        private static String Host = String.Empty, RadioSerialNumber = String.Empty; // Read from Configuration.xml
        private static List<DataParameter> MainData = new List<DataParameter>(); // List of data that are sended to Server
        private static object commandsLocker = new object(); // lock commands
        private static String radioResetPin = String.Empty; // String value of pin that reset radio module (Read from Configuration.xml)
        private static String RaspberryID = String.Empty; // Read from Configuration.xml - For Repository
        private static object locker = new object(); // locaker for MainData
        private static SerialPort serialModulator; // COM Port for work with modulator
        private static GPIO RadioResetGPIO; // GPIO that resets radio module

        public static List<ServerCommands> SortByPriority(List<ServerCommands> commands)
        {
            commands.Sort((x, y) => x.priority.CompareTo(y.priority)); // Sort commands in List by priority
            return commands;
        }

        static public byte HI(ushort x) // return high byte
        {
            return Convert.ToByte(x >> 8);
        }

        static public byte LO(ushort x) //  return low byte
        {
            return Convert.ToByte(x & 0xFF);
        }

        static public ushort CalcCRC16(byte[] data) // Function for finding crc for packet protocol
        {
            ushort crc = 0x0000;
            for (int i = 0; i < data.Length; i++)
            {
                crc ^= (ushort)(data[i] << 8);
                for (int j = 0; j < 8; j++)
                {
                    if ((crc & 0x8000) > 0)
                        crc = (ushort)((crc << 1) ^ 0x8005);
                    else
                        crc <<= 1;
                }
            }
            return crc;
        }

        public class Nik
        {
            private byte[] receive;

            static byte[] SNRM = { 0x7e, 0xa0, 0x0a, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x93, 0x06, 0x13, 0x7e }; //frame SNRM
            static byte[] AARQ = { 0x7e, 0xa0, 0x50, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x0d, 0x3e, 0xe6, 0xe6, 0x00,
                                   0x60, 0x3f ,0xa1, 0x09, 0x06, 0x07, 0x60, 0x85, 0x74, 0x05, 0x08, 0x01, 0x01, 0x8a,
                                   0x02, 0x07, 0x80 ,0x8b ,0x07, 0x60, 0x85, 0x74, 0x05, 0x08, 0x02, 0x01, 0xac, 0x12,
                                   0x80, 0x11, 0x00, 0x31, 0x31, 0x31 ,0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31,
                                   0x31, 0x31, 0x31, 0x31, 0x31, 0xbe, 0x10, 0x04 ,0x0e, 0x01, 0x00, 0x00, 0x00, 0x06,
                                   0x5f, 0x1f, 0x04, 0x00, 0x00, 0x08, 0xcb, 0x00, 0x80, 0x9c ,0x46, 0x7e }; // AARQ request for authorization by User with passord

            static byte[] DISC = { 0x7e, 0xa0, 0x0a, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x53, 0x0a, 0xd5, 0x7e }; // frame DISC  

            //Requests for Parameters
            private byte[] VoltagePhaseA = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                             0x48, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                             0x00, 0x20, 0x07, 0x00, 0x00, 0x02, 0x00, 0x73, 0x88, 0x7e };

            private byte[] VoltagePhaseB = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x93,
                                             0x17, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                             0x00, 0x34, 0x07, 0x00, 0x00, 0x02, 0x00, 0x6f, 0xda, 0x7e };

            private byte[] VoltagePhaseC = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                             0x48, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                             0x00, 0x48, 0x07, 0x00, 0x00, 0x02, 0x00, 0x9a, 0x2e, 0x7e };

            private byte[] CurrentPhaseA = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                             0x48, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                             0x00, 0x1f, 0x07, 0x00, 0x00, 0x02, 0x00, 0x2a, 0x72, 0x7e, };

            private byte[] CurrentPhaseB = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                             0x48, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                             0x00, 0x33, 0x07, 0x00, 0x00, 0x02, 0x00, 0xbe, 0xc6, 0x7e };

            private byte[] CurrentPhaseC = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                             0x48, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                             0x00, 0x47, 0x07, 0x00, 0x00, 0x02, 0x00, 0x13, 0x13, 0x7e };

            private byte[] PowerPhaseA = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                           0x48, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                           0x00, 0x15, 0x07, 0x00, 0x00, 0x02, 0x00, 0x84, 0xd6, 0x7e };

            private byte[] PowerPhaseB = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                           0x48, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                           0x00, 0x29, 0x07, 0x00, 0x00, 0x02, 0x00, 0x6d, 0x70, 0x7e };

            private byte[] PowerPhaseC = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                           0x48, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                           0x00, 0x3D, 0x07, 0x00, 0x00, 0x02, 0x00, 0x24, 0x5b, 0x7e };

            private byte[] AveragePower = { 0x7E, 0xA0, 0x1C, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                            0x48, 0xE6, 0xE6, 0x00, 0xC0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                            0x00, 0x01, 0x07, 0x00, 0x00, 0x02, 0x00, 0x38, 0x09, 0x7E };

            private byte[] SummaryPower = { 0x7E, 0xA0, 0x1C, 0x06, 0xA8, 0xAC, 0x01, 0x21, 0x10, 0x91,
                                            0x48, 0xE6, 0xE6, 0x00, 0xC0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                            0x00, 0x01, 0x08, 0x00, 0xFF, 0x02, 0x00, 0x37, 0xA5, 0x7E};

            private short CrcHDLC(byte[] data, int size, int iOffset) // Supporting Function for finding CRC
            {
                byte bTmp;

                int wCrc = (int)0xFFFF;//InitHdlcCrc;
                for (int i = 0; i < size; i++)
                {
                    bTmp = (byte)(data[iOffset + i] ^ (byte)(wCrc & 0x00ff));
                    bTmp = (byte)((bTmp << 4) ^ bTmp);
                    wCrc = (int)(wCrc >> 8);

                    int iTmp0 = bTmp << 8;
                    iTmp0 ^= (bTmp << 3) & 0x07ff;
                    iTmp0 ^= (bTmp >> 4) & 0x0000000f;

                    wCrc ^= iTmp0 & 0x0000ffff;
                }
                wCrc ^= 0xffff;
                return (short)wCrc;
            }

            private void FirstCheckSum(byte[] array) // Supporting Function for finding CRC
            {
                byte[] HDLC_HEADER = new byte[8];
                for (int i = 1, j = 0; i < HDLC_HEADER.Length + 1; i++, j++) HDLC_HEADER[j] = array[i];
                short crc = CrcHDLC(HDLC_HEADER, HDLC_HEADER.Length, 0);
                byte[] sum = BitConverter.GetBytes(crc);
                array[9] = sum[0];
                array[10] = sum[1];
            }

            private void SecondCheckSum(byte[] array) // Supporting Function for finding CRC
            {
                byte[] cal = new byte[array.Length - 4];
                for (int i = 1, k = 0; i < array.Length - 3; i++, k++) cal[k] = array[i];
                short crc = CrcHDLC(cal, cal.Length, 0);
                byte[] ret = BitConverter.GetBytes(crc);
                array[array.Length - 3] = ret[0];
                array[array.Length - 2] = ret[1];
            }

            private void BarcodeToAddr(long bar, ref ushort addrHI, ref ushort addrLO) // Supporting Function for finding CRC
            {
                addrHI = 0;
                addrLO = (ushort)(bar & 0x3FFF);
                if ((bar & 0x3FFF) < 16)
                {
                    addrLO += 16;
                    addrHI |= (1 << 12);
                }
                else
                {
                    addrLO = (ushort)(bar & 0x3FFF);
                }
                if (((bar >> 14) & 0x3FFF) < 16)
                {
                    addrHI += (ushort)(((bar >> 14) & 0x3FFF) + 16);
                    addrHI |= (1 << 13);
                }
                else
                {
                    addrHI |= (ushort)((bar >> 14) & 0x3FFF);
                }
            }

            private ushort HDLC_ADDRESS_CONVERT(int a, int l) // Supporting Function for finding CRC
            {
                return (ushort)(((((a) << 1) & 0x00FE) | (l)) | (((a) << 2) & 0xFE00));
            }

            private ushort HDLC_ADDRESS_DECODE(int a) // Supporting Function for finding CRC
            {
                return (ushort)((((a) >> 1) & 0x007F) | (((a) >> 2) & 0x3F80));
            }

            private void MakeCRC(int address) // Function for finding CRC 
            {
                ushort high = 0, low = 0;
                BarcodeToAddr(address, ref high, ref low);
                high = HDLC_ADDRESS_CONVERT(high, 0);
                low = HDLC_ADDRESS_CONVERT(low, 1);

                address = (high << 16) | low;

                byte[] array = BitConverter.GetBytes(address).Reverse().ToArray();

                for (int i = 3, j = 0; i < 7; i++, j++)
                {
                    SNRM[i] = array[j];
                    AARQ[i] = array[j];
                    DISC[i] = array[j];
                    VoltagePhaseA[i] = array[j];
                    VoltagePhaseB[i] = array[j];
                    VoltagePhaseC[i] = array[j];
                    CurrentPhaseA[i] = array[j];
                    CurrentPhaseB[i] = array[j];
                    CurrentPhaseC[i] = array[j];
                    PowerPhaseA[i] = array[j];
                    PowerPhaseB[i] = array[j];
                    PowerPhaseC[i] = array[j];
                    SummaryPower[i] = array[j];
                    AveragePower[i] = array[j];
                }

                FirstCheckSum(AARQ);
                FirstCheckSum(VoltagePhaseA);
                FirstCheckSum(VoltagePhaseB);
                FirstCheckSum(VoltagePhaseC);
                FirstCheckSum(CurrentPhaseA);
                FirstCheckSum(CurrentPhaseB);
                FirstCheckSum(CurrentPhaseC);
                FirstCheckSum(PowerPhaseA);
                FirstCheckSum(PowerPhaseB);
                FirstCheckSum(PowerPhaseC);
                FirstCheckSum(AveragePower);
                FirstCheckSum(SummaryPower);

                SecondCheckSum(SNRM);
                SecondCheckSum(AARQ);
                SecondCheckSum(DISC);
                SecondCheckSum(VoltagePhaseA);
                SecondCheckSum(VoltagePhaseB);
                SecondCheckSum(VoltagePhaseC);
                SecondCheckSum(CurrentPhaseA);
                SecondCheckSum(CurrentPhaseB);
                SecondCheckSum(CurrentPhaseC);
                SecondCheckSum(PowerPhaseA);
                SecondCheckSum(PowerPhaseB);
                SecondCheckSum(PowerPhaseC);
                SecondCheckSum(AveragePower);
                SecondCheckSum(SummaryPower);
            }

            private byte[] ReadPacket(SerialPort port)
            {
                int byteCount = port.BytesToRead; // get count of bytes in buffer
                byte[] retBytes = new byte[byteCount];
                port.Read(retBytes, 0, byteCount); // read bytes from buffer
                if (retBytes.Length == 0) throw new IndexOutOfRangeException(); // if array is Empty throw Exception
                return retBytes;
            }

            private string GetParam(SerialPort port, byte[] data, int first = 20, int last = 29)
            {
                string value = String.Empty;
                try
                {
                    WritePacket(port, data); // Send request for Parameter
                    receive = ReadPacket(port); // receive responce
                    for (int i = first; i < last; i++) value += Convert.ToChar(receive[i]); // Get only data of parameter

                    Console.WriteLine(value);

                    return value; // return read value;
                }
                catch (Exception ex) { Console.WriteLine(">>> TCER_31: " + ex.Message); return " - 1"; } // return -1 if some troubles
            }

            public void GetData()
            {
                if (USE_NOW == USERASPBERRY)
                {
                    try
                    {
                        if (Array.IndexOf(SerialPort.GetPortNames(), "/dev/ttyUSB0") > -1) // check if USB-COM is available
                        {
                            SerialPort port = new SerialPort("/dev/rs485", 9600, Parity.None, 8, StopBits.One); // Create SerialPort object

                            port.Handshake = Handshake.None; // HandShake to none
                            port.ReadTimeout = 2000; // TimeOut for read and write
                            port.WriteTimeout = 2000;

                            if (!(port.IsOpen)) port.Open(); // open port

                            WritePacket(port, SNRM); // Write SNRM request
                            receive = ReadPacket(port); //Answer from NIK

                            WritePacket(port, AARQ); // Write AARQ request
                            receive = ReadPacket(port); //Answer from NIK

                            //Console.WriteLine(receive[31]);

                            // Read parameters from NIK
                            lock (locker)
                            {
                                double value = Convert.ToDouble(GetParam(port, VoltagePhaseA).Replace(",", "."));
                                WriteRaspberryParamsToArchive(97, 0, 2, value);
                                MainData.Add(new DataParameter(NIK_Address.ToString(), 22, 97, 0, 2, DateTime.Now, value)); // Adding Voltage Phase A

                                value = Convert.ToDouble(GetParam(port, VoltagePhaseB).Replace(",", "."));
                                WriteRaspberryParamsToArchive(97, 1, 2, value);
                                MainData.Add(new DataParameter(NIK_Address.ToString(), 22, 97, 1, 2, DateTime.Now, value)); // Adding Voltage Phase B

                                value = Convert.ToDouble(GetParam(port, VoltagePhaseC).Replace(",", "."));
                                WriteRaspberryParamsToArchive(97, 2, 2, value);
                                MainData.Add(new DataParameter(NIK_Address.ToString(), 22, 97, 2, 2, DateTime.Now, value)); // Adding Voltage Phase C

                                value = Convert.ToDouble(GetParam(port, CurrentPhaseA).Replace(",", "."));
                                WriteRaspberryParamsToArchive(3, 0, 2, value);
                                MainData.Add(new DataParameter(NIK_Address.ToString(), 22, 3, 0, 2, DateTime.Now, value)); // Adding Current Phase A

                                value = Convert.ToDouble(GetParam(port, CurrentPhaseB).Replace(",", "."));
                                WriteRaspberryParamsToArchive(3, 1, 2, value);
                                MainData.Add(new DataParameter(NIK_Address.ToString(), 22, 3, 1, 2, DateTime.Now, value)); // Adding Current Phase B

                                value = Convert.ToDouble(GetParam(port, CurrentPhaseC).Replace(",", "."));
                                WriteRaspberryParamsToArchive(3, 2, 2, value);
                                MainData.Add(new DataParameter(NIK_Address.ToString(), 22, 3, 2, 2, DateTime.Now, value)); // Adding Current Phase C

                                value = Convert.ToDouble(GetParam(port, PowerPhaseA).Replace(",", "."));
                                WriteRaspberryParamsToArchive(2, 0, 2, value);
                                MainData.Add(new DataParameter(NIK_Address.ToString(), 22, 2, 0, 2, DateTime.Now, value)); // Adding Power Phase A

                                value = Convert.ToDouble(GetParam(port, PowerPhaseB).Replace(",", "."));
                                WriteRaspberryParamsToArchive(2, 1, 2, value);
                                MainData.Add(new DataParameter(NIK_Address.ToString(), 22, 2, 1, 2, DateTime.Now, value)); // Adding Power Phase B 

                                value = Convert.ToDouble(GetParam(port, PowerPhaseC).Replace(",", "."));
                                WriteRaspberryParamsToArchive(2, 2, 2, value);
                                MainData.Add(new DataParameter(NIK_Address.ToString(), 22, 2, 2, 2, DateTime.Now, value)); // Adding Power Phase C

                                value = Convert.ToDouble(GetParam(port, AveragePower).Replace(",", "."));
                                WriteRaspberryParamsToArchive(2, 3, 2, value);
                                MainData.Add(new DataParameter(NIK_Address.ToString(), 85, 2, 4, 2, DateTime.Now, value)); // Adding Average Power by 3 Phases

                                value = Convert.ToDouble(GetParam(port, SummaryPower).Replace(",", "."));
                                WriteRaspberryParamsToArchive(2, 4, 2, value);
                                MainData.Add(new DataParameter(NIK_Address.ToString(), 42, 2, 5, 2, DateTime.Now, value)); // Adding Summary Power by all time working

                                WritePacket(port, DISC); // Write request for ending send
                                port.Close(); // ClosePort
                                Console.WriteLine(">>> Port is closed");
                            }
                        }
                        else
                        {
                            Console.WriteLine(">>> RS-485 not found");
                            Thread.Sleep(10000);
                            throw new Exception();
                        }
                    }
                    catch (Exception ex) { Console.WriteLine(">>> TCER_32: " + ex.Message + " NIK"); }
                }
            }

            public Nik(int address, int Time_Span)
            {
                int counts = 0;

                MakeCRC(address); // Calculate CRC for all request's

                while (true)
                {
                    if (USE_NOW == USERASPBERRY)
                    {
                        try
                        {
                            GetData(); // Read parameters from NIK
                            Console.WriteLine(">>> NIK Time span : " + Time_Span);
                            Thread.Sleep(TimeSpan.FromSeconds(Time_Span));
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(">>> TCER_33: " + ex.Message + " - NIK");
                            Time_Span += 2;
                            if (++counts >= 20) // counter for problems with COM-port
                            {
                                // run reboot of serial port 
                            }
                            continue;
                        }
                    }
                    else
                    {
                        Thread.Sleep(5000);
                    }
                }
            }
        }

        public static void WritePacket(SerialPort port, byte[] data)
        {
            port.Write(data, 0, data.Length); //Sending frame AARQ
            Thread.Sleep(2000);
        }

        public class GPIO
        {
            public string GPIOPin;

            public GPIO() { }

            public GPIO(string gnum)
            {
                GPIOPin = gnum;
            }

            public int GPIOExport()
            {
                Console.WriteLine(GPIOPin);
                string export_str = "/sys/class/gpio/export";
                if (File.Exists(export_str)) File.WriteAllText(export_str, GPIOPin);
                else throw new FileNotFoundException();
                return 0;
            }

            public int GPIOUnexport()
            {
                string unexport_str = "/sys/class/gpio/unexport";
                if (File.Exists(unexport_str)) File.WriteAllText(unexport_str, GPIOPin);
                else throw new FileNotFoundException();
                return 0;
            }

            public int GPIOSetDirection(string dir)
            {
                string setdir_str = "/sys/class/gpio/gpio" + GPIOPin + "/direction";
                if (File.Exists(setdir_str)) File.WriteAllText(setdir_str, dir);
                else throw new FileNotFoundException();
                return 0;
            }

            public int GPIOSetValue(string val)
            {
                string setval_str = "/sys/class/gpio/gpio" + GPIOPin + "/value";
                if (File.Exists(setval_str)) File.WriteAllText(setval_str, val);
                else throw new FileNotFoundException();
                return 0;
            }

            public int GPIOGetValue()
            {
                string getval_str = "/sys/class/gpio/gpio" + GPIOPin + "/value";
                string val = String.Empty;
                if (File.Exists(getval_str)) val = File.ReadAllText(getval_str);
                else throw new FileNotFoundException();
                return Convert.ToInt32(val);
            }

            public static GPIO Export(string pin, string direction)
            {
                GPIO gpio = new GPIO(pin);
                try { gpio.GPIOExport(); }
                catch (System.IO.IOException)
                {
                    try
                    {
                        Thread.Sleep(200);
                        gpio.GPIOUnexport();
                        Thread.Sleep(200);
                        gpio.GPIOExport();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(">>> GPIO Export failed + " + ex.Message);
                    }
                }
                gpio.GPIOSetDirection(direction);
                return gpio;
            }
        }

        public static void GetDoorSensorState(string GpioPin, string ParameterID, int Time_Span)
        {
            Console.WriteLine(">>> Door Sensor : StationID : " + StationID + " <<< RaspberryID : " + RaspberryID + " <<< GpioPin : " + GpioPin + " <<< ParameterID " + ParameterID);

            GPIO gpio = GPIO.Export(GpioPin, "in"); // Export GPIO for door sensor
            int state = 0, currentState = 0, count = 0;

            while (true)
            {
                try
                {
                    currentState = gpio.GPIOGetValue();
                    if (state == 1 && currentState == 0) // if door sensor activated
                    {
                        state = currentState; // change lastState of sensor on current
                        Console.WriteLine(">>> DoorSensor current state : " + currentState);

                        //Run program to make photos
                        //RunCamera();
                    }
                    if (state == 0 && currentState == 1) state = currentState; // change lastState if sendor diactivates
                    Thread.Sleep(200);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(">>> " + ex.Message + " GetDoorSensorState()");
                    if (++count >= 20) // counter for problems with sensor
                    {
                        //run diagnostic of door sensor
                    }
                }
                Thread.Sleep(200);
            }
        }

        public static bool IsConnected(Socket socket)
        {
            try
            {
                return !(socket.Poll(1, SelectMode.SelectRead) && socket.Available == 0); // detect for socket connection
            }
            catch (SocketException) { return false; }
        }

        private static bool sendCommand(string lamp_address, byte register, byte functionCode, byte[] Value)
        {
            bool sendOk = false;
            int tryCount = 0;

            try
            {
                Console.WriteLine(">>> Trying to send command to controller");
                if (functionCode == 0x03)
                {
                    if (Request(Convert.ToUInt16(lamp_address), functionCode, register))
                    {
                        Console.WriteLine(">>> Commands executed success");
                        Thread.Sleep(20000);
                    }
                    else Console.WriteLine(">>> Commands executed failed");
                }
                else
                {
                    if (Request(Convert.ToUInt16(lamp_address), functionCode, register, Value))
                    {
                        Console.WriteLine(">>> Commands executed success");

                        byte byteRead = 0;

                        if (register == 0x70)
                        {
                            String response = String.Empty;

                            while (Convert.ToChar(byteRead) != '&')
                            {
                                try
                                {
                                    byteRead = Convert.ToByte(serial.ReadByte());
                                    response += Convert.ToChar(byteRead);
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(">>> Waiting for response from radio");
                                }
                            }
                            Console.WriteLine(">>> " + response);
                            ConvertRadioResponse(response);
                        }
                    }
                    else Console.WriteLine(">>> Commands executed failed");
                }
                sendOk = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " sendCommands()");
                sendOk = false;
                tryCount = tryCount + 1;
                if (tryCount >= 10) return sendOk;
            }
            return sendOk;
        }

        private static void ConvertRadioResponse(String response)
        {
            //example of response --- #ALL-î-01/01/00/20/01/29/00-064-Z-0&
            String[] parameters = response.Split('-');
            String ID = parameters[1];

            double illumination = Convert.ToDouble(parameters[3]);
            int brightness = (int)(parameters[4][0]);
            int state = Convert.ToInt32(parameters[5][0]) - 0x30;

            Console.WriteLine(">>> Illumination = " + illumination);
            Console.WriteLine(">>> Brightness = " + brightness);
            Console.WriteLine(">>> State = " + state);

            MainData.Add(new DataParameter(GetSerialNumber(Convert.ToInt32(ID)), 18, 46, 0, 2, DateTime.Now, Convert.ToDouble(illumination)));
            MainData.Add(new DataParameter(GetSerialNumber(Convert.ToInt32(ID)), 18, 47, 0, 2, DateTime.Now, Convert.ToDouble(brightness)));
            MainData.Add(new DataParameter(GetSerialNumber(Convert.ToInt32(ID)), 18, 45, 0, 2, DateTime.Now, Convert.ToDouble(state)));

        }

        public static void ExecuteCommand(ServerCommands cmd)
        {
            try
            {
                if (cmd.command.Contains("sBrightness-")) // if we receive command to change PWM
                {
                    cmd.command = cmd.command.Replace("sBrightness-", "");
                    string[] commands = cmd.command.Split('-');
                    setBrightness(commands[0], commands[1]);
                }
                if (cmd.command.Contains("gBrightness-")) // if we receive command to get PWM
                {
                    cmd.command = cmd.command.Replace("gBrightness-", "");
                    getBrightness(cmd.command);
                }
                if (cmd.command.Contains("ON-")) // if we receive command to switch ON the lamp
                {
                    cmd.command = cmd.command.Replace("ON-", "");
                    string[] commands = cmd.command.Split('-');
                    if (Convert.ToInt32(commands[0]) == 1) LampON(commands[1]);
                }
                if (cmd.command.Contains("OFF-")) // if we receive command to switch OFF the lamp
                {
                    cmd.command = cmd.command.Replace("OFF-", "");
                    string[] commands = cmd.command.Split('-');
                    if (Convert.ToInt32(commands[0]) == 1) LampOFF(commands[1]);
                }
                if (cmd.command.Contains("SetID-"))
                {
                    Thread.Sleep(5000);
                    cmd.command = cmd.command.Replace("SetID-", "");
                    string[] commands = cmd.command.Split('-');
                    Console.WriteLine("SetID is coming...");
                    SetID(commands[0], commands[1]);
                }
                if (cmd.command.Contains("GetTime-"))
                {
                    cmd.command = cmd.command.Replace("GetTime-", "");
                    GetTime(cmd.command);
                }
                if (cmd.command.Contains("GetADC-"))
                {
                    cmd.command = cmd.command.Replace("GetADC-", "");
                    GetIllumination(cmd.command);
                }
                if (cmd.command.Contains("GetState-"))
                {
                    cmd.command = cmd.command.Replace("GetState-", "");
                    GetState(cmd.command);
                }
                if (cmd.command.Contains("GetGPS-"))
                {
                    cmd.command = cmd.command.Replace("GetGPS-", "");
                    GetGPS(cmd.command);
                }
                if (cmd.command.Contains("AddLampToGroup-"))
                {
                    cmd.command = cmd.command.Replace("AddLampToGroup-", "");
                    string[] commands = cmd.command.Split('-');
                    AddLampToGroup(commands[0], Convert.ToByte(commands[1]));
                }
                if (cmd.command.Contains("DeleteLampFromGroup-"))
                {
                    cmd.command = cmd.command.Replace("DeleteLampFromGroup-", "");
                    string[] commands = cmd.command.Split('-');
                    DeleteLampFromGroup(commands[0], Convert.ToByte(commands[1]));
                }
                if (cmd.command.Contains("GSBrightness-"))
                {
                    cmd.command = cmd.command.Replace("GSBrightness-", "");
                    string[] commands = cmd.command.Split('-');
                    GroupSetBrightness(commands[0], commands[1]);
                }
                if (cmd.command.Contains("Gon-"))
                {
                    cmd.command = cmd.command.Replace("Gon-", "");
                    string[] commands = cmd.command.Split('-');
                    if (Convert.ToInt32(commands[1]) == 1) GroupON(commands[0]);
                }
                if (cmd.command.Contains("Goff-"))
                {
                    cmd.command = cmd.command.Replace("Goff-", "");
                    string[] commands = cmd.command.Split('-');
                    if (Convert.ToInt32(commands[1]) == 1) GroupOFF(commands[0]);
                }
                if (cmd.command.Contains("GetAll-"))
                {
                    string GetID = cmd.command.Replace("GetAll-", "");
                    GetAll(GetID);
                }
                Thread.Sleep(200);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " ExecuteCommand()");
            }
        }

        private static void GetAll(string StationEquipmentID)
        {
            try
            {
                Console.WriteLine(">>> Command to get all info from Lamp Address : " + StationEquipmentID);
                byte[] Value = { 0, 0, 0, 0 };
                sendCommand(StationEquipmentID, 0x70, 0x0A, Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " setBrightnes");
            }

        }

        private static void setBrightness(string lamp_address, string percents)
        {
            try
            {
                Console.WriteLine(">>> Command to set PWM with Lamp Address : " + lamp_address + " and set Percents to : " + percents);
                byte[] Value = { 0, 0, 0, Convert.ToByte(percents) };
                sendCommand(lamp_address, 0x5F, 0x0A, Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " setBrightnes");
            }
        }

        private static void getBrightness(string lamp_address)
        {
            try
            {
                Console.WriteLine(">>> Command to get PWM of Lamp with  Address : " + lamp_address);
                byte[] Value = { 0, 0, 0, 0 };
                sendCommand(lamp_address, 0x5F, 0x03, Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " getBrightnes");
            }
        }

        private static void LampON(string lamp_address)
        {
            try
            {
                Console.WriteLine(">>> Command to switch on lamp with address : " + lamp_address);
                byte[] Value = { 0, 0, 0, Convert.ToByte(1) };
                sendCommand(lamp_address, 0x5D, 0x0A, Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " LampON");
            }
        }

        private static void LampOFF(string lamp_address)
        {
            try
            {
                Console.WriteLine(">>> Command to switch off lamp with address : " + lamp_address);
                byte[] Value = { 0, 0, 0, Convert.ToByte(0) };
                sendCommand(lamp_address, 0x5D, 0x0A, Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " LampOFF");
            }
        }

        private static void SetID(String ID, String serialNumber)
        {
            try
            {
                UInt16 val = Convert.ToUInt16(ID);
                UInt16 serialXor = 0;

                Console.WriteLine(">>> Go to xor serialNumber value...");
                foreach (var a in serialNumber)
                {
                    serialXor += Convert.ToUInt16(a);
                }
                Console.WriteLine(">>> Xor is succesfull");
                Console.WriteLine(">>> SerialXor = " + serialXor);
                //byte[] Value = { Convert.ToByte(serialXor >> 8), Convert.ToByte(serialXor), Convert.ToByte(val >> 8), Convert.ToByte(val) };
                byte[] tmp = BitConverter.GetBytes(serialXor);
                Console.WriteLine(">>> First byte : " + tmp[0]);
                Console.WriteLine(">>> Second byte : " + tmp[1]);
                byte[] Value = { tmp[0], tmp[1], Convert.ToByte(val >> 8), Convert.ToByte(val) };
                sendCommand("0", 0x5E, 0x0A, Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " SetID()");
            }
        }

        private static void GetIllumination(string lamp_address)
        {
            try
            {
                Console.WriteLine(">>> Command to get illumination of lamp with address : " + lamp_address);
                byte[] Value = { 0, 0, 0, 0 };
                sendCommand(lamp_address, 0x62, 0x03, Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " GetIllumination");
            }
        }

        private static void GetState(string lamp_address)
        {
            try
            {
                Console.WriteLine(">>> Command to get state of lamp with address : " + lamp_address);
                byte[] Value = { 0, 0, 0, 0 };
                sendCommand(lamp_address, 0x5D, 0x03, Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " GetState");
            }
        }

        private static void GetGPS(string lamp_address)
        {
            try
            {
                Console.WriteLine(">>> Command to get gps of lamp with address : " + lamp_address);
                byte[] Value = { 0, 0, 0, 0 };
                sendCommand(lamp_address, 0x61, 0x03, Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " GetState");
            }
        }

        private static void GetTime(string lamp_address)
        {
            try
            {
                Console.WriteLine(">>> Command to get time on lamp with address : " + lamp_address);
                byte[] Value = { 0, 0, 0, 0 };
                sendCommand(lamp_address, 0x60, 0x03, Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " GetTime");
            }
        }

        private static void AddLampToGroup(string lamp_address, byte GroupID)
        {
            try
            {
                Console.WriteLine(">>> Command to add lamp with address " + lamp_address + " from group " + GroupID);
                byte[] Value = { 0, 0, 0, GroupID };
                sendCommand(lamp_address, 0x63, 0x0A, Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " AddLampToGroup");
            }
        }

        private static void DeleteLampFromGroup(string lamp_address, byte GroupID)
        {
            try
            {
                Console.WriteLine(">>> Command to delete lamp with address " + lamp_address + " from group " + GroupID);
                byte[] Value = { 0, 0, 0, GroupID };
                sendCommand(lamp_address, 0x64, 0x0A, Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " DeleteLampFromGroup");
            }
        }

        private static void GroupSetBrightness(string GroupID, string percents)
        {
            try
            {
                Console.WriteLine(">>> Command to set PWM to group : " + GroupID + " and set Percents to : " + percents);
                byte[] Value = { 0, 0, 0, Convert.ToByte(percents) };
                sendCommand(GroupID, 0x5F, 0x0A, Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " GroupSetBrightness");
            }
        }

        private static void GroupON(string GroupID)
        {
            try
            {
                Console.WriteLine(">>> Command to switch on lamps in group " + GroupID);
                byte[] Value = { 0, 0, 0, 1 };
                sendCommand(GroupID, 0x5D, 0x0F, Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " GroupON");
            }
        }

        private static void GroupOFF(string GroupID)
        {
            try
            {
                Console.WriteLine(">>> Command to switch off lamps in group " + GroupID);
                byte[] Value = { 0, 0, 0, 0 };
                sendCommand(GroupID, 0x5D, 0x0F, Value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " GroupOFF");
            }
        }

        private static void RunCamera()
        {
            try
            {
                string strCmdText = "bash /home/pi/camera.sh";

                ProcessStartInfo start = new ProcessStartInfo();
                start.Arguments = strCmdText;
                start.FileName = "sudo";
                start.CreateNoWindow = true;

                using (Process proc = Process.Start(start))
                {
                    proc.WaitForExit();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " RunCamera");
            }
        }

        public static void ReadConfiguration(string xml)
        {
            RadioSerialNumber = GetRadioSerial();
            RadioSerialNumber = RadioSerialNumber.Replace(Convert.ToString(Convert.ToChar(0x0a)), "");

            using (XmlReader reader = XmlReader.Create(new StringReader(xml)))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement()) // Only detect start elements.
                    {
                        switch (reader.Name) // Get element name and switch on it.
                        {
                            case "Raspberry":
                                {
                                    reader.MoveToAttribute("RaspberryID");
                                    RaspberryID = reader.Value;
                                    //Console.WriteLine("RaspID Converted");

                                    reader.MoveToAttribute("StationID");
                                    StationID = Convert.ToInt32(reader.Value);
                                    //Console.WriteLine("StationID Converted");


                                    reader.MoveToAttribute("EquipmentTypeID");
                                    EquipmentTypeID = Convert.ToInt32(reader.Value);
                                    //Console.WriteLine("EquipmentTypeID Converted");

                                    reader.MoveToAttribute("Host");
                                    Host = reader.Value;

                                    reader.MoveToAttribute("Port");
                                    Port = Convert.ToInt32(reader.Value);

                                    reader.MoveToAttribute("RadioResetPin");
                                    radioResetPin = reader.Value;
                                }
                                break;
                            case "NIK":
                                {
                                    reader.MoveToAttribute("Address");
                                    NIK_Address = Convert.ToInt32(reader.Value);

                                    reader.MoveToAttribute("Time_Span");
                                    int Time_Span = Convert.ToInt32(reader.Value);

                                    Thread nik_Thread = new Thread(unused => new Nik(NIK_Address, Time_Span));
                                    nik_Thread.IsBackground = true;
                                    nik_Thread.Start();
                                }
                                break;
                            case "DoorSensor":
                                {
                                    reader.MoveToAttribute("GPIO");
                                    String GpioPin = reader.Value;

                                    reader.MoveToAttribute("Time_Span");
                                    int Time_Span = Convert.ToInt32(reader.Value);

                                    reader.MoveToAttribute("ParameterID");
                                    String ParameterID = reader.Value;

                                    Thread doorSensorThread = new Thread(unused => GetDoorSensorState(GpioPin, ParameterID, Time_Span));
                                    doorSensorThread.IsBackground = true;
                                    //doorSensorThread.Start();
                                }
                                break;
                        }
                    }
                }
            }

            Console.WriteLine("==================================================");
            Console.WriteLine(">>>   Host : " + Host);
            Console.WriteLine(">>>   Port : " + Port);
            Console.WriteLine(">>>   RaspberryID : " + RaspberryID);
            Console.WriteLine(">>>   StationID : " + StationID);
            Console.WriteLine(">>>   NIK Address : " + NIK_Address);
            Console.WriteLine(">>>   Serial Number of Radio : " + RadioSerialNumber);
            Console.WriteLine(">>>   Radio Reset Pin : " + radioResetPin);
            Console.WriteLine("==================================================");
        }

        private static String GetStationEquipmentID(int ID, String SerialNumber)
        {
            String StationEquipmentID = String.Empty;
            string cs = @"server=localhost;userid=root;
                      password=raspberry;database=RaspDB";

            if (ID != 65535)
            {
                try
                {
                    String DBSerial = GetSerialNumber(ID);
                    String DB_ID = GetStationEquipmentIDBySerialNumber(SerialNumber);

                    if (DBSerial == SerialNumber)
                    {
                        Console.WriteLine(">>> Serial is idential");
                    }
                    else
                    {
                        Console.WriteLine(">>> Serial is changed");

                        MySqlConnection conn = new MySqlConnection(cs);
                        conn.Open();

                        string sql = "UPDATE StationEquipmentIdToSerialNumber SET SerialNumber ='" + SerialNumber + "\' WHERE StationEquipmentID = " + ID;

                        MySqlCommand cmd = new MySqlCommand(sql, conn);
                        cmd.Prepare();

                        cmd.ExecuteNonQuery();

                        conn.Close();
                    }
                    return Convert.ToString(ID);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(">>> TCER_34: " + ex.Message + " GetStationEquipmentID");
                }
            }
            else
            {
                StationEquipmentID = GetStationEquipmentIDBySerialNumber(SerialNumber);

                if (StationEquipmentID != String.Empty)
                {
                    return StationEquipmentID;
                }
                else
                {
                    byte[] buffer = new byte[8];
                    while (true)
                    {
                        try
                        {
                            Console.WriteLine(">>> Connect to server for geting StationEquipmentID - Host : " + Host + ", Port : " + Port);

                            TcpClient client = new TcpClient(Host, Port);

                            client.Client.SendTimeout = 5000;
                            client.Client.ReceiveTimeout = 5000;

                            client.Client.Send(Encoding.ASCII.GetBytes("StationEquipmentID$" + SerialNumber + "$" + RaspberryID)); // request to server with sending SerialNumber
                            Thread.Sleep(TimeSpan.FromSeconds(10));
                            int bytesRead = client.Client.Receive(buffer);

                            Console.WriteLine(">>> Bytes read : " + bytesRead);

                            buffer = buffer.Take(bytesRead).ToArray();
                            StationEquipmentID = Encoding.ASCII.GetString(buffer); // Converting received value 

                            Console.WriteLine(">>> Geted StationEquipmentID : " + StationEquipmentID);
                            InsertSerialNumber(Convert.ToInt32(StationEquipmentID), SerialNumber); // Add StationEquipmentId of new lamp to out table on Raspberry

                            return StationEquipmentID; // return StationEquipmentID of new lamp to controller
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(">>> TCER_35: " + ex.Message + " GetStationEquipmentID()");
                            Thread.Sleep(TimeSpan.FromSeconds(5)); // while we don't receive StationEquipmentID of new lamp we will try to do this again and again
                            continue;
                        }
                    }
                }
            }
            return null;
        }

        private static String GetSerialNumber(int StationEquipmentID)
        {
            String SerialNumber = String.Empty;

            string cs = @"server=localhost;userid=root;password=raspberry;database=RaspDB";

            try
            {
                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                string sql = "Select SerialNumber From StationEquipmentIdToSerialNumber Where StationEquipmentID = " + StationEquipmentID;

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Console.WriteLine(rdr.GetString(0));
                    SerialNumber = rdr.GetString(0);
                }

                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_36: " + ex.Message + " GetSerialNumber()");
            }
            return SerialNumber;
        }

        private static String GetStationEquipmentIDBySerialNumber(String SerialNumber)
        {
            String StationEquipmentID = String.Empty;

            string cs = @"server=localhost;userid=root;password=raspberry;database=RaspDB";

            try
            {
                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                string sql = "Select StationEquipmentID From StationEquipmentIdToSerialNumber Where SerialNumber =\'" + SerialNumber + "\'";

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Console.WriteLine(rdr.GetString(0));
                    StationEquipmentID = rdr.GetString(0);
                }

                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_37: " + ex.Message + " GetStationEquipmentIDBySerialNumber");
            }
            return StationEquipmentID;
        }

        private static void InsertSerialNumber(int StationEquipmentID, String SerialNumber)
        {
            string cs = @"server=localhost;userid=root;password=raspberry;database=RaspDB";

            try
            {
                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                string sql = "INSERT INTO StationEquipmentIdToSerialNumber() VALUES(NULL, @StationEquipmentID, @SerialNumber);";

                MySqlCommand cmd = new MySqlCommand(sql);
                cmd.Connection = conn;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@StationEquipmentID", StationEquipmentID);
                cmd.Parameters.AddWithValue("@SerialNumber", SerialNumber);

                cmd.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_38: " + ex.Message + " InsertSerialNumber()");
            }
        }

        private static void WriteLampParamsToArchive(int LampAddress, int ObisCode, int ParameterNumber, int QualityID, double Value)
        {
            string cs = @"server=localhost;userid=root;
            	      password=raspberry;database=RaspDB";

            try
            {
                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                string sql = "INSERT INTO LampArchives() VALUES(NULL, @RaspberryID, @LampAddress, @ObisCode, @ParameterNumber, @QualityID, @Value);";

                MySqlCommand cmd = new MySqlCommand(sql);
                cmd.Connection = conn;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@RaspberryID", RaspberryID);
                cmd.Parameters.AddWithValue("@LampAddress", LampAddress);
                cmd.Parameters.AddWithValue("@ObisCode", ObisCode);
                cmd.Parameters.AddWithValue("@ParameterNumber", ParameterNumber);
                cmd.Parameters.AddWithValue("@QualityID", QualityID);
                cmd.Parameters.AddWithValue("@Value", Value);

                cmd.ExecuteNonQuery();

                conn.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(">>> TCER_39: " + ex.Message + " WriteLampParamsToArchive");
                return;
            }
        }

        private static void WriteRaspberryParamsToArchive(int ObisCode, int ParameterNumber, int QualityID, double Value)
        {
            string cs = @"server=localhost;userid=root;
            	      password=raspberry;database=RaspDB";

            try
            {
                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                string sql = "INSERT INTO RaspberryArchives() VALUES(NULL, @RaspberryID, @ObisCode, @ParameterNumber, @QualityID, @Value);";

                MySqlCommand cmd = new MySqlCommand(sql);
                cmd.Connection = conn;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@RaspberryID", RaspberryID);
                cmd.Parameters.AddWithValue("@ObisCode", ObisCode);
                cmd.Parameters.AddWithValue("@ParameterNumber", ParameterNumber);
                cmd.Parameters.AddWithValue("@QualityID", QualityID);
                cmd.Parameters.AddWithValue("@Value", Value);

                cmd.ExecuteNonQuery();

                conn.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("TCER_40: " + ex.Message + " WriteRaspberryParamsToArchive");
                return;
            }
        }

        public static void SendData()
        {
            while (true)
            {
                try
                {
                    lock (locker)
                    {
                        string toSend = String.Empty;
                        foreach (var a in MainData)
                        {
                            toSend += Convert.ToString(a.DriverFeature) + '$';
                            toSend += Convert.ToString(a.EquipmentTypeID) + '$';
                            toSend += Convert.ToString(a.obisCode) + '$';
                            toSend += Convert.ToString(a.parameterNumber) + '$';
                            toSend += Convert.ToString(a.qualityID) + '$';
                            toSend += Convert.ToString(a.Date) + '$';
                            toSend += Convert.ToString(a.value) + '#';
                        }
                        MainData.Clear();
                        Console.WriteLine(toSend);

                        Console.WriteLine("Host - " + Host + " : Port - " + Port);

                        if (toSend != String.Empty)
                        {
                            TcpClient client = new TcpClient(Host, Convert.ToInt32(Port));
                            Socket socket = client.Client;

                            socket.Send(Encoding.ASCII.GetBytes(toSend));

                            socket.Close();
                            client.Close();
                        }
                    }
                    Thread.Sleep(TimeSpan.FromMinutes(1));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(">>> TCER_41: " + ex.Message + " SendData()");
                    Thread.Sleep(1000);
                    continue;
                }
            }
        }

        public static void RadioWork()
        {
            serial = new SerialPort("/dev/ttyAMA0", 9600, Parity.None, 8, StopBits.One);
            serial.ReadTimeout = 500;
            serial.WriteTimeout = 500;

            serial.Open();

            int byteRead = 0;
            String radioCommand = String.Empty;

            while (true)
            {
                radioCommand = String.Empty;

                try
                {
                    lock (commandsLocker)
                    {
                        if (SetCommands.Any())
                        {
                            ServerCommands command = SetCommands.First();
                            SetCommands.RemoveAt(0);
                            Console.WriteLine(command.command);
                            ExecuteCommand(command);
                        }
                    }

                    byteRead = serial.ReadByte();
                    if (Convert.ToChar(byteRead) == '#')
                    {
                        Console.WriteLine(">>> Start frame comes");
                        while (Convert.ToChar(byteRead) != '&')
                        {
                            byteRead = serial.ReadByte();
                            if (Convert.ToChar(byteRead) != '&') radioCommand += Convert.ToChar(byteRead);
                        }
                        Console.WriteLine(">>> End frame comes");
                        Console.WriteLine(">>> Command : " + radioCommand);

                        if (radioCommand.Contains("ID"))
                        {
                            string[] responce = radioCommand.Split('-');
                            int ID = (responce[1][0] << 8) | (responce[1][1]);
                            string serialNumber = responce[2];
                            String NewID = GetStationEquipmentID(ID, serialNumber);

                            Console.WriteLine(">>> Getted ID from Radio : " + ID);
                            Console.WriteLine(">>> Getted RadioSerial from Radio : " + serialNumber);

                            lock (commandsLocker)
                            {
                                SetCommands.Add(new ServerCommands("SetID-" + NewID + "-" + serialNumber, 0, 0));
                            }
                        }
                        if (radioCommand.Contains("ADC"))
                        {
                            string[] responce = radioCommand.Split('-');
                            int ID = (responce[1][0] << 8) | (responce[1][1]);
                            string value = responce[2];

                            Console.WriteLine(">>> Getted ID from Radio : " + ID);
                            Console.WriteLine(">>> Getted ADC from Radio : " + value);
                        }
                        if (radioCommand.Contains("PWM"))
                        {
                            string[] responce = radioCommand.Split('-');
                            int ID = (responce[1][0] << 8) | (responce[1][1]);
                            int value = (int)Char.GetNumericValue(Convert.ToChar(responce[2]));

                            Console.WriteLine(">>> Getted ID from Radio : " + ID);
                            Console.WriteLine(">>> Getted PWM from Radio : " + value);
                        }
                        if (radioCommand.Contains("STATE"))
                        {
                            string[] responce = radioCommand.Split('-');
                            int ID = (responce[1][0] << 8) | (responce[1][1]);
                            int value = Convert.ToInt32(Convert.ToChar(responce[2]));

                            Console.WriteLine(">>> Getted ID from Radio : " + ID);
                            Console.WriteLine(">>> Getted State from Radio : " + value);
                        }
                        if (radioCommand.Contains("RTC"))
                        {
                            string[] responce = radioCommand.Split('-');
                            int ID = (responce[1][0] << 8) | (responce[1][1]);
                            string[] time = responce[2].Split('/');

                            int date = (int)Char.GetNumericValue(Convert.ToChar(time[0]));
                            int day = (int)Char.GetNumericValue(Convert.ToChar(time[1]));
                            int hour = (int)Char.GetNumericValue(Convert.ToChar(time[2]));
                            int min = (int)Char.GetNumericValue(Convert.ToChar(time[3]));
                            int month = (int)Char.GetNumericValue(Convert.ToChar(time[4]));
                            int sec = (int)Char.GetNumericValue(Convert.ToChar(time[5]));
                            int year = (int)Char.GetNumericValue(Convert.ToChar(time[6]));

                            DateTime lampTime = new DateTime(year, month, day, hour, min, sec);

                            Console.WriteLine(">>> Getted ID from Radio : " + ID);
                            Console.WriteLine(">>> Getted Time from Radio : " + lampTime);
                        }
                    }
                    Thread.Sleep(100);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(">>> TCER_42: " + ex.Message + "RadioWork()");
                    //Console.WriteLine(">>> Radio work exception");
                    Thread.Sleep(100);
                    radioCommand = String.Empty;
                }
            }
        }

        // function for get Request to radio module
        static bool Request(ushort DeviceAddress, byte function, byte RegisterAddress)
        {
            if (!(serial.IsOpen)) serial.Open();

            Console.WriteLine(">>> Sending GET request...");
            Console.WriteLine("==========================================");
            byte[] ReadRequestArr = { HI(DeviceAddress), LO(DeviceAddress), function, HI(RegisterAddress),
                                      LO(RegisterAddress), 0x00, 0x00};
            ushort crc = CalcCRC16(ReadRequestArr.Take(ReadRequestArr.Length - 2).ToArray());
            ReadRequestArr[5] = HI(crc);
            ReadRequestArr[6] = LO(crc);

            byte crcHi = HI(crc);
            byte crcLow = LO(crc);

            Console.WriteLine(crc);

            for (int i = 0; i < ReadRequestArr.Length; i++)
            {
                Console.Write("{0:X} ", ReadRequestArr[i]);
            }
            Console.WriteLine();
            Console.WriteLine("==========================================");

            int tries = 5;

            while (tries != 0)
            {
                try
                {
                    byte[] request = new byte[255];
                    int bytesRead = 0;

                    Console.WriteLine(">>> Sending data1"); // Will not Newer run 
                    byte[] eCMD = { 0x2b, 0x2b, 0x2b };
                    serial.Write(eCMD, 0, eCMD.Length);
                    Thread.Sleep(1000);
                    bytesRead = serial.Read(request, 0, serial.BytesToRead);
                    Console.Write(">>> Byte" + System.Text.Encoding.ASCII.GetString(request));
                    //Thread.Sleep(1000);

                    Console.WriteLine(">>> Sending eATAP");
                    byte[] eATAP = { 0x41, 0x54, 0x41, 0x50, 0x20, 0x31, 0xd, 0xa };
                    serial.Write(eATAP, 0, eATAP.Length);
                    Thread.Sleep(1000);
                    bytesRead = serial.Read(request, 0, serial.BytesToRead);
                    Console.Write(">>> Byte" + System.Text.Encoding.ASCII.GetString(request));
                    //Thread.Sleep(1000);

                    Console.WriteLine(">>> Sending eATRO");
                    byte[] eATRO = { 0x41, 0x54, 0x52, 0x4f, 0x20, 0x31, 0x30, 0x30, 0x30, 0xd, 0xa };
                    serial.Write(eATRO, 0, eATRO.Length);
                    Thread.Sleep(1000);
                    bytesRead = serial.Read(request, 0, serial.BytesToRead);
                    Console.Write(">>> Byte" + System.Text.Encoding.ASCII.GetString(request));
                    //Thread.Sleep(1000);

                    Console.WriteLine(">>> Sending eATCN");
                    byte[] eATCN = { 0x41, 0x54, 0x43, 0x4e, 0xd, 0xa };
                    serial.Write(eATCN, 0, eATCN.Length);
                    Thread.Sleep(1000);
                    bytesRead = serial.Read(request, 0, serial.BytesToRead);
                    Console.Write(">>> Byte" + System.Text.Encoding.ASCII.GetString(request));
                    //Thread.Sleep(1000);

                    Console.WriteLine(">>> Sending eEnter");
                    byte[] eEnter = { 0xd, 0xa };
                    serial.Write(eEnter, 0, eEnter.Length);
                    Thread.Sleep(1000);

                    serial.Write(ReadRequestArr, 0, ReadRequestArr.Length);
                    //Thread.Sleep(1000);

                    Thread.Sleep(15000);
                    bytesRead = serial.Read(request, 0, serial.BytesToRead);
                    Console.WriteLine(">>> BytesRead : " + bytesRead);

                    for (int i = 0; i < bytesRead; i++)
                        Console.Write("Byte {0:X}", request[i]);

                    Console.WriteLine(">>> SerialRead succesfull");

                    if (bytesRead > 2)
                    {
                        Console.Write(">>> Byteread=" + bytesRead);
                        Console.Write(">>> SetRequestArr.Length=" + ReadRequestArr.Length);
                        request = request.Take(bytesRead).Skip(bytesRead - 3).ToArray();

                        Console.WriteLine(">>> CrcHi : " + request[0]);
                        Console.WriteLine(">>> CrcLow : " + request[1]);
                        Console.WriteLine(">>> Third byte : " + request[2]);

                        if (request[0] == crcHi && request[1] == crcLow)
                        {
                            Console.WriteLine(">>> Write Succeeded!");
                            return true;
                        }
                    }
                    tries--;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(">>> TCER_43: " + ex.Message + " tries " + tries + " Request");
                    tries--;
                }
            }
            Console.WriteLine(">>> Reset RadioGpio");
            HardwareReset(RadioResetGPIO);
            Thread.Sleep(30000);
            return false;
        }

        // function for set Requset to radio Module
        static bool Request(ushort DeviceAddress, byte function, byte RegisterAddress, byte[] Value)
        {
            if (!(serial.IsOpen)) serial.Open();

            Console.WriteLine(">>> Sending SET Request...");
            Console.Write("==========================================\n");

            byte[] SetRequestArr = new byte[Value.Length + 7];
            SetRequestArr[0] = HI(DeviceAddress);
            SetRequestArr[1] = LO(DeviceAddress);
            SetRequestArr[2] = function;
            SetRequestArr[3] = HI(RegisterAddress);
            SetRequestArr[4] = LO(RegisterAddress);

            for (int i = 5; i < Value.Length + 5; i++)
            {
                SetRequestArr[i] = Value[i - 5];
            }

            ushort crc = CalcCRC16(SetRequestArr.Take(SetRequestArr.Length - 2).ToArray());

            byte crcHi = HI(crc);
            byte crcLow = LO(crc);

            Console.WriteLine(crc);
            Console.WriteLine(">>> Base crc Hi - {0:X}", crcHi);
            Console.WriteLine(">>> Base crc Low - {0:X}", crcLow);

            SetRequestArr[SetRequestArr.Length - 2] = crcHi;
            SetRequestArr[SetRequestArr.Length - 1] = crcLow;

            for (int i = 0; i < SetRequestArr.Length; i++)
            {
                Console.Write("{0:X} ", SetRequestArr[i]);
            }
            Console.WriteLine();
            Console.Write("==========================================\n");
            int tries = 5;

            while (tries != 0)
            {
                try
                {
                    byte[] request = new byte[255];
                    int bytesRead = 0;

                    Console.WriteLine(">>> Sending data1"); // Will not Newer run 
                    byte[] eCMD = { 0x2b, 0x2b, 0x2b };
                    serial.Write(eCMD, 0, eCMD.Length);
                    Thread.Sleep(1000);
                    bytesRead = serial.Read(request, 0, serial.BytesToRead);
                    Console.Write(">>> Byte" + System.Text.Encoding.ASCII.GetString(request));
                    //Thread.Sleep(1000);

                    Console.WriteLine(">>> Sending eATAP");
                    byte[] eATAP = { 0x41, 0x54, 0x41, 0x50, 0x20, 0x31, 0xd, 0xa };
                    serial.Write(eATAP, 0, eATAP.Length);
                    Thread.Sleep(1000);
                    bytesRead = serial.Read(request, 0, serial.BytesToRead);
                    Console.Write(">>> Byte" + System.Text.Encoding.ASCII.GetString(request));
                    //Thread.Sleep(1000);

                    Console.WriteLine(">>> Sending eATRO");
                    byte[] eATRO = { 0x41, 0x54, 0x52, 0x4f, 0x20, 0x31, 0x30, 0x30, 0x30, 0xd, 0xa };
                    serial.Write(eATRO, 0, eATRO.Length);
                    Thread.Sleep(1000);
                    bytesRead = serial.Read(request, 0, serial.BytesToRead);
                    Console.Write(">>> Byte" + System.Text.Encoding.ASCII.GetString(request));
                    //Thread.Sleep(1000);

                    Console.WriteLine(">>> Sending eATCN");
                    byte[] eATCN = { 0x41, 0x54, 0x43, 0x4e, 0xd, 0xa };
                    serial.Write(eATCN, 0, eATCN.Length);
                    Thread.Sleep(1000);
                    bytesRead = serial.Read(request, 0, serial.BytesToRead);
                    Console.Write(">>> Byte" + System.Text.Encoding.ASCII.GetString(request));
                    //Thread.Sleep(1000);

                    Console.WriteLine(">>> Sending eEnter");
                    byte[] eEnter = { 0xd, 0xa };
                    serial.Write(eEnter, 0, eEnter.Length);
                    Thread.Sleep(1000);

                    serial.Write(SetRequestArr, 0, SetRequestArr.Length);
                    Thread.Sleep(1000);

                    Thread.Sleep(15000);
                    bytesRead = serial.Read(request, 0, serial.BytesToRead);
                    Console.WriteLine(">>> BytesRead : " + bytesRead);

                    for (int i = 0; i < bytesRead; i++)
                        Console.Write("Byte {0:X}", request[i]);

                    Console.WriteLine(">>> SerialRead succesfull");

                    if (bytesRead > 2)
                    {
                        Console.Write(">>> Byteread=" + bytesRead);
                        Console.Write(">>> SetRequestArr.Length=" + SetRequestArr.Length);
                        request = request.Take(bytesRead).Skip(bytesRead - 3).ToArray();

                        Console.WriteLine(">>> CrcHi : " + request[0]);
                        Console.WriteLine(">>> CrcLow : " + request[1]);
                        Console.WriteLine(">>> Third byte : " + request[2]);

                        if (request[0] == crcHi && request[1] == crcLow)
                        {
                            Console.WriteLine(">>> Write Succeeded!");
                            return true;
                        }
                    }
                    tries--;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(">>> TCER_44: " + ex.Message + " tries " + tries + " Request");
                    tries--;
                }
            }
            Console.WriteLine(">>> Reset RadioGpio");
            HardwareReset(RadioResetGPIO);
            Thread.Sleep(30000);
            return false;
        }

        public static String GetRadioSerial()
        {
            HardwareReset(RadioResetGPIO);
            String serialRadio = String.Empty;
            bool readFlag = false;

            while (!readFlag)
            {
                try
                {
                    if (!(serial.IsOpen)) serial.Open();

                    WriteCmd(serial, "+++");
                    serialRadio += WriteCmd(serial, "ATSH\n");
                    serialRadio += WriteCmd(serial, "ATSL\n");
                    WriteCmd(serial, "ATCN\n");
                    if (serialRadio != String.Empty) readFlag = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(">>> " + ex.Message + " RadioSerial");
                    HardwareReset(RadioResetGPIO);
                }
            }
            return serialRadio;
        }

        private static void SoftwareReset() // Программный сброс радиомодуля
        {
            try
            {
                WriteCmd(serial, "+++");
                WriteCmd(serial, "ATFR\n");
                WriteCmd(serial, "ATCN\n");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " SoftwareReset()");
                HardwareReset(RadioResetGPIO);
            }
        }

        private static void NetworkReset() // Сетевой сброс радиомодуля
        {
            try
            {
                WriteCmd(serial, "+++");
                WriteCmd(serial, "ATNR\n");
                WriteCmd(serial, "ATCN\n");
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " NetworkReset()");
                HardwareReset(RadioResetGPIO);
            }
        }

        public static String WriteCmd(SerialPort port, string cmd)
        {
            try
            {
                byte[] receive = new byte[16];

                serial.Write(cmd);
                Thread.Sleep(1000);
                int bytesRead = port.Read(receive, 0, receive.Length);

                Console.WriteLine(">>> Command to radio : " + cmd);
                Console.Write(">>> Radio responce : ");

                for (int i = 0; i < bytesRead; i++)
                {
                    Console.Write(Convert.ToChar(receive[i]));
                }

                receive = receive.Take(bytesRead - 2).ToArray();
                String answer = String.Empty;

                foreach (var a in receive)
                {
                    answer += Convert.ToChar(a);
                }
                return answer;
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_46: " + ex.Message + " WriteCmd");
                HardwareReset(RadioResetGPIO);
                return String.Empty;
            }
        }

        public static void HardwareReset(GPIO RadioResetGPIO)
        {
            if (USE_NOW == USERASPBERRY)
            {
                RadioResetGPIO.GPIOSetValue("0");
                Thread.Sleep(1000);
                RadioResetGPIO.GPIOSetValue("1");
                Thread.Sleep(10000);
            }
        }
        /////////

        /// <summary>
        /// START MY CODE
        /// </summary>
        /// 

        private static void setBrightnesGroupModulator(string SlaveAdress, string Group, string percents)
        {
            try
            {
                UInt32 g = (Convert.ToUInt32(Group)) + (Convert.ToUInt32(percents) << 16);

                byte[] k = BitConverter.GetBytes(g);

                byte adress = Convert.ToByte(SlaveAdress);
                byte[] Initiaze_pak = new byte[] { adress, 0x10, 0x00, 0x01, 0x00, 2, 4, k[1], k[0], k[3], k[2], 0x0, 0x0 };
                CalcCRC16(Initiaze_pak);

                WritePacket(serialModulator, Initiaze_pak);
            }
            catch (Exception ex)
            {
                Console.WriteLine("TCER_18: " + ex.Message);
            }
        }

        private static void setBrightnesModulator(string SlaveAdress, string lamp_address, string percents)
        {
            try
            {
                {
                    UInt32 g = (Convert.ToUInt32(lamp_address)) + (Convert.ToUInt32(percents) << 16);


                    byte[] k = BitConverter.GetBytes(g);

                    byte adress = Convert.ToByte(SlaveAdress);
                    byte[] Initiaze_pak = new byte[] { adress, 0x10, 0x00, 0x02, 0x00, 2, 4, k[1], k[0], k[3], k[2], 0x0, 0x0 };

                    CalcCRC16(Initiaze_pak);

                    WritePacket(serialModulator, Initiaze_pak);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_19: " + ex.Message);
            }
        }

        private static void AddLamptoGroupModulator(string LampID, string GroupID, string SlaveAdress)
        {
            try
            {
                UInt32 g = (Convert.ToUInt32(LampID)) + (Convert.ToUInt32(GroupID) << 16);

                byte[] k = BitConverter.GetBytes(g);

                byte adress = Convert.ToByte(SlaveAdress);
                byte[] Initiaze_pak = new byte[] { adress, 0x10, 0x00, 0x03, 0x00, 2, 4, k[1], k[0], k[3], k[2], 0x0, 0x0 };

                CalcCRC16(Initiaze_pak);

                WritePacket(serialModulator, Initiaze_pak);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_20: " + ex.Message);
            }
        }

        private static void DeleteLamptoGroupModulator(string LampID, string GroupID, string SlaveAdress)
        {
            try
            {
                UInt32 g = (Convert.ToUInt32(LampID)) + (Convert.ToUInt32(GroupID) << 16);

                byte[] k = BitConverter.GetBytes(g);

                byte adress = Convert.ToByte(SlaveAdress);
                byte[] Initiaze_pak = new byte[] { adress, 0x10, 0x00, 0x04, 0x00, 2, 4, k[1], k[0], k[3], k[2], 0x0, 0x0 };

                CalcCRC16(Initiaze_pak);

                WritePacket(serialModulator, Initiaze_pak);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_21: " + ex.Message);
            }
        }

        static List<List<string>> JobQueue;

        public class Nik_Data
        {
            public String SerialNumber = String.Empty;

            public double VoltagePhaseA = 0; public double VoltagePhaseB = 0; public double VoltagePhaseC = 0;
            public double CurrentPhaseA = 0; public double CurrentPhaseB = 0; public double CurrentPhaseC = 0;
            public double PowerPhaseA = 0; public double PowerPhaseB = 0; public double PowerPhaseC = 0;
            public double AveragePower = 0; public double SummaryPower = 0;

            public Nik_Data()
            {
                SerialNumber = String.Empty;
                VoltagePhaseA = 0; VoltagePhaseB = 0; VoltagePhaseC = 0;
                CurrentPhaseA = 0; CurrentPhaseB = 0; CurrentPhaseC = 0;
                PowerPhaseA = 0; PowerPhaseB = 0; PowerPhaseC = 0;
                AveragePower = 0; SummaryPower = 0;
            }

            public Nik_Data(double VoltagePhase,
            double VoltagePhaseA, double VoltagePhaseB, double VoltagePhaseC,
            double CurrentPhaseA, double CurrentPhaseB, double CurrentPhaseC,
            double PowerPhaseA, double PowerPhaseB, double PowerPhaseC,
            double AveragePower, double SummaryPower)
            {
                this.VoltagePhaseA = VoltagePhaseA;
                this.VoltagePhaseB = VoltagePhaseB;
                this.VoltagePhaseC = VoltagePhaseC;
                this.CurrentPhaseA = CurrentPhaseA;
                this.CurrentPhaseB = CurrentPhaseB;
                this.CurrentPhaseC = CurrentPhaseC;
                this.PowerPhaseA = PowerPhaseA;
                this.PowerPhaseB = PowerPhaseB;
                this.PowerPhaseC = PowerPhaseC;
                this.AveragePower = AveragePower;
                this.SummaryPower = SummaryPower;
            }

            public void SetData(double VoltagePhase,
            double VoltagePhaseA, double VoltagePhaseB, double VoltagePhaseC,
            double CurrentPhaseA, double CurrentPhaseB, double CurrentPhaseC,
            double PowerPhaseA, double PowerPhaseB, double PowerPhaseC,
            double AveragePower, double SummaryPower)
            {
                this.VoltagePhaseA = VoltagePhaseA;
                this.VoltagePhaseB = VoltagePhaseB;
                this.VoltagePhaseC = VoltagePhaseC;
                this.CurrentPhaseA = CurrentPhaseA;
                this.CurrentPhaseB = CurrentPhaseB;
                this.CurrentPhaseC = CurrentPhaseC;
                this.PowerPhaseA = PowerPhaseA;
                this.PowerPhaseB = PowerPhaseB;
                this.PowerPhaseC = PowerPhaseC;
                this.AveragePower = AveragePower;
                this.SummaryPower = SummaryPower;
            }
        }

        static List<List<Nik_Data>> NIK_Struct;

        static string PreviousCpommands = "";
        public static void ReceiveCommands()
        {
            TcpClient client = new TcpClient();
            NetworkStream stream;
            int bytesRead = 0;

            byte[] buffer = new byte[32];

            while (true)
            {
                try
                {
                    Console.WriteLine(">>> Trying to connect...");

                    client = new TcpClient(Host, Port); // Trying to Connect to server

                    Console.WriteLine(">>> Connected");

                    client.Client.SendTimeout = 5000;
                    client.Client.ReceiveTimeout = 5000;

                    stream = new NetworkStream(client.Client); // Get server stream

                    stream.ReadTimeout = 5000;
                    stream.WriteTimeout = 5000;

                    String initialString = "RaspID" + RaspberryID + "$RadioSerial" + RadioSerialNumber + "$NikAddress" + NIK_Address;

                    stream.Write(Encoding.ASCII.GetBytes(initialString), 0, Encoding.ASCII.GetBytes(initialString).Length); // Sending RaspberryID to Server
                }
                catch (Exception connectException)
                {
                    Console.WriteLine(connectException.Message + " receiveCommands()");
                    Thread.Sleep(TimeSpan.FromSeconds(5));
                    continue;  // resume trying to connect ot server
                }

                Console.WriteLine(">>> Sended RaspberryID - " + RaspberryID + " to : " + Host + " on port - " + Port);

                while (true)
                {
                    try
                    {
                        if (!(IsConnected(client.Client))) throw new Exception("Socket is not connected!");

                        client.Client.Blocking = false;
                        client.Client.Send(new byte[1], 0, 0);

                        if (stream.DataAvailable) // if stream isn't empty
                        {
                            bytesRead = stream.Read(buffer, 0, buffer.Length); // read buffer

                            if (!(bytesRead > 0)) continue; // if buffer is empty go to next reading

                            string cmd = Encoding.ASCII.GetString(buffer, 0, bytesRead); // convert bytes to string command

                            Console.WriteLine(cmd);
                            PreviousCpommands += cmd;
                            while (PreviousCpommands.Contains(";"))
                            {
                                string oneCommand = "";

                                oneCommand = PreviousCpommands.Substring(0, PreviousCpommands.IndexOf(";", 0));

                                string[] commands = oneCommand.Split('$');

                                PreviousCpommands = PreviousCpommands.Remove(0, (oneCommand + ";").Length);
                                List<string> g = new List<string>();

                                for (int i = 0; i < commands.Count(); i++)
                                {
                                    g.Add(commands[i]);
                                }
                                JobQueue.Add(g);
                            }
                        }
                        Thread.Sleep(300);
                    }
                    catch (Exception receiveException)
                    {
                        Console.WriteLine(">>> " + receiveException.Message + " receiveCommands()");
                        Thread.Sleep(2000);
                        stream.Close(); // close stream
                        client.Client.Close();
                        break; // begin trying to connect to server
                    }
                }
                client.Close();
                Thread.Sleep(10);
            }
        }

        public static void setBrToBaseGroup(string GroupID, string value)
        {
            try
            {
                string cs = @"server=localhost;server=localhost;userid=root;
                              password=raspberry;database=RaspDB";

                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                string sql1 = "Select *  From LampGroupsRelatives where GroupID= " + GroupID + ";";

                MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                MySqlDataReader rdr1 = cmd1.ExecuteReader();

                while (rdr1.Read())
                {
                    var hs = (rdr1.GetString(1));
                    setBrToBase(hs, value);
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_24: " + ex.Message);
            }
        }

        public static void setBrToBase(string stationEquipID, string value)
        {
            try
            {
                string cs = @"server=localhost;server=localhost;userid=root;
                              password=raspberry;database=RaspDB";

                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                string sql1 = "UPDATE LampForNIK SET Brightness = " + value +
                " where StationEquipmentID = " + stationEquipID + ";";

                MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                MySqlDataReader rdr1 = cmd1.ExecuteReader();

                while (rdr1.Read())
                {
                    var hs = (rdr1.GetString(0));
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_25: " + ex.Message);
            }
        }

        public static List<List<string>> getInformATION(string GroupID)
        {
            string cs = @"server=localhost;server=localhost;userid=root;
                          password=raspberry;database=RaspDB";

            MySqlConnection conn = new MySqlConnection(cs);
            conn.Open();

            string sql0 = "SELECT GroupID FROM LampGroups where ViewGroupID =" + GroupID + ";";

            MySqlCommand cmd0 = new MySqlCommand(sql0, conn);
            MySqlDataReader rdr0 = cmd0.ExecuteReader();

            int GrouID0 = 0;

            while (rdr0.Read())
            {
                GrouID0 = Convert.ToInt32(rdr0.GetString(0));
            }
            conn.Close();

            List<string> b = new List<string>();
            b.Add(GrouID0.ToString());

            /////
            //ТУПО ПОСЫЛАЮ НА ВСЕХ

            conn.Open();
            string sql01 = "Select * From StationEquipmentIdToSerialNumberSlaves;";

            MySqlCommand cmd01 = new MySqlCommand(sql01, conn);
            MySqlDataReader rdr01 = cmd01.ExecuteReader();

            List<List<string>> Job = new List<List<string>>();
            Job.Add(b);

            while (rdr01.Read())
            {
                List<string> bb = new List<string>();
                bb.Add(rdr01.GetString(2));
                bb.Add(rdr01.GetString(3));
                Job.Add(bb);
            }
            conn.Close();

            return Job;
        }

        public static List<string> getInform(int LampEquipmentID)
        {
            string cs = @"server=localhost;server=localhost;userid=root;
                          password=raspberry;database=RaspDB";

            MySqlConnection conn = new MySqlConnection(cs);
            conn.Open();

            string sql = "Select * From StationEquipmentIdToSerialNumber where StationEquipmentID  = " + LampEquipmentID + ";";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            string c1 = " ";
            string c2 = " ";
            string c3 = " ";

            while (rdr.Read())
            {
                c1 = rdr.GetString(1);
                c2 = rdr.GetString(2);
                c3 = rdr.GetString(3);
            }
            conn.Close();

            string sql1 = "Select * From StationEquipmentIdToSerialNumberSlaves where StationEquipmentID  = " + c3 + ";";
            conn.Open();

            MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
            MySqlDataReader rdr1 = cmd1.ExecuteReader();

            string cc1 = " ";
            string cc2 = " ";
            string EquipmentType = " ";

            while (rdr1.Read())
            {
                cc1 = rdr1.GetString(1);
                cc2 = rdr1.GetString(2);
                EquipmentType = rdr1.GetString(3);
            }

            conn.Close();

            List<string> s = new List<string>(); //LampDerive, SlaveDerive, SlaveAdress, SlaveType
            s.Add(c2);
            s.Add(cc1);
            s.Add(cc2);
            s.Add(EquipmentType);

            return s;
        }

        public static void JobQueueWorkControl()
        {
            while (true)
            {
                string cs = @"server=localhost;server=localhost;userid=root;
                              password=raspberry;database=RaspDB";
                // try
                {
                    Thread.Sleep(200);

                    if (JobQueue.Count > 0)
                    {

                    startDebug: List<string> s = JobQueue.First();
                        Console.WriteLine(s.First());

                        switch (s.First())
                        {
                            default:
                                {
                                    Console.WriteLine(">>> WTF is This+:" + JobQueue.First());
                                }
                                break;

                            case "DeleteLampinGroup":
                                {
                                    MySqlConnection conn = new MySqlConnection(cs);
                                    conn.Open();

                                    string sql0 = "SELECT GroupID FROM LampGroups where ViewGroupID =" + s.ElementAt(1) + ";";

                                    MySqlCommand cmd0 = new MySqlCommand(sql0, conn);
                                    MySqlDataReader rdr0 = cmd0.ExecuteReader();

                                    int GrouID = 0;

                                    while (rdr0.Read())
                                    {
                                        GrouID = Convert.ToInt32(rdr0.GetString(0));
                                    }
                                    conn.Close();

                                    conn.Open();

                                    string sql = "SELECT * FROM LampGroupsRelatives where GroupID = " + s.ElementAt(1) + " and StationEquipmentID =" + s.ElementAt(2) + ";";

                                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                                    MySqlDataReader rdr = cmd.ExecuteReader();

                                    bool exist = false;
                                    while (rdr.Read())
                                    {
                                        exist = true;
                                    }
                                    conn.Close();

                                    if (exist)
                                    {
                                        conn.Open();

                                        string sql1 = "DELETE FROM LampGroupsRelatives WHERE GroupID = " + s.ElementAt(1) + " and StationEquipmentID=" + s.ElementAt(2) + ";";

                                        MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                        MySqlDataReader rdr1 = cmd1.ExecuteReader();

                                        while (rdr1.Read())
                                        {

                                        }
                                        conn.Close();

                                        //LampDerive, SlaveDerive, SlaveAdress, SlaveType
                                        List<string> inform = getInform(Convert.ToInt32(s.ElementAt(2)));

                                        if (Convert.ToInt32(inform.ElementAt(3)) == 20)// Мой Модулятор
                                        {
                                            DeleteLamptoGroupModulator(s.ElementAt(2), GrouID.ToString(), inform.ElementAt(2));
                                            Thread.Sleep(8000);
                                        }
                                        if (Convert.ToInt32(inform.ElementAt(3)) == 21)// Сереги Радиомодуль
                                        {
                                            /// СЕРЕГА, СЮДА ПИСАТЬ ТВОЙ КОД
                                            lock (commandsLocker)
                                            {
                                                //DeleteLampInGroup-StationEquipmentID-GROUP_ID
                                                SetCommands.Add(new ServerCommands("DeleteLampFromGroup-" + s.ElementAt(2) + "-" + GrouID.ToString(), 0, 0));
                                            }
                                        }
                                    }
                                    JobQueue.RemoveAt(0);
                                }
                                break;

                            case "DeleteGroup":
                                {
                                    MySqlConnection conn = new MySqlConnection(cs);
                                    conn.Open();

                                    string sql0 = "SELECT GroupID FROM LampGroups where ViewGroupID =" + s.ElementAt(1) + ";";

                                    MySqlCommand cmd0 = new MySqlCommand(sql0, conn);
                                    MySqlDataReader rdr0 = cmd0.ExecuteReader();

                                    int GrouID0 = 0;

                                    while (rdr0.Read())
                                    {
                                        GrouID0 = Convert.ToInt32(rdr0.GetString(0));
                                    }
                                    conn.Close();

                                    if (GrouID0 != 0)//Если она есть
                                    {
                                        conn.Open();
                                        string sql1 = "DELETE FROM LampGroups WHERE ViewGroupID = " + s.ElementAt(1) + ";";

                                        MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                        MySqlDataReader rdr1 = cmd1.ExecuteReader();

                                        while (rdr1.Read())
                                        {
                                        }
                                        conn.Close();
                                    }
                                    JobQueue.RemoveAt(0);
                                }
                                break;

                            case "GsBrightness":
                                {
                                    List<List<string>> inform = getInformATION(s.ElementAt(1));
                                    {
                                        string groupID = inform.First().First();
                                        for (int i = 1; i < inform.Count; i++)
                                        {
                                            if (inform.ElementAt(i).ElementAt(1) == "20")// Сереги Радиомодуль
                                            {
                                                inform.ElementAt(i).ElementAt(0);
                                                setBrightnesGroupModulator(inform.ElementAt(i).ElementAt(0), groupID, s.ElementAt(2));
                                            }
                                            if (Convert.ToInt32(inform.ElementAt(i).ElementAt(1)) == 21)// Сереги Радиомодуль
                                            {
                                                /// СЕРЕГА, СЮДА ПИСАТЬ ТВОЙ КОД
                                                lock (commandsLocker)
                                                {
                                                    // GsBrigghtness-GROUP_ID-percents
                                                    SetCommands.Add(new ServerCommands("GSBrigghtness-" + groupID + "-" + s.ElementAt(2), 0, 0));// if we receive command to change PWM
                                                }
                                            }
                                        }
                                        setBrToBaseGroup(s.ElementAt(1), s.ElementAt(2));

                                        Thread.Sleep(8000);

                                        JobQueue.RemoveAt(0);
                                    }
                                }
                                break;
                            case "GON":
                                {
                                    List<List<string>> inform = getInformATION(s.ElementAt(1));
                                    {
                                        string groupID = inform.First().First();
                                        for (int i = 1; i < inform.Count; i++)
                                        {
                                            if (inform.ElementAt(i).ElementAt(1) == "20")// Сереги Радиомодуль
                                            {
                                                inform.ElementAt(i).ElementAt(0);
                                                setBrightnesGroupModulator(inform.ElementAt(i).ElementAt(0), groupID, "100");
                                            }
                                            if (Convert.ToInt32(inform.ElementAt(i).ElementAt(1)) == 21)// Сереги Радиомодуль
                                            {
                                                lock (commandsLocker)
                                                {
                                                    // GON-GROUP_ID_1 *** 1 - Mean that parameter on = 1 = enable
                                                    SetCommands.Add(new ServerCommands("Gon-" + groupID + "-" + "1", 0, 0));
                                                }
                                                /// СЕРЕГА, СЮДА ПИСАТЬ ТВОЙ КОД
                                            }
                                        }
                                        setBrToBaseGroup(s.ElementAt(1), "100");

                                        Thread.Sleep(8000);
                                        JobQueue.RemoveAt(0);
                                    }
                                }
                                break;
                            case "GOFF":
                                {
                                    List<List<string>> inform = getInformATION(s.ElementAt(1));
                                    {
                                        string groupID = inform.First().First();
                                        for (int i = 1; i < inform.Count; i++)
                                        {
                                            if (inform.ElementAt(i).ElementAt(1) == "20")// Сереги Радиомодуль
                                            {
                                                inform.ElementAt(i).ElementAt(0);
                                                setBrightnesGroupModulator(inform.ElementAt(i).ElementAt(0), groupID, "0");
                                            }
                                            if (Convert.ToInt32(inform.ElementAt(i).ElementAt(1)) == 21)// Сереги Радиомодуль
                                            {
                                                lock (commandsLocker)
                                                {
                                                    // GOFF-GROUP_ID_1 *** 1 - Mean that parameter off = 1 = enable
                                                    SetCommands.Add(new ServerCommands("Goff-" + groupID + "-" + "1", 0, 0));
                                                }
                                                /// СЕРЕГА, СЮДА ПИСАТЬ ТВОЙ КОД
                                            }
                                        }
                                        setBrToBaseGroup(s.ElementAt(1), "0");

                                        Thread.Sleep(8000);
                                        JobQueue.RemoveAt(0);
                                    }
                                }
                                break;

                            case "AddSlave":
                                {
                                    try
                                    {
                                        MySqlConnection conn = new MySqlConnection(cs);
                                        conn.Open();

                                        {
                                            string sql = "Select * From StationEquipmentIdToSerialNumberSlaves where StationEquipmentID  = " + s.ElementAt(1) + ";";

                                            MySqlCommand cmd = new MySqlCommand(sql, conn);
                                            MySqlDataReader rdr = cmd.ExecuteReader();

                                            int existALL = 0;
                                            bool ExistID = false;

                                            while (rdr.Read())
                                            {
                                                if ((rdr.GetString(0)) == s.ElementAt(1))
                                                {
                                                    existALL++; ExistID = true;
                                                }
                                                if ((rdr.GetString(1)) == s.ElementAt(2))
                                                {
                                                    existALL++;
                                                }
                                                if ((rdr.GetString(2)) == s.ElementAt(3))
                                                {
                                                    existALL++;
                                                }
                                                if ((rdr.GetString(3)) == s.ElementAt(4))
                                                {
                                                    existALL++;
                                                }
                                                //  StationEquipmentID = rdr.GetString(0); // getting StationEquipmentID wich applies our received SerialNumber
                                            }
                                            conn.Close();

                                            if (!ExistID)  //Если нет то добавить
                                            {
                                                string sql1 = "INSERT INTO StationEquipmentIdToSerialNumberSlaves VALUES(" +
                                                        s.ElementAt(1) + ", '" + s.ElementAt(2) + "'," + s.ElementAt(3) + "," + s.ElementAt(4) + ");";
                                                conn.Open();
                                                MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                                MySqlDataReader rdr1 = cmd1.ExecuteReader();
                                                while (rdr1.Read())
                                                {
                                                    var hs = (rdr1.GetString(0));
                                                }
                                                conn.Close();
                                            }
                                            else
                                            {
                                                if (existALL != 4) // если не сходится то обновить
                                                {
                                                    string sql1 = "UPDATE StationEquipmentIdToSerialNumberSlaves SET " +
                                                           "SerialNumber = '" + s.ElementAt(2) +
                                                           "',DeviceAdress = " + s.ElementAt(3) +
                                                           ",EquipmentTypeID = " + s.ElementAt(4) +
                                                           " where StationEquipmentID = " + s.ElementAt(1) + ";";

                                                    conn.Open();
                                                    MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                                    MySqlDataReader rdr1 = cmd1.ExecuteReader();

                                                    while (rdr1.Read())
                                                    {
                                                        var hs = (rdr1.GetString(0));
                                                    }
                                                    conn.Close();
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine(">>> TCER_28: " + ex.Message);
                                    }

                                    Thread.Sleep(80);

                                    JobQueue.RemoveAt(0);
                                }

                                break;
                            case "AddLamp":
                                {
                                    try
                                    {
                                        MySqlConnection conn = new MySqlConnection(cs);
                                        conn.Open();

                                        string sql0 = "Select * From StationEquipmentIdToSerialNumberSlaves where StationEquipmentId  = " + s.ElementAt(2) + ";";

                                        MySqlCommand cmd0 = new MySqlCommand(sql0, conn);
                                        MySqlDataReader rdr0 = cmd0.ExecuteReader();

                                        string EquipType = "";
                                        while (rdr0.Read())
                                        {
                                            EquipType = rdr0.GetString(3);
                                        }
                                        conn.Close();
                                        conn.Open();

                                        if (EquipType == "22")
                                        {
                                            /*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-*/
                                            string sql = "Select * From LampForNIK where StationEquipmentID  = " + s.ElementAt(1) + ";";

                                            MySqlCommand cmd = new MySqlCommand(sql, conn);
                                            MySqlDataReader rdr = cmd.ExecuteReader();

                                            int existALL = 0;
                                            bool ExistID = false;
                                            while (rdr.Read())
                                            {
                                                if ((rdr.GetString(1)) == s.ElementAt(1)) // StationEquipmentID
                                                {
                                                    existALL++; ExistID = true;
                                                }
                                                if ((rdr.GetString(2)) == s.ElementAt(3)) //SerialNumber
                                                {
                                                    existALL++;
                                                }
                                                if ((rdr.GetString(3)) == s.ElementAt(2)) //SlaveID
                                                {
                                                    existALL++;
                                                }
                                                if ((rdr.GetString(5)) == s.ElementAt(4)) //SlaveID
                                                {
                                                    existALL++;
                                                }
                                                if ((rdr.GetString(8)) == s.ElementAt(5)) //SlaveID
                                                {
                                                    existALL++;
                                                }
                                                //  StationEquipmentID = rdr.GetString(0); // getting StationEquipmentID wich applies our received SerialNumber
                                            }
                                            conn.Close();

                                            if (!ExistID)  //Если нет то добавить
                                            {

                                                /*
                                                ID int(11) NOT NULL AUTO_INCREMENT
                                                StationEquipmentID int(11)
                                                SerialNumber varchar(255)
                                                SlaveID int(11)
                                                Brightness int(11)
                                                PhaseNumber int(11)
                                                SummaryPover int(11)
                                                CurrentPover int(11)
                                                */

                                                string sql1 = "INSERT INTO LampForNIK VALUES(" + "null" + "," +
                                                    s.ElementAt(1) + ", '" + s.ElementAt(3) + "'," + s.ElementAt(2) + "," + 0 + "," + s.ElementAt(4) + "," + 0 + "," + 0 + "," + 0 + "," + 0 + ");";
                                                conn.Open();
                                                MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                                MySqlDataReader rdr1 = cmd1.ExecuteReader();
                                                while (rdr1.Read())
                                                {
                                                    var hs = (rdr1.GetString(0));
                                                }
                                                conn.Close();
                                            }
                                            else
                                            {
                                                if (existALL != 5) // если не сходится то обновить
                                                {
                                                    string sql1 = "UPDATE LampForNIK SET " +
                                                           "SerialNumber = '" + s.ElementAt(3) +
                                                           "',SlaveID = " + s.ElementAt(2) +
                                                           ",PhaseNumber = " + s.ElementAt(4) +
                                                           ",Nominal = " + s.ElementAt(5) +
                                                           " where StationEquipmentID = " + s.ElementAt(1) + ";";
                                                    conn.Open();
                                                    MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                                    MySqlDataReader rdr1 = cmd1.ExecuteReader();
                                                    while (rdr1.Read())
                                                    {
                                                        var hs = (rdr1.GetString(0));
                                                    }
                                                    conn.Close();
                                                }
                                            }
                                            /*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-*/
                                        }
                                        else
                                        {
                                            string sql = "Select * From StationEquipmentIdToSerialNumber where StationEquipmentID  = " + s.ElementAt(1) + ";";

                                            MySqlCommand cmd = new MySqlCommand(sql, conn);
                                            MySqlDataReader rdr = cmd.ExecuteReader();

                                            int existALL = 0;
                                            bool ExistID = false;
                                            while (rdr.Read())
                                            {
                                                if ((rdr.GetString(1)) == s.ElementAt(1))
                                                {
                                                    existALL++; ExistID = true;
                                                }
                                                if ((rdr.GetString(2)) == s.ElementAt(3))
                                                {
                                                    existALL++;
                                                }
                                                if ((rdr.GetString(3)) == s.ElementAt(2))
                                                {
                                                    existALL++;
                                                }

                                                //  StationEquipmentID = rdr.GetString(0); // getting StationEquipmentID wich applies our received SerialNumber
                                            }
                                            conn.Close();

                                            if (!ExistID)  //Если нет то добавить
                                            {
                                                string sql1 = "INSERT INTO StationEquipmentIdToSerialNumber VALUES( NULL, " +
                                                    s.ElementAt(1) + ", '" + s.ElementAt(3) + "'," + s.ElementAt(2) + ");";

                                                conn.Open();

                                                MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                                MySqlDataReader rdr1 = cmd1.ExecuteReader();

                                                while (rdr1.Read())
                                                {
                                                    var hs = (rdr1.GetString(0));
                                                }
                                                conn.Close();
                                            }
                                            else
                                            {
                                                if (existALL != 3) // если не сходится то обновить
                                                {
                                                    string sql1 = "UPDATE StationEquipmentIdToSerialNumber SET " +
                                                           "StationEquipmentID  = " + s.ElementAt(1) +
                                                           ",SerialNumber  = '" + s.ElementAt(3) +
                                                           "',SlaveID  = " + s.ElementAt(2) +
                                                           " where StationEquipmentID = " + s.ElementAt(1) + ";";

                                                    conn.Open();

                                                    MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                                    MySqlDataReader rdr1 = cmd1.ExecuteReader();
                                                    while (rdr1.Read())
                                                    {
                                                        var hs = (rdr1.GetString(0));
                                                    }
                                                    conn.Close();
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine(">>> TCER_29: " + ex.Message);
                                    }

                                    Thread.Sleep(80);

                                    JobQueue.RemoveAt(0);
                                }
                                break;
                            case "sBrightness":
                                {
                                    //LampDerive, SlaveDerive, SlaveAdress, SlaveType
                                    List<string> inform = getInform(Convert.ToInt32(s.ElementAt(1)));

                                    if (Convert.ToInt32(inform.ElementAt(3)) == 20)// Мой Модулятор
                                    {
                                        setBrightnesModulator(inform.ElementAt(2), inform.ElementAt(0), s.ElementAt(2));
                                        Thread.Sleep(8000);
                                    }

                                    if (Convert.ToInt32(inform.ElementAt(3)) == 21)// Сереги Радиомодуль
                                    {
                                        lock (commandsLocker)
                                        {
                                            SetCommands.Add(new ServerCommands("sBrightness-" + s.ElementAt(1) + "-" + s.ElementAt(2), 0, 0));// if we receive command to change PWM
                                        }
                                        /// СЕРЕГА, СЮДА ПИСАТЬ ТВОЙ КОД
                                    }
                                    setBrToBase(s.ElementAt(1), s.ElementAt(2));
                                    JobQueue.RemoveAt(0);
                                }
                                break;

                            case "ON":
                                {
                                    //LampDerive, SlaveDerive, SlaveAdress, SlaveType
                                    List<string> inform = getInform(Convert.ToInt32(s.ElementAt(2)));

                                    if (Convert.ToInt32(inform.ElementAt(3)) == 20)// Мой Модулятор
                                    {
                                        setBrightnesModulator(inform.ElementAt(2), inform.ElementAt(0), "100");
                                        Thread.Sleep(8000);
                                    }
                                    if (Convert.ToInt32(inform.ElementAt(3)) == 21)// Сереги Радиомодуль
                                    {
                                        lock (commandsLocker)
                                        {
                                            SetCommands.Add(new ServerCommands("ON-1-" + s.ElementAt(2), 0, 0));
                                        }
                                        /// СЕРЕГА, СЮДА ПИСАТЬ ТВОЙ КОД
                                    }
                                    setBrToBase(s.ElementAt(1), "100");

                                    JobQueue.RemoveAt(0);
                                }
                                break;

                            case "OFF":
                                {
                                    //LampDerive, SlaveDerive, SlaveAdress, SlaveType
                                    List<string> inform = getInform(Convert.ToInt32(s.ElementAt(2)));

                                    if (Convert.ToInt32(inform.ElementAt(3)) == 20)// Мой Модулятор
                                    {
                                        setBrightnesModulator(inform.ElementAt(2), inform.ElementAt(0), "0");
                                        Thread.Sleep(8000);
                                    }
                                    if (Convert.ToInt32(inform.ElementAt(3)) == 21)// Сереги Радиомодуль
                                    {
                                        lock (commandsLocker)
                                        {
                                            SetCommands.Add(new ServerCommands("OFF-1-" + s.ElementAt(2), 0, 0));
                                        }
                                        /// СЕРЕГА, СЮДА ПИСАТЬ ТВОЙ КОД
                                    }
                                    setBrToBase(s.ElementAt(1), "0");

                                    JobQueue.RemoveAt(0);
                                }
                                break;
                            case "AddGroup":
                                {
                                    MySqlConnection conn = new MySqlConnection(cs);
                                    conn.Open();

                                    string sql0 = "SELECT GroupID FROM LampGroups where ViewGroupID =" + s.ElementAt(1) + ";";

                                    MySqlCommand cmd0 = new MySqlCommand(sql0, conn);
                                    MySqlDataReader rdr0 = cmd0.ExecuteReader();

                                    int GrouID0 = 0;
                                    while (rdr0.Read())
                                    {
                                        GrouID0 = Convert.ToInt32(rdr0.GetString(0));
                                    }
                                    conn.Close();

                                    if (GrouID0 == 0)//Если ент группы еще
                                    {
                                        conn.Open();

                                        string sql = "SELECT GroupID FROM LampGroups where ViewGroupID is null LIMIT 1;";

                                        MySqlCommand cmd = new MySqlCommand(sql, conn);
                                        MySqlDataReader rdr = cmd.ExecuteReader();

                                        int GrouID = 0;
                                        while (rdr.Read())
                                        {
                                            GrouID = Convert.ToInt32(rdr.GetString(0));
                                        }
                                        conn.Close();

                                        if (GrouID != 0) // Иначе кончились свободніе групппы для фонарей
                                        {
                                            conn.Open();
                                            string sql1 = "UPDATE LampGroups SET ViewGroupID = " + s.ElementAt(1) + " WHERE GroupID = " + GrouID + ";";

                                            MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                            MySqlDataReader rdr1 = cmd1.ExecuteReader();

                                            while (rdr1.Read())
                                            {
                                            }
                                            conn.Close();
                                        }
                                    }
                                    JobQueue.RemoveAt(0);
                                }
                                break;

                            case "AddLamptoGroup":
                                {

                                    MySqlConnection conn = new MySqlConnection(cs);
                                    conn.Open();

                                    string sql0 = "SELECT GroupID FROM LampGroups where ViewGroupID =" + s.ElementAt(1) + ";";

                                    MySqlCommand cmd0 = new MySqlCommand(sql0, conn);
                                    MySqlDataReader rdr0 = cmd0.ExecuteReader();

                                    int GrouID = 0;
                                    while (rdr0.Read())
                                    {
                                        GrouID = Convert.ToInt32(rdr0.GetString(0));
                                    }
                                    conn.Close();

                                    conn.Open();

                                    string sql = "SELECT * FROM LampGroupsRelatives where GroupID = " + s.ElementAt(1) + " and StationEquipmentID =" + s.ElementAt(2) + ";";

                                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                                    MySqlDataReader rdr = cmd.ExecuteReader();

                                    bool exist = false;
                                    while (rdr.Read())
                                    {
                                        exist = true;
                                    }
                                    conn.Close();

                                    if (!exist)
                                    {
                                        conn.Open();
                                        string sql1 = "INSERT INTO LampGroupsRelatives VALUES(" + s.ElementAt(1) + ", " + s.ElementAt(2) + "); ";

                                        MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                        MySqlDataReader rdr1 = cmd1.ExecuteReader();

                                        while (rdr1.Read())
                                        {
                                        }
                                        conn.Close();

                                        //LampDerive, SlaveDerive, SlaveAdress, SlaveType
                                        List<string> inform = getInform(Convert.ToInt32(s.ElementAt(2)));

                                        if (Convert.ToInt32(inform.ElementAt(3)) == 20)// Мой Модулятор
                                        {
                                            AddLamptoGroupModulator(s.ElementAt(2), GrouID.ToString(), inform.ElementAt(2));
                                            Thread.Sleep(8000);
                                        }
                                        if (Convert.ToInt32(inform.ElementAt(3)) == 21)// Сереги Радиомодуль
                                        {
                                            lock (commandsLocker)
                                            {
                                                //AddLampToGroup-StationEquipmentID-GROUP_ID
                                                SetCommands.Add(new ServerCommands("AddLampToGroup-" + s.ElementAt(2) + "-" + GrouID.ToString(), 0, 0));// if we receive command to change PWM
                                            }
                                            /// СЕРЕГА, СЮДА ПИСАТЬ ТВОЙ КОД
                                        }
                                    }
                                    JobQueue.RemoveAt(0);

                                }
                                break;
                        }
                    }
                }
                // catch (Exception ex)
                {
                    //   Console.WriteLine("TCER_50: "+ex.Message);
                }
            }
        }

        public static void Analitica()
        {
            while (true)
            {
                Thread.Sleep(5000);
            }
        }

        static int? Search<T>(T[] array, int left, int right, T key) where T : IComparable<T>
        {
            int middle = -1;
            while (right >= left)
            {
                middle = (left + right) / 2;

                int comp = array[middle].CompareTo(key);
                if (comp > 0)
                {
                    right = middle - 1;
                }
                else if (comp < 0)
                {
                    left = middle + 1;
                }
                else
                {
                    return middle;
                }
            }
            return middle;
        }

        static void Main(string[] args)
        {
            JobQueue = new List<List<string>>();

            serial.ReadTimeout = 5000;
            serial.WriteTimeout = 5000;

            if (USE_NOW == USERASPBERRY)
            {
                if (!(serial.IsOpen)) serial.Open();
            }

            if (USE_NOW == USERASPBERRY)
            {
                //serialModulator = new SerialPort("dev/ttyUSB0", 9600, Parity.Even, 8); 
            }
            else
            {
                //serialModulator = new SerialPort("COM4", 9600, Parity.Even, 8);
            }

            //serialModulator.ReadTimeout = 5000;
            //serialModulator.WriteTimeout = 5000;

            //if (!(serialModulator.IsOpen)) serialModulator.Open();

            if (USE_NOW == USERASPBERRY)
            {
                RadioResetGPIO = GPIO.Export("2", "out");
                HardwareReset(RadioResetGPIO);
                ReadConfiguration(File.ReadAllText("/usr/local/bin/Configuration.xml"));
                Thread.Sleep(30000);
            }
            else
            {
                ReadConfiguration(File.ReadAllText("Configuration.xml"));
            }

            Thread controllThread = new Thread(unused => ReceiveCommands()); // Thread for receiving control commands from server
            controllThread.IsBackground = true;
            controllThread.Start();

            Thread sendDataThread = new Thread(unused => SendData()); // Thread for sending collecting data to server
            sendDataThread.IsBackground = true;
            sendDataThread.Start();

            Thread radioThread = new Thread(unused => RadioWork()); // Thread for sending collecting data to server
            radioThread.IsBackground = true;

            if (USE_NOW == USERASPBERRY)
            {
                radioThread.Start();
            }

            Thread JobQueueControl = new Thread(unused => JobQueueWorkControl()); // Thread for receiving control commands from server
            JobQueueControl.IsBackground = true;
            JobQueueControl.Start();

            Thread Analitik = new Thread(unused => Analitica()); // Thread for receiving control commands from server
            Analitik.IsBackground = true;
            Analitik.Start();


            while (true)
            {
                Thread.Sleep(10);
            }
        }
    }
}
