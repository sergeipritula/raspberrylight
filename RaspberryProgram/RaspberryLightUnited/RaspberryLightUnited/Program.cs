﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Net.Sockets;
using System.Xml;
using MySql.Data.MySqlClient;

namespace RaspberryLightUnited
{
    public class ServerCommands
    {
        public String serialNumber = String.Empty;
        public String command = String.Empty;
        public int priority = 0;
        public int commandsCount = 0;
        public bool commandStatus = false;
        public short ID = 0;
        public short register = 0;
        public short value = 0;
        public byte functionCode = 0;
        public bool GroupFlag = false;

        public ServerCommands()
        {
            command = String.Empty;
            priority = 0;
            commandsCount = 0;
            commandStatus = false;
            ID = 0;
            value = 0;
            register = 0;
        }

        public ServerCommands(String command, int priority, int commandsCount)
        {
            this.command = command;
            this.priority = priority;
            this.commandsCount = commandsCount;
            commandStatus = false;
        }

        public ServerCommands(String command, int priority, int commandsCount, short ID, short register, short value, byte functionCode, bool GroupFlag)
        {
            this.command = command;
            this.priority = priority;
            this.commandsCount = commandsCount;
            this.ID = ID;
            this.register = register;
            this.value = value;
            this.functionCode = functionCode;
            this.GroupFlag = GroupFlag;
            commandStatus = GroupFlag;
        }

        public ServerCommands(String command, int priority, int commandsCount, short ID, short register, short value, byte functionCode, bool GroupFlag, String serialNumber)
        {
            this.command = command;
            this.priority = priority;
            this.commandsCount = commandsCount;
            this.ID = ID;
            this.register = register;
            this.value = value;
            this.functionCode = functionCode;
            this.GroupFlag = GroupFlag;
            this.serialNumber = serialNumber;
            commandStatus = false;
        }

        public ServerCommands(ServerCommands cmd)
        {
            command = cmd.command;
            priority = cmd.priority;
            commandsCount = cmd.commandsCount;
            commandStatus = cmd.commandStatus;
            ID = cmd.ID;
            register = cmd.register;
            value = cmd.value;
            functionCode = cmd.functionCode;
            GroupFlag = cmd.GroupFlag;
            serialNumber = cmd.serialNumber;
        }

        public static List<ServerCommands> SortByPriority(List<ServerCommands> commands)
        {
            commands.Sort((x, y) => x.priority.CompareTo(y.commandsCount).CompareTo(x.functionCode)); // Sort commands in List by priority
            return commands;
        }
    }

    public class Nik_Data
    {
        public String SerialNumber = String.Empty;

        public double VoltagePhaseA = 0; public double VoltagePhaseB = 0; public double VoltagePhaseC = 0;
        public double CurrentPhaseA = 0; public double CurrentPhaseB = 0; public double CurrentPhaseC = 0;
        public double PowerPhaseA = 0; public double PowerPhaseB = 0; public double PowerPhaseC = 0;
        public double AveragePower = 0; public double SummaryPower = 0;

        public Nik_Data()
        {
            SerialNumber = String.Empty;
            VoltagePhaseA = 0; VoltagePhaseB = 0; VoltagePhaseC = 0;
            CurrentPhaseA = 0; CurrentPhaseB = 0; CurrentPhaseC = 0;
            PowerPhaseA = 0; PowerPhaseB = 0; PowerPhaseC = 0;
            AveragePower = 0; SummaryPower = 0;
        }

        public Nik_Data(double VoltagePhase,
        double VoltagePhaseA, double VoltagePhaseB, double VoltagePhaseC,
        double CurrentPhaseA, double CurrentPhaseB, double CurrentPhaseC,
        double PowerPhaseA, double PowerPhaseB, double PowerPhaseC,
        double AveragePower, double SummaryPower)
        {
            this.VoltagePhaseA = VoltagePhaseA;
            this.VoltagePhaseB = VoltagePhaseB;
            this.VoltagePhaseC = VoltagePhaseC;
            this.CurrentPhaseA = CurrentPhaseA;
            this.CurrentPhaseB = CurrentPhaseB;
            this.CurrentPhaseC = CurrentPhaseC;
            this.PowerPhaseA = PowerPhaseA;
            this.PowerPhaseB = PowerPhaseB;
            this.PowerPhaseC = PowerPhaseC;
            this.AveragePower = AveragePower;
            this.SummaryPower = SummaryPower;
        }

        public void SetData(double VoltagePhase,
        double VoltagePhaseA, double VoltagePhaseB, double VoltagePhaseC,
        double CurrentPhaseA, double CurrentPhaseB, double CurrentPhaseC,
        double PowerPhaseA, double PowerPhaseB, double PowerPhaseC,
        double AveragePower, double SummaryPower)
        {
            this.VoltagePhaseA = VoltagePhaseA;
            this.VoltagePhaseB = VoltagePhaseB;
            this.VoltagePhaseC = VoltagePhaseC;
            this.CurrentPhaseA = CurrentPhaseA;
            this.CurrentPhaseB = CurrentPhaseB;
            this.CurrentPhaseC = CurrentPhaseC;
            this.PowerPhaseA = PowerPhaseA;
            this.PowerPhaseB = PowerPhaseB;
            this.PowerPhaseC = PowerPhaseC;
            this.AveragePower = AveragePower;
            this.SummaryPower = SummaryPower;
        }
    }

    public class GPIO
    {
        public readonly static string DIRECTION_IN = "in";
        public readonly static string DIRECTION_OUT = "out";
        public readonly static string PIN_TO_HIGH = "1";
        public readonly static string PIN_TO_LOW = "0";
        public string GPIOPin;


        public GPIO() { }

        public GPIO(string gnum)
        {
            GPIOPin = gnum;
        }

        public int GPIOExport()
        {
            string export_str = "/sys/class/gpio/export";
            if (File.Exists(export_str)) File.WriteAllText(export_str, GPIOPin);
            else throw new FileNotFoundException();
            return 0;
        }

        public int GPIOUnexport()
        {
            string unexport_str = "/sys/class/gpio/unexport";
            if (File.Exists(unexport_str)) File.WriteAllText(unexport_str, GPIOPin);
            else throw new FileNotFoundException();
            return 0;
        }

        public int GPIOSetDirection(string dir)
        {
            string setdir_str = "/sys/class/gpio/gpio" + GPIOPin + "/direction";
            if (File.Exists(setdir_str)) File.WriteAllText(setdir_str, dir);
            else throw new FileNotFoundException();
            return 0;
        }

        public int GPIOSetValue(string val)
        {
            string setval_str = "/sys/class/gpio/gpio" + GPIOPin + "/value";
            if (File.Exists(setval_str)) File.WriteAllText(setval_str, val);
            else throw new FileNotFoundException();
            return 0;
        }

        public int GPIOGetValue()
        {
            string getval_str = "/sys/class/gpio/gpio" + GPIOPin + "/value";
            string val = String.Empty;
            if (File.Exists(getval_str)) val = File.ReadAllText(getval_str);
            else throw new FileNotFoundException();
            return Convert.ToInt32(val);
        }

        public static GPIO Export(string pin, string direction)
        {
            GPIO gpio = new GPIO(pin);
            try { gpio.GPIOExport(); }
            catch (System.IO.IOException)
            {
                try
                {
                    Thread.Sleep(200);
                    gpio.GPIOUnexport();
                    Thread.Sleep(200);
                    gpio.GPIOExport();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(">>> GPIO Export failed + " + ex.Message);
                }
            }
            gpio.GPIOSetDirection(direction);
            return gpio;
        }
    }

    public class Nik
    {
        private const int NIK_EQUIPMENT_TYPE_ID = 22;
        private const int VOLTAGE_OBIS_CODE = 97;
        private const int CURRENT_OBIS_CODE = 3;
        private const int POWER_OBIS_CODE = 2;
        private const int ACTIVE_POWER_OBIS_CODE = 85;
        private const int SUMMARY_POWER_OBIS_CODE = 42;

        private String SerialNumber;
        private byte[] receive;

        private byte[] SNRM = { 0x7e, 0xa0, 0x0a, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x93, 0x06, 0x13, 0x7e }; //frame SNRM
        private byte[] AARQ = { 0x7e, 0xa0, 0x50, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x0d, 0x3e, 0xe6, 0xe6, 0x00,
                                0x60, 0x3f ,0xa1, 0x09, 0x06, 0x07, 0x60, 0x85, 0x74, 0x05, 0x08, 0x01, 0x01, 0x8a,
                                0x02, 0x07, 0x80 ,0x8b ,0x07, 0x60, 0x85, 0x74, 0x05, 0x08, 0x02, 0x01, 0xac, 0x12,
                                0x80, 0x11, 0x00, 0x31, 0x31, 0x31 ,0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31,
                                0x31, 0x31, 0x31, 0x31, 0x31, 0xbe, 0x10, 0x04 ,0x0e, 0x01, 0x00, 0x00, 0x00, 0x06,
                                0x5f, 0x1f, 0x04, 0x00, 0x00, 0x08, 0xcb, 0x00, 0x80, 0x9c ,0x46, 0x7e }; // AARQ request for authorization by User with passord

        private byte[] DISC = { 0x7e, 0xa0, 0x0a, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x53, 0x0a, 0xd5, 0x7e }; // frame DISC  

        //Requests for Parameters
        private byte[] Request_VoltagePhaseA = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                                 0x48, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                                 0x00, 0x20, 0x07, 0x00, 0x00, 0x02, 0x00, 0x73, 0x88, 0x7e };

        private byte[] Request_VoltagePhaseB = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x93,
                                                 0x17, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                                 0x00, 0x34, 0x07, 0x00, 0x00, 0x02, 0x00, 0x6f, 0xda, 0x7e };

        private byte[] Request_VoltagePhaseC = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                                 0x48, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                                 0x00, 0x48, 0x07, 0x00, 0x00, 0x02, 0x00, 0x9a, 0x2e, 0x7e };

        private byte[] Request_CurrentPhaseA = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                                 0x48, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                                 0x00, 0x1f, 0x07, 0x00, 0x00, 0x02, 0x00, 0x2a, 0x72, 0x7e, };

        private byte[] Request_CurrentPhaseB = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                                 0x48, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                                 0x00, 0x33, 0x07, 0x00, 0x00, 0x02, 0x00, 0xbe, 0xc6, 0x7e };

        private byte[] Request_CurrentPhaseC = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                                 0x48, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                                 0x00, 0x47, 0x07, 0x00, 0x00, 0x02, 0x00, 0x13, 0x13, 0x7e };

        private byte[] Request_PowerPhaseA = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                               0x48, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                               0x00, 0x15, 0x07, 0x00, 0x00, 0x02, 0x00, 0x84, 0xd6, 0x7e };

        private byte[] Request_PowerPhaseB = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                               0x48, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                               0x00, 0x29, 0x07, 0x00, 0x00, 0x02, 0x00, 0x6d, 0x70, 0x7e };

        private byte[] Request_PowerPhaseC = { 0x7e, 0xa0, 0x1c, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                               0x48, 0xe6, 0xe6, 0x00, 0xc0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                               0x00, 0x3D, 0x07, 0x00, 0x00, 0x02, 0x00, 0x24, 0x5b, 0x7e };

        private byte[] Request_AveragePower = { 0x7E, 0xA0, 0x1C, 0x00, 0x22, 0x80, 0x9f, 0x21, 0x10, 0x91,
                                                0x48, 0xE6, 0xE6, 0x00, 0xC0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                                0x00, 0x01, 0x07, 0x00, 0x00, 0x02, 0x00, 0x38, 0x09, 0x7E };

        private byte[] Request_SummaryPower = { 0x7E, 0xA0, 0x1C, 0x06, 0xA8, 0xAC, 0x01, 0x21, 0x10, 0x91,
                                                0x48, 0xE6, 0xE6, 0x00, 0xC0, 0x01, 0x81, 0x00, 0x03, 0x01,
                                                0x00, 0x01, 0x08, 0x00, 0xFF, 0x02, 0x00, 0x37, 0xA5, 0x7E};

        private short CrcHDLC(byte[] data, int size, int iOffset) // Supporting Function for finding CRC
        {
            byte bTmp;

            int wCrc = (int)0xFFFF;//InitHdlcCrc;
            for (int i = 0; i < size; i++)
            {
                bTmp = (byte)(data[iOffset + i] ^ (byte)(wCrc & 0x00ff));
                bTmp = (byte)((bTmp << 4) ^ bTmp);
                wCrc = (int)(wCrc >> 8);

                int iTmp0 = bTmp << 8;
                iTmp0 ^= (bTmp << 3) & 0x07ff;
                iTmp0 ^= (bTmp >> 4) & 0x0000000f;

                wCrc ^= iTmp0 & 0x0000ffff;
            }
            wCrc ^= 0xffff;
            return (short)wCrc;
        }

        private void FirstCheckSum(byte[] array) // Supporting Function for finding CRC
        {
            byte[] HDLC_HEADER = new byte[8];
            for (int i = 1, j = 0; i < HDLC_HEADER.Length + 1; i++, j++) HDLC_HEADER[j] = array[i];
            short crc = CrcHDLC(HDLC_HEADER, HDLC_HEADER.Length, 0);
            byte[] sum = BitConverter.GetBytes(crc);
            array[9] = sum[0];
            array[10] = sum[1];
        }

        private void SecondCheckSum(byte[] array) // Supporting Function for finding CRC
        {
            byte[] cal = new byte[array.Length - 4];
            for (int i = 1, k = 0; i < array.Length - 3; i++, k++) cal[k] = array[i];
            short crc = CrcHDLC(cal, cal.Length, 0);
            byte[] ret = BitConverter.GetBytes(crc);
            array[array.Length - 3] = ret[0];
            array[array.Length - 2] = ret[1];
        }

        private void BarcodeToAddr(long bar, ref ushort addrHI, ref ushort addrLO) // Supporting Function for finding CRC
        {
            addrHI = 0;
            addrLO = (ushort)(bar & 0x3FFF);
            if ((bar & 0x3FFF) < 16)
            {
                addrLO += 16;
                addrHI |= (1 << 12);
            }
            else
            {
                addrLO = (ushort)(bar & 0x3FFF);
            }
            if (((bar >> 14) & 0x3FFF) < 16)
            {
                addrHI += (ushort)(((bar >> 14) & 0x3FFF) + 16);
                addrHI |= (1 << 13);
            }
            else
            {
                addrHI |= (ushort)((bar >> 14) & 0x3FFF);
            }
        }

        private ushort HDLC_SerialNumber_CONVERT(int a, int l) // Supporting Function for finding CRC
        {
            return (ushort)(((((a) << 1) & 0x00FE) | (l)) | (((a) << 2) & 0xFE00));
        }

        private ushort HDLC_SerialNumber_DECODE(int a) // Supporting Function for finding CRC
        {
            return (ushort)((((a) >> 1) & 0x007F) | (((a) >> 2) & 0x3F80));
        }

        private void MakeCRC(int SerialNumber) // Function for finding CRC 
        {
            ushort high = 0, low = 0;
            BarcodeToAddr(SerialNumber, ref high, ref low);
            high = HDLC_SerialNumber_CONVERT(high, 0);
            low = HDLC_SerialNumber_CONVERT(low, 1);

            SerialNumber = (high << 16) | low;

            byte[] array = BitConverter.GetBytes(SerialNumber).Reverse().ToArray();

            for (int i = 3, j = 0; i < 7; i++, j++)
            {
                SNRM[i] = array[j];
                AARQ[i] = array[j];
                DISC[i] = array[j];
                Request_VoltagePhaseA[i] = array[j];
                Request_VoltagePhaseB[i] = array[j];
                Request_VoltagePhaseC[i] = array[j];
                Request_CurrentPhaseA[i] = array[j];
                Request_CurrentPhaseB[i] = array[j];
                Request_CurrentPhaseC[i] = array[j];
                Request_PowerPhaseA[i] = array[j];
                Request_PowerPhaseB[i] = array[j];
                Request_PowerPhaseC[i] = array[j];
                Request_SummaryPower[i] = array[j];
                Request_AveragePower[i] = array[j];
            }

            FirstCheckSum(AARQ);
            FirstCheckSum(Request_VoltagePhaseA);
            FirstCheckSum(Request_VoltagePhaseB);
            FirstCheckSum(Request_VoltagePhaseC);
            FirstCheckSum(Request_CurrentPhaseA);
            FirstCheckSum(Request_CurrentPhaseB);
            FirstCheckSum(Request_CurrentPhaseC);
            FirstCheckSum(Request_PowerPhaseA);
            FirstCheckSum(Request_PowerPhaseB);
            FirstCheckSum(Request_PowerPhaseC);
            FirstCheckSum(Request_AveragePower);
            FirstCheckSum(Request_SummaryPower);

            SecondCheckSum(SNRM);
            SecondCheckSum(AARQ);
            SecondCheckSum(DISC);
            SecondCheckSum(Request_VoltagePhaseA);
            SecondCheckSum(Request_VoltagePhaseB);
            SecondCheckSum(Request_VoltagePhaseC);
            SecondCheckSum(Request_CurrentPhaseA);
            SecondCheckSum(Request_CurrentPhaseB);
            SecondCheckSum(Request_CurrentPhaseC);
            SecondCheckSum(Request_PowerPhaseA);
            SecondCheckSum(Request_PowerPhaseB);
            SecondCheckSum(Request_PowerPhaseC);
            SecondCheckSum(Request_AveragePower);
            SecondCheckSum(Request_SummaryPower);
        }

        public static void WritePacket(SerialPort port, byte[] data)
        {
            port.Write(data, 0, data.Length); //Sending frame AARQ
            Thread.Sleep(2000);
        }

        private byte[] ReadPacket(SerialPort port)
        {
            int byteCount = port.BytesToRead; // get count of bytes in buffer
            byte[] retBytes = new byte[byteCount];
            port.Read(retBytes, 0, byteCount); // read bytes from buffer
            if (retBytes.Length == 0)
                throw new IndexOutOfRangeException(); // if array is Empty throw Exception
            return retBytes;
        }

        private string GetParam(SerialPort port, byte[] data)
        {
            string value = String.Empty;

            int StartDataIndex = 20;
            int EndDataIndex = 29;

            try
            {
                WritePacket(port, data); // Send request for Parameter
                receive = ReadPacket(port); // receive responce

                for (int i = StartDataIndex; i < EndDataIndex; i++)
                    value += Convert.ToChar(receive[i]); // Get only data of parameter

                return value.Replace(",", "."); // return read value;
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_31: " + ex.Message);
                return " - 1"; // return -1 if some troubles
            }
        }

        public List<DataParameter> ReadCurrentValues()
        {
            if (Array.IndexOf(SerialPort.GetPortNames(), "/dev/ttyUSB0") > -1) // check if USB-COM is available
            {
                SerialPort port = new SerialPort("/dev/rs485", 9600, Parity.None, 8, StopBits.One); // Create SerialPort object

                port.Handshake = Handshake.None; // HandShake to none
                port.ReadTimeout = 2000; // TimeOut for read and write
                port.WriteTimeout = 2000;

                if (!(port.IsOpen)) port.Open(); // open port

                WritePacket(port, SNRM); // Write SNRM request
                receive = ReadPacket(port); //Answer from NIK

                WritePacket(port, AARQ); // Write AARQ request
                receive = ReadPacket(port); //Answer from NIK

                Nik_Data nikData = new Nik_Data();

                nikData.VoltagePhaseA = Convert.ToDouble(GetParam(port, Request_VoltagePhaseA));
                nikData.VoltagePhaseB = Convert.ToDouble(GetParam(port, Request_VoltagePhaseB));
                nikData.VoltagePhaseC = Convert.ToDouble(GetParam(port, Request_VoltagePhaseC));
                nikData.CurrentPhaseA = Convert.ToDouble(GetParam(port, Request_CurrentPhaseA));
                nikData.CurrentPhaseB = Convert.ToDouble(GetParam(port, Request_CurrentPhaseB));
                nikData.CurrentPhaseC = Convert.ToDouble(GetParam(port, Request_CurrentPhaseC));
                nikData.PowerPhaseA = Convert.ToDouble(GetParam(port, Request_PowerPhaseA));
                nikData.PowerPhaseB = Convert.ToDouble(GetParam(port, Request_PowerPhaseB));
                nikData.PowerPhaseC = Convert.ToDouble(GetParam(port, Request_PowerPhaseC));
                nikData.AveragePower = Convert.ToDouble(GetParam(port, Request_AveragePower));
                nikData.SummaryPower = Convert.ToDouble(GetParam(port, Request_SummaryPower));

                WritePacket(port, DISC); // Write request for ending send
                port.Close(); // ClosePort

                return ConvertToListOfCurrentValues(nikData);
            }
            else
            {
                Console.WriteLine(">>> RS-485 not found");
                return new List<DataParameter>();
            }
        }

        public List<DataParameter> ConvertToListOfCurrentValues(Nik_Data data)
        {
            List<DataParameter> NikCurrentValues = new List<DataParameter>();

            NikCurrentValues.Add(new DataParameter(SerialNumber.ToString(), NIK_EQUIPMENT_TYPE_ID, VOLTAGE_OBIS_CODE, 0, data.VoltagePhaseA)); // Adding Voltage Phase A
            NikCurrentValues.Add(new DataParameter(SerialNumber.ToString(), NIK_EQUIPMENT_TYPE_ID, VOLTAGE_OBIS_CODE, 1, data.VoltagePhaseB)); // Adding Voltage Phase B
            NikCurrentValues.Add(new DataParameter(SerialNumber, NIK_EQUIPMENT_TYPE_ID, VOLTAGE_OBIS_CODE, 2, data.VoltagePhaseC)); // Adding Voltage Phase C
            NikCurrentValues.Add(new DataParameter(SerialNumber, NIK_EQUIPMENT_TYPE_ID, CURRENT_OBIS_CODE, 0, data.CurrentPhaseA)); // Adding Current Phase A
            NikCurrentValues.Add(new DataParameter(SerialNumber, NIK_EQUIPMENT_TYPE_ID, CURRENT_OBIS_CODE, 1, data.CurrentPhaseB)); // Adding Current Phase B
            NikCurrentValues.Add(new DataParameter(SerialNumber, NIK_EQUIPMENT_TYPE_ID, CURRENT_OBIS_CODE, 2, data.CurrentPhaseC)); // Adding Current Phase C
            NikCurrentValues.Add(new DataParameter(SerialNumber, NIK_EQUIPMENT_TYPE_ID, POWER_OBIS_CODE, 0, data.PowerPhaseA)); // Adding Power Phase A
            NikCurrentValues.Add(new DataParameter(SerialNumber, NIK_EQUIPMENT_TYPE_ID, POWER_OBIS_CODE, 1, data.PowerPhaseB)); // Adding Power Phase B 
            NikCurrentValues.Add(new DataParameter(SerialNumber, NIK_EQUIPMENT_TYPE_ID, POWER_OBIS_CODE, 2, data.PowerPhaseC)); // Adding Power Phase C
            NikCurrentValues.Add(new DataParameter(SerialNumber, NIK_EQUIPMENT_TYPE_ID, ACTIVE_POWER_OBIS_CODE, 4, data.AveragePower)); // Adding Average Power by 3 Phases
            NikCurrentValues.Add(new DataParameter(SerialNumber, NIK_EQUIPMENT_TYPE_ID, SUMMARY_POWER_OBIS_CODE, 5, data.SummaryPower)); // Adding Summary Power by all time working

            return NikCurrentValues;
        }

        public Nik(int SerialNumber)
        {
            this.SerialNumber = SerialNumber.ToString();
            MakeCRC(SerialNumber); // Calculate CRC for all request's
        }
    }

    public class DataParameter
    {
        public String DriverFeature = String.Empty;
        public int EquipmentTypeID = 0;
        public int obisCode = 0;
        public int parameterNumber = 0;
        public int qualityID = 0;
        public DateTime Date = new DateTime();
        public double value = 0;

        public DataParameter()
        {
            DriverFeature = String.Empty;
            EquipmentTypeID = 0;
            obisCode = 0;
            parameterNumber = 0;
            qualityID = 0;
            Date = new DateTime();
            value = 0;
        }

        public DataParameter(String DriverFeature, int EquipmentTypeID, int obisCode, int parameterNumber, double value)
        {
            this.DriverFeature = DriverFeature;
            this.EquipmentTypeID = EquipmentTypeID;
            this.obisCode = obisCode;
            this.parameterNumber = parameterNumber;
            this.qualityID = 2;
            this.value = value;
            Date = DateTime.Now;
        }
    }

    public class Program
    {
        // Локально или на малинке
        const int LOCALDEBAG = 0;
        const int USERASPBERRY = 1;
        static int USE_NOW = USERASPBERRY;

        private static SerialPort RadioSerialPort; // Com port for work with radio module
        private static int NIK_Address = 0, StationID = 0, Port = 0, EquipmentTypeID = 0; // Read from Configuration.xml
        private static LinkedList<ServerCommands> SetCommands = new LinkedList<ServerCommands>(); // List of commands that are sended to radio modules
        private static String Host = String.Empty, RadioSerialNumber = String.Empty; // Read from Configuration.xml
        private static List<DataParameter> MainData = new List<DataParameter>(); // List of data that are sended to Server
        private static object commandsLocker = new object(); // lock commands
        private static String RaspberryID = String.Empty; // Read from Configuration.xml - For Repository
        private static object locker = new object(); // locaker for MainData
        private static SerialPort serialModulator; // COM Port for work with modulator

        private static String RadioResetPin = "18"; // String value of pin that reset radio modul
        private static GPIO RadioResetGPIO; // GPIO that resets radio module


        static public byte HI(ushort x) // return high byte
        {
            return Convert.ToByte(x >> 8);
        }

        static public byte LO(ushort x) //  return low byte
        {
            return Convert.ToByte(x & 0xFF);
        }

        static public ushort CalcCRC16(byte[] data) // Function for finding crc for packet protocol
        {
            ushort crc = 0x0000;
            for (int i = 0; i < data.Length; i++)
            {
                crc ^= (ushort)(data[i] << 8);
                for (int j = 0; j < 8; j++)
                {
                    if ((crc & 0x8000) > 0)
                        crc = (ushort)((crc << 1) ^ 0x8005);
                    else
                        crc <<= 1;
                }
            }
            return crc;
        }


        public static void WritePacket(SerialPort port, byte[] data)
        {
            port.Write(data, 0, data.Length); //Sending frame AARQ
            Thread.Sleep(2000);
        }


        public static void GetDoorSensorState(string GpioPin, string ParameterID, int Time_Span)
        {
            Console.WriteLine(">>> Door Sensor : StationID : " + StationID + " <<< RaspberryID : " + RaspberryID + " <<< GpioPin : " + GpioPin + " <<< ParameterID " + ParameterID);

            GPIO gpio = GPIO.Export(GpioPin, "in"); // Export GPIO for door sensor
            int state = 0, currentState = 0, count = 0;

            while (true)
            {
                try
                {
                    currentState = gpio.GPIOGetValue();
                    if (state == 1 && currentState == 0) // if door sensor activated
                    {
                        state = currentState; // change lastState of sensor on current
                        Console.WriteLine(">>> DoorSensor current state : " + currentState);

                        //Run program to make photos
                        //RunCamera();
                    }
                    if (state == 0 && currentState == 1) state = currentState; // change lastState if sendor diactivates
                    Thread.Sleep(200);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(">>> " + ex.Message + " GetDoorSensorState()");
                    if (++count >= 20) // counter for problems with sensor
                    {
                        //run diagnostic of door sensor
                    }
                }
                Thread.Sleep(200);
            }
        }

        public static bool IsConnected(Socket socket)
        {
            try
            {
                return !(socket.Poll(1, SelectMode.SelectRead) && socket.Available == 0); // detect for socket connection
            }
            catch (SocketException)
            {
                return false;
            }
        }


        private static void ConvertRadioResponse(String response)
        {
            //example of response ---> #ALL-i-01/01/00/20/01/29/00-064-Z-0&
            String[] parameters = response.Split('-');
            String ID = parameters[1];

            double illumination = Convert.ToDouble(parameters[3]);
            int brightness = (int)(parameters[4][0]);
            int state = Convert.ToInt32(parameters[5][0]) - 0x30;

            Console.WriteLine(">>> Illumination = " + illumination);
            Console.WriteLine(">>> Brightness = " + brightness);
            Console.WriteLine(">>> State = " + state);

            MainData.Add(new DataParameter(GetSerialNumber(Convert.ToInt32(ID)), 18, 46, 0, Convert.ToDouble(illumination)));
            MainData.Add(new DataParameter(GetSerialNumber(Convert.ToInt32(ID)), 18, 47, 0, Convert.ToDouble(brightness)));
            MainData.Add(new DataParameter(GetSerialNumber(Convert.ToInt32(ID)), 18, 45, 0, Convert.ToDouble(state)));
        }


        public static short ComputeHash(byte[] data)
        {
            const int p = 1423;
            int hash = 2166;

            for (int i = 0; i < data.Length; i++)
                hash = (short)((hash ^ data[i]) * p);

            hash ^= hash >> 7;
            hash += hash << 3;
            hash += hash << 5;
            return (short)hash;
        }

        public static void ExecuteCommand(ServerCommands cmd)
        {
            try
            {
                if (cmd.commandStatus == false)
                {
                    Console.WriteLine("   ===>>>   ExecuteCommands status : " + cmd.commandStatus);
                    byte[] bytes = BitConverter.GetBytes(cmd.value);
                    byte[] Value = { 0, 0, bytes[1], bytes[0] };

                    if (cmd.serialNumber != String.Empty && cmd.register == 0x5E)
                    {
                        UInt16 serialXor = 0;

                        Console.WriteLine(">>> Go to xor serialNumber value...");

                        foreach (var a in cmd.serialNumber)
                        {
                            serialXor += Convert.ToUInt16(Convert.ToByte(a) * Convert.ToByte(a));
                        }

                        short HashedSerialNumber = ComputeHash(Encoding.ASCII.GetBytes(cmd.serialNumber));

                        byte[] tmp = BitConverter.GetBytes(serialXor);
                        Value[0] = tmp[0];
                        Value[1] = tmp[1];
                    }

                    if (Request(Convert.ToUInt16(cmd.ID), cmd.functionCode, Convert.ToByte(cmd.register), Value))
                    {
                        cmd.commandStatus = true;
                        cmd.commandsCount = 0;
                        ExecDBString("UPDATE Commands SET status = true, count = 0 WHERE ID = " + cmd.ID + " AND functionCode = " + cmd.functionCode + " AND register = " + cmd.register);
                    }
                    else
                    {
                        cmd.commandsCount += 1;

                        if (cmd.functionCode == 0xF)
                        {
                            cmd.commandStatus = true;
                            cmd.commandsCount = 0;
                        }

                        if (cmd.commandsCount >= 3)
                        {
                            cmd.commandStatus = true;
                        }

                        Console.WriteLine("Increment command count");
                        ExecDBString("UPDATE Commands SET count = " + cmd.commandsCount + " , status = " + cmd.commandStatus + " WHERE ID = " + cmd.ID + " AND functionCode = " + cmd.functionCode + " AND register = " + cmd.register);
                    }
                }

                SetCommands.Remove(cmd);
                SetCommands.AddLast(cmd);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " ExecuteCommand()");
            }
        }

        private static void RunCamera()
        {
            try
            {
                string strCmdText = "bash /home/pi/camera.sh";

                ProcessStartInfo start = new ProcessStartInfo();
                start.Arguments = strCmdText;
                start.FileName = "sudo";
                start.CreateNoWindow = true;

                using (Process proc = Process.Start(start))
                {
                    proc.WaitForExit();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> " + ex.Message + " RunCamera");
            }
        }

        public static void ReadConfiguration(string xml)
        {
            ReadCommandsFromDB();
            RadioSerialNumber = GetRadioSerial();

            if (RadioSerialNumber == String.Empty)
            {
                Console.WriteLine(">>> RadioModule Error. Need to reboot");
                System.Environment.Exit(-1);
            }

            using (XmlReader reader = XmlReader.Create(new StringReader(xml)))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement()) // Only detect start elements.
                    {
                        switch (reader.Name) // Get element name and switch on it.
                        {
                            case "Raspberry":
                                {
                                    reader.MoveToAttribute("RaspberryID");
                                    RaspberryID = reader.Value;

                                    reader.MoveToAttribute("EquipmentTypeID");
                                    EquipmentTypeID = Convert.ToInt32(reader.Value);

                                    reader.MoveToAttribute("Host");
                                    Host = reader.Value;

                                    reader.MoveToAttribute("Port");
                                    Port = Convert.ToInt32(reader.Value);
                                }
                                break;
                            case "NIK":
                                {
                                    reader.MoveToAttribute("Address");
                                    NIK_Address = Convert.ToInt32(reader.Value);

                                    reader.MoveToAttribute("TimeSpan");
                                    int TimeSpan = Convert.ToInt32(reader.Value);

                                    Thread nik_Thread = new Thread(unused => ReadNikData(NIK_Address, TimeSpan));
                                    nik_Thread.IsBackground = true;
                                    nik_Thread.Start();
                                }
                                break;
                            case "DoorSensor":
                                {
                                    reader.MoveToAttribute("GPIO");
                                    String GpioPin = reader.Value;

                                    reader.MoveToAttribute("TimeSpan");
                                    int Time_Span = Convert.ToInt32(reader.Value);

                                    reader.MoveToAttribute("obisCode");
                                    String obisCode = reader.Value;

                                    Thread doorSensorThread = new Thread(unused => GetDoorSensorState(GpioPin, obisCode, Time_Span));
                                    doorSensorThread.IsBackground = true;
                                    //doorSensorThread.Start();
                                }
                                break;
                        }
                    }
                }
            }

            Console.WriteLine("==================================================");
            Console.WriteLine(">>>   Host : " + Host);
            Console.WriteLine(">>>   Port : " + Port);
            Console.WriteLine(">>>   RaspberryID : " + RaspberryID);
            Console.WriteLine(">>>   EquipmentTypeID : " + EquipmentTypeID);
            Console.WriteLine(">>>   NIK Address : " + NIK_Address);
            Console.WriteLine(">>>   Serial Number of Radio : " + RadioSerialNumber);
            Console.WriteLine("==================================================");
        }

        private static void ReadNikData(int NikSerialNumber, int TimeDelay)
        {
            Nik nik = new Nik(NIK_Address);
            int errorCount = 0;

            while (true)
            {
                lock (locker)
                {
                    try
                    {
                        MainData.Concat(nik.ReadCurrentValues()); // Read parameters from NIK
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(">>> TCER_33: " + ex.Message + " - NIK");
                        TimeDelay += 2;
                        if (++errorCount >= 20) // counter for problems with COM-port
                        {
                            // run reboot of serial port 
                        }
                    }
                }

                Thread.Sleep(TimeSpan.FromSeconds(TimeDelay));
            }
        }

        public static String InsertLampAndGetID(String serialNumber)
        {
            while (true)
            {
                try
                {
                    String StationEquipmentID = GetStationEquipmentIDBySerialNumber(serialNumber);

                    if (StationEquipmentID.Equals("0"))
                    {
                        Console.WriteLine(">>> Connect to server for geting StationEquipmentID - Host : " + Host + ", Port : " + Port);
                        byte[] buffer = new byte[256];

                        TcpClient client = new TcpClient(Host, Port);

                        client.Client.SendTimeout = 3000;
                        client.Client.ReceiveTimeout = 3000;

                        client.Client.Send(Encoding.ASCII.GetBytes("StationEquipmentID$" + serialNumber + "$" + RaspberryID)); // request to server with sending SerialNumber
                        Thread.Sleep(TimeSpan.FromSeconds(10));
                        int bytesRead = client.Client.Receive(buffer);

                        Console.WriteLine(">>> Bytes read : " + bytesRead);

                        buffer = buffer.Take(bytesRead).ToArray();
                        string data = Encoding.ASCII.GetString(buffer);
                        string[] dataParams = data.Split(';');

                        StationEquipmentID = dataParams[0]; // Converting received value 

                        Console.WriteLine(">>> Geted StationEquipmentID : " + StationEquipmentID);

                        lock (JobQueueLocker)
                        {
                            for (int i = 1; i < dataParams.Length; i++)
                            {
                                List<string> info = new List<string>();
                                string[] commandInfo = dataParams[i].Split('$');

                                foreach (var cmdInfo in commandInfo)
                                {
                                    info.Add(cmdInfo);
                                }
                                JobQueue.Add(info);
                            }
                        }

                        InsertSerialNumber(Convert.ToInt32(StationEquipmentID), serialNumber); // Add StationEquipmentId of new lamp to out table on Raspberry
                    }
                    else
                    {
                        Console.WriteLine("StationEquipmentID From Raspberry DB : " + StationEquipmentID);
                    }

                    return StationEquipmentID; // return StationEquipmentID of new lamp to controller
                }
                catch (Exception ex)
                {
                    Console.WriteLine(">>> TCER_35: " + ex.Message + " GetStationEquipmentID()");
                    Thread.Sleep(TimeSpan.FromSeconds(5)); // while we don't receive StationEquipmentID of new lamp we will try to do this again and again
                    continue;
                }
            }
        }

        private static String GetSerialNumber(int StationEquipmentID)
        {
            String SerialNumber = String.Empty;

            string cs = @"server=localhost;userid=root;password=raspberry;database=RaspDB";

            try
            {
                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                string sql = "Select SerialNumber From StationEquipmentIdToSerialNumber Where StationEquipmentID = " + StationEquipmentID;

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Console.WriteLine(rdr.GetString(0));
                    SerialNumber = rdr.GetString(0);
                }

                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_36: " + ex.Message + " GetSerialNumber()");
            }
            return SerialNumber;
        }

        private static String GetStationEquipmentIDBySerialNumber(String SerialNumber)
        {
            String StationEquipmentID = String.Empty;

            string cs = @"server=localhost;userid=root;password=raspberry;database=RaspDB";

            try
            {
                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                string sql = "Select StationEquipmentID From StationEquipmentIdToSerialNumber Where SerialNumber =\'" + SerialNumber + "\'";

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Console.WriteLine(rdr.GetString(0));
                    StationEquipmentID = rdr.GetString(0);
                }

                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_37: " + ex.Message + " GetStationEquipmentIDBySerialNumber");
            }
            if (StationEquipmentID == String.Empty)
                return "0";
            else
                return StationEquipmentID;
        }

        private static void InsertSerialNumber(int StationEquipmentID, String SerialNumber)
        {
            string cs = @"server=localhost;userid=root;password=raspberry;database=RaspDB";

            try
            {
                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                string sql = "INSERT INTO StationEquipmentIdToSerialNumber VALUES(NULL, @StationEquipmentID, @SerialNumber, null);";

                MySqlCommand cmd = new MySqlCommand(sql);
                cmd.Connection = conn;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@StationEquipmentID", StationEquipmentID);
                cmd.Parameters.AddWithValue("@SerialNumber", SerialNumber);

                cmd.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_38: " + ex.Message + " InsertSerialNumber()");
            }
        }

        private static void WriteLampParamsToArchive(int LampAddress, int ObisCode, int ParameterNumber, int QualityID, double Value)
        {
            string cs = @"server=localhost;userid=root;
            	      password=raspberry;database=RaspDB";

            try
            {
                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                string sql = "INSERT INTO LampArchives() VALUES(NULL, @RaspberryID, @LampAddress, @ObisCode, @ParameterNumber, @QualityID, @Value);";

                MySqlCommand cmd = new MySqlCommand(sql);
                cmd.Connection = conn;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@RaspberryID", RaspberryID);
                cmd.Parameters.AddWithValue("@LampAddress", LampAddress);
                cmd.Parameters.AddWithValue("@ObisCode", ObisCode);
                cmd.Parameters.AddWithValue("@ParameterNumber", ParameterNumber);
                cmd.Parameters.AddWithValue("@QualityID", QualityID);
                cmd.Parameters.AddWithValue("@Value", Value);

                cmd.ExecuteNonQuery();

                conn.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(">>> TCER_39: " + ex.Message + " WriteLampParamsToArchive");
                return;
            }
        }

        private static void WriteRaspberryParamsToArchive(int ObisCode, int ParameterNumber, int QualityID, double Value)
        {
            string cs = @"server=localhost;userid=root;
            	      password=raspberry;database=RaspDB";

            try
            {
                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                string sql = "INSERT INTO RaspberryArchives() VALUES(NULL, @RaspberryID, @ObisCode, @ParameterNumber, @QualityID, @Value);";

                MySqlCommand cmd = new MySqlCommand(sql);
                cmd.Connection = conn;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@RaspberryID", RaspberryID);
                cmd.Parameters.AddWithValue("@ObisCode", ObisCode);
                cmd.Parameters.AddWithValue("@ParameterNumber", ParameterNumber);
                cmd.Parameters.AddWithValue("@QualityID", QualityID);
                cmd.Parameters.AddWithValue("@Value", Value);

                cmd.ExecuteNonQuery();

                conn.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("TCER_40: " + ex.Message + " WriteRaspberryParamsToArchive");
                return;
            }
        }

        public static void SendData()
        {
            List<DataParameter> temp;

            while (true)
            {
                try
                {
                    lock (locker)
                    {
                        temp = new List<DataParameter>(MainData);
                        MainData.Clear();
                    }

                    string toSend = String.Empty;
                    foreach (var a in temp)
                    {
                        toSend += Convert.ToString(a.DriverFeature) + '$';
                        toSend += Convert.ToString(a.EquipmentTypeID) + '$';
                        toSend += Convert.ToString(a.obisCode) + '$';
                        toSend += Convert.ToString(a.parameterNumber) + '$';
                        toSend += Convert.ToString(a.qualityID) + '$';
                        toSend += Convert.ToString(a.Date) + '$';
                        toSend += Convert.ToString(a.value) + '#';
                    }

                    if (toSend != String.Empty)
                    {
                        TcpClient client = new TcpClient(Host, Convert.ToInt32(Port));
                        client.Client.Send(Encoding.ASCII.GetBytes(toSend));
                        client.Client.Shutdown(SocketShutdown.Both);
                        client.Close();
                    }

                    Thread.Sleep(TimeSpan.FromMinutes(1));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(">>> TCER_41: " + ex.Message + " SendData()");
                    continue;
                }
            }
        }

        public static void Swap(List<ServerCommands> list, int indexA, int indexB)
        {
            ServerCommands tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
        }

        public static void RadioWork()
        {
            SendInitRequest(0, 0x0A, 0x71, new byte[] { 0, 0, 0, 0 });

            while (true)
            {
                try
                {
                    lock (commandsLocker)
                    {
                        if (SetCommands.Any())
                        {
                            ServerCommands command = SetCommands.First();
                            ExecuteCommand(command);
                        }
                    }

                    #region need to be refactored
                    /*int byteRead = RadioSerialPort.ReadByte();
                    if (Convert.ToChar(byteRead) == '#')
                    {
                        Console.WriteLine(">>> Start frame comes");
                        while (Convert.ToChar(byteRead) != '&')
                        {
                            byteRead = RadioSerialPort.ReadByte();
                            if (Convert.ToChar(byteRead) != '&') radioCommand += Convert.ToChar(byteRead);
                        }
                        Console.WriteLine(">>> End frame comes");
                        Console.WriteLine(">>> Command : " + radioCommand);

                        if (radioCommand.Contains("ID"))
                        {
                            string[] responce = radioCommand.Split('-');
                            int ID = (responce[1][0] << 8) | (responce[1][1]);
                            string serialNumber = responce[2];
                            String NewID = GetStationEquipmentID(ID, serialNumber);

                            Console.WriteLine(">>> Getted ID from Radio : " + ID);
                            Console.WriteLine(">>> Getted RadioSerial from Radio : " + serialNumber);

                            lock (commandsLocker)
                            {
                                if (!(SetCommands.Where(x => x.command.Equals("SetID-" + NewID + "-" + serialNumber)).ToList().Any()))
                                    SetCommands.AddFirst(new ServerCommands("SetID-" + NewID + "-" + serialNumber, 0, 0, 0, 0x5E, Convert.ToInt16(NewID), 0x0A, false, serialNumber));

                            }
                        }
                        if (radioCommand.Contains("ADC"))
                        {
                            string[] responce = radioCommand.Split('-');
                            int ID = (responce[1][0] << 8) | (responce[1][1]);
                            string value = responce[2];

                            Console.WriteLine(">>> Getted ID from Radio : " + ID);
                            Console.WriteLine(">>> Getted ADC from Radio : " + value);
                        }
                        if (radioCommand.Contains("PWM"))
                        {
                            string[] responce = radioCommand.Split('-');
                            int ID = (responce[1][0] << 8) | (responce[1][1]);
                            int value = (int)Char.GetNumericValue(Convert.ToChar(responce[2]));

                            Console.WriteLine(">>> Getted ID from Radio : " + ID);
                            Console.WriteLine(">>> Getted PWM from Radio : " + value);
                        }
                        if (radioCommand.Contains("STATE"))
                        {
                            string[] responce = radioCommand.Split('-');
                            int ID = (responce[1][0] << 8) | (responce[1][1]);
                            int value = Convert.ToInt32(Convert.ToChar(responce[2]));

                            Console.WriteLine(">>> Getted ID from Radio : " + ID);
                            Console.WriteLine(">>> Getted State from Radio : " + value);
                        }
                        if (radioCommand.Contains("RTC"))
                        {
                            string[] responce = radioCommand.Split('-');
                            int ID = (responce[1][0] << 8) | (responce[1][1]);
                            string[] time = responce[2].Split('/');

                            int date = (int)Char.GetNumericValue(Convert.ToChar(time[0]));
                            int day = (int)Char.GetNumericValue(Convert.ToChar(time[1]));
                            int hour = (int)Char.GetNumericValue(Convert.ToChar(time[2]));
                            int min = (int)Char.GetNumericValue(Convert.ToChar(time[3]));
                            int month = (int)Char.GetNumericValue(Convert.ToChar(time[4]));
                            int sec = (int)Char.GetNumericValue(Convert.ToChar(time[5]));
                            int year = (int)Char.GetNumericValue(Convert.ToChar(time[6]));

                            DateTime lampTime = new DateTime(year, month, day, hour, min, sec);

                            Console.WriteLine(">>> Getted ID from Radio : " + ID);
                            Console.WriteLine(">>> Getted Time from Radio : " + lampTime);
                        }
                    }*/
                    #endregion
                    Thread.Sleep(10);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(">>> TCER_42: " + ex.Message + "RadioWork()");
                }
            }
        }

        private static void RadioSerialPortBufferClear()
        {
            RadioSerialPort.DiscardOutBuffer();
            RadioSerialPort.DiscardInBuffer();
        }

        // function for GET Request to radio module
        private static bool Request(ushort DeviceAddress, byte function, byte RegisterAddress)
        {
            Console.WriteLine(">>> Sending GET request...");
            Console.WriteLine("==========================================");

            byte[] ID = BitConverter.GetBytes(DeviceAddress).Reverse().ToArray();
            byte[] ReadRequestArr = { ID[0], ID[1], function, HI(RegisterAddress), LO(RegisterAddress), 0x00, 0x00 };

            ushort crc = CalcCRC16(ReadRequestArr.Take(ReadRequestArr.Length - 2).ToArray());
            ReadRequestArr[5] = HI(crc);
            ReadRequestArr[6] = LO(crc);

            for (int i = 0; i < ReadRequestArr.Length; i++)
            {
                Console.Write("{0:X} ", ReadRequestArr[i]);
            }

            Console.WriteLine();
            Console.WriteLine("==========================================");

            int tries = 3;

            while (tries != 0)
            {
                try
                {
                    RadioSerialPortBufferClear();
                    byte[] responce = new byte[128];
                    int bytesRead = 0;

                    Console.WriteLine(">>> Sending data1"); // Will not Newer run 
                    byte[] eCMD = { 0x2b, 0x2b, 0x2b };
                    RadioSerialPort.Write(eCMD, 0, eCMD.Length);
                    bytesRead = RadioSerialPort.Read(responce, 0, RadioSerialPort.BytesToRead);
                    Console.Write(">>> Byte" + Encoding.ASCII.GetString(responce));

                    Console.WriteLine(">>> Sending eATAP");
                    byte[] eATAP = { 0x41, 0x54, 0x41, 0x50, 0x20, 0x31, 0xd, 0xa };
                    RadioSerialPort.Write(eATAP, 0, eATAP.Length);
                    bytesRead = RadioSerialPort.Read(responce, 0, RadioSerialPort.BytesToRead);
                    Console.Write(">>> Byte" + Encoding.ASCII.GetString(responce));

                    Console.WriteLine(">>> Sending eATRO");
                    byte[] eATRO = { 0x41, 0x54, 0x52, 0x4f, 0x20, 0x31, 0x30, 0x30, 0x30, 0xd, 0xa };
                    RadioSerialPort.Write(eATRO, 0, eATRO.Length);
                    bytesRead = RadioSerialPort.Read(responce, 0, RadioSerialPort.BytesToRead);
                    Console.Write(">>> Byte" + Encoding.ASCII.GetString(responce));

                    Console.WriteLine(">>> Sending eATCN");
                    byte[] eATCN = { 0x41, 0x54, 0x43, 0x4e, 0xd, 0xa };
                    RadioSerialPort.Write(eATCN, 0, eATCN.Length);
                    bytesRead = RadioSerialPort.Read(responce, 0, RadioSerialPort.BytesToRead);
                    Console.Write(">>> Byte" + Encoding.ASCII.GetString(responce));

                    Console.WriteLine(">>> Sending eEnter");
                    byte[] eEnter = { 0xd, 0xa };
                    RadioSerialPort.Write(eEnter, 0, eEnter.Length);
                    RadioSerialPort.Write(ReadRequestArr, 0, ReadRequestArr.Length);
                    Thread.Sleep(15000);
                    bytesRead = RadioSerialPort.Read(responce, 0, RadioSerialPort.BytesToRead);
                    Console.WriteLine(">>> BytesRead : " + bytesRead);

                    for (int i = 0; i < bytesRead; i++)
                        Console.Write("Byte {0:X}", responce[i]);

                    Console.WriteLine("\nReceived String -->>  " + Encoding.ASCII.GetString(responce));
                    Console.WriteLine(">>> SerialRead succesfull");

                    if (bytesRead > 2)
                    {
                        Console.Write(">>> Byteread=" + bytesRead);
                        Console.Write(">>> SetRequestArr.Length=" + ReadRequestArr.Length);
                        responce = responce.Take(bytesRead).ToArray();

                        Console.WriteLine(">>> CrcHi : " + responce[0]);
                        Console.WriteLine(">>> CrcLow : " + responce[1]);
                        Console.WriteLine(">>> Third byte : " + responce[2]);

                        if (ParseResponce(responce.ToList(), ReadRequestArr[5], ReadRequestArr[6]))
                        {
                            Console.WriteLine(">>> Write Succeeded!");
                            return true;
                        }
                    }
                    tries--;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(">>> TCER_43: " + ex.Message + " tries " + tries + " Request");
                    tries--;
                }
            }
            return false;
        }

        // function for SET Requset to radio Module
        static bool Request(ushort DeviceAddress, byte function, byte RegisterAddress, byte[] Value)
        {
            Console.WriteLine(">>> Sending SET Request...");
            Console.Write("==========================================\n");

            byte[] SetRequestArr = new byte[Value.Length + 7];
            byte[] ID = BitConverter.GetBytes(DeviceAddress).Reverse().ToArray();

            SetRequestArr[0] = ID[0];
            SetRequestArr[1] = ID[1];
            SetRequestArr[2] = function;
            SetRequestArr[3] = HI(RegisterAddress);
            SetRequestArr[4] = LO(RegisterAddress);

            for (int i = 5; i < Value.Length + 5; i++)
            {
                SetRequestArr[i] = Value[i - 5];
            }

            ushort crc = CalcCRC16(SetRequestArr.Take(SetRequestArr.Length - 2).ToArray());
            SetRequestArr[SetRequestArr.Length - 2] = HI(crc);
            SetRequestArr[SetRequestArr.Length - 1] = LO(crc);

            for (int i = 0; i < SetRequestArr.Length; i++)
            {
                Console.Write("{0:X} ", SetRequestArr[i]);
            }
            Console.WriteLine();
            Console.Write("==========================================\n");
            int tries = 3;

            if (function == 0x0F)
                tries = 2;

            while (tries != 0)
            {
                try
                {
                    RadioSerialPortBufferClear();
                    byte[] request = new byte[128];
                    int bytesRead = 0;

                    Console.WriteLine(">>> Sending data"); // Will not Newer run 
                    byte[] eCMD = { 0x2b, 0x2b, 0x2b };
                    RadioSerialPort.Write(eCMD, 0, eCMD.Length);
                    bytesRead = RadioSerialPort.Read(request, 0, RadioSerialPort.BytesToRead);
                    Console.Write(">>> Byte" + Encoding.ASCII.GetString(request));

                    Console.WriteLine(">>> Sending eATAP");
                    byte[] eATAP = { 0x41, 0x54, 0x41, 0x50, 0x20, 0x31, 0xd, 0xa };
                    RadioSerialPort.Write(eATAP, 0, eATAP.Length);
                    Thread.Sleep(1000);
                    bytesRead = RadioSerialPort.Read(request, 0, RadioSerialPort.BytesToRead);
                    Console.Write(">>> Byte" + Encoding.ASCII.GetString(request));

                    Console.WriteLine(">>> Sending eATRO");
                    byte[] eATRO = { 0x41, 0x54, 0x52, 0x4f, 0x20, 0x31, 0x30, 0x30, 0x30, 0xd, 0xa };
                    RadioSerialPort.Write(eATRO, 0, eATRO.Length);
                    Thread.Sleep(1000);
                    bytesRead = RadioSerialPort.Read(request, 0, RadioSerialPort.BytesToRead);
                    Console.Write(">>> Byte" + Encoding.ASCII.GetString(request));

                    Console.WriteLine(">>> Sending eATCN");
                    byte[] eATCN = { 0x41, 0x54, 0x43, 0x4e, 0xd, 0xa };
                    RadioSerialPort.Write(eATCN, 0, eATCN.Length);
                    Thread.Sleep(1000);
                    bytesRead = RadioSerialPort.Read(request, 0, RadioSerialPort.BytesToRead);
                    Console.Write(">>> Byte" + Encoding.ASCII.GetString(request));

                    Console.WriteLine(">>> Sending eEnter");
                    byte[] eEnter = { 0xd, 0xa };
                    RadioSerialPort.Write(eEnter, 0, eEnter.Length);
                    Thread.Sleep(1000);

                    RadioSerialPort.Write(SetRequestArr, 0, SetRequestArr.Length);

                    Thread.Sleep(15000);
                    bytesRead = RadioSerialPort.Read(request, 0, RadioSerialPort.BytesToRead);
                    Console.WriteLine(">>> BytesRead : " + bytesRead);

                    for (int i = 0; i < bytesRead; i++)
                        Console.Write("Byte {0:X}", request[i]);

                    Console.WriteLine("\nReceived String -->>  " + Encoding.ASCII.GetString(request));
                    Console.WriteLine(">>> SerialRead succesfull");

                    if (bytesRead > 2)
                    {
                        Console.Write(">>> Byteread = " + bytesRead);
                        Console.Write(">>> SetRequestArr.Length = " + SetRequestArr.Length);
                        request = request.Take(bytesRead).ToArray();

                        Console.WriteLine(">>> CrcHi : " + request[0]);
                        Console.WriteLine(">>> CrcLow : " + request[1]);
                        Console.WriteLine(">>> Third byte : " + request[2]);

                        if (ParseResponce(request.ToList(), SetRequestArr[SetRequestArr.Length - 2], SetRequestArr[SetRequestArr.Length - 1]))
                        {
                            Console.WriteLine(">>> Write Succeeded!");
                            return true;
                        }
                    }
                    tries--;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(">>> TCER_44: " + ex.Message + " tries " + tries + " Request");
                    tries--;
                }
            }
            return false;
        }

        private static void SendInitRequest(ushort DeviceAddress, byte function, byte RegisterAddress, byte[] Value)
        {
            Console.WriteLine(">>> Sending SET Request...");
            Console.Write("==========================================\n");

            byte[] SetRequestArr = new byte[Value.Length + 7];
            byte[] ID = BitConverter.GetBytes(DeviceAddress).Reverse().ToArray();

            SetRequestArr[0] = ID[0];
            SetRequestArr[1] = ID[1];
            SetRequestArr[2] = function;
            SetRequestArr[3] = HI(RegisterAddress);
            SetRequestArr[4] = LO(RegisterAddress);

            for (int i = 5; i < Value.Length + 5; i++)
            {
                SetRequestArr[i] = Value[i - 5];
            }

            ushort crc = CalcCRC16(SetRequestArr.Take(SetRequestArr.Length - 2).ToArray());
            SetRequestArr[SetRequestArr.Length - 2] = HI(crc);
            SetRequestArr[SetRequestArr.Length - 1] = LO(crc);

            for (int i = 0; i < SetRequestArr.Length; i++)
            {
                Console.Write("{0:X} ", SetRequestArr[i]);
            }
            Console.WriteLine();
            Console.Write("==========================================\n");

            RadioSerialPortBufferClear();
            byte[] request = new byte[128];
            int bytesRead = 0;

            Console.WriteLine(">>> Sending data"); // Will not Newer run 
            byte[] eCMD = { 0x2b, 0x2b, 0x2b };
            RadioSerialPort.Write(eCMD, 0, eCMD.Length);
            bytesRead = RadioSerialPort.Read(request, 0, RadioSerialPort.BytesToRead);
            Console.Write(">>> Byte" + Encoding.ASCII.GetString(request));

            Console.WriteLine(">>> Sending eATAP");
            byte[] eATAP = { 0x41, 0x54, 0x41, 0x50, 0x20, 0x31, 0xd, 0xa };
            RadioSerialPort.Write(eATAP, 0, eATAP.Length);
            Thread.Sleep(1000);
            bytesRead = RadioSerialPort.Read(request, 0, RadioSerialPort.BytesToRead);
            Console.Write(">>> Byte" + Encoding.ASCII.GetString(request));

            Console.WriteLine(">>> Sending eATRO");
            byte[] eATRO = { 0x41, 0x54, 0x52, 0x4f, 0x20, 0x31, 0x30, 0x30, 0x30, 0xd, 0xa };
            RadioSerialPort.Write(eATRO, 0, eATRO.Length);
            Thread.Sleep(1000);
            bytesRead = RadioSerialPort.Read(request, 0, RadioSerialPort.BytesToRead);
            Console.Write(">>> Byte" + Encoding.ASCII.GetString(request));

            Console.WriteLine(">>> Sending eATCN");
            byte[] eATCN = { 0x41, 0x54, 0x43, 0x4e, 0xd, 0xa };
            RadioSerialPort.Write(eATCN, 0, eATCN.Length);
            Thread.Sleep(1000);
            bytesRead = RadioSerialPort.Read(request, 0, RadioSerialPort.BytesToRead);
            Console.Write(">>> Byte" + Encoding.ASCII.GetString(request));

            Console.WriteLine(">>> Sending eEnter");
            byte[] eEnter = { 0xd, 0xa };
            RadioSerialPort.Write(eEnter, 0, eEnter.Length);
            Thread.Sleep(1000);

            RadioSerialPort.Write(SetRequestArr, 0, SetRequestArr.Length);

            Thread.Sleep(15000);
            bytesRead = RadioSerialPort.Read(request, 0, RadioSerialPort.BytesToRead);
            Console.WriteLine(">>> BytesRead : " + bytesRead);

            for (int i = 0; i < bytesRead; i++)
                Console.Write("Byte {0:X}", request[i]);

            Console.WriteLine("\nReceived String -->>  " + Encoding.ASCII.GetString(request));
            Console.WriteLine(">>> SerialRead succesfull");
            Console.Write(">>> Byteread = " + bytesRead);
            Console.Write(">>> SetRequestArr.Length = " + SetRequestArr.Length);
            request = request.Take(bytesRead).ToArray();

            Console.WriteLine(">>> CrcHi : " + request[0]);
            Console.WriteLine(">>> CrcLow : " + request[1]);
            Console.WriteLine(">>> Third byte : " + request[2]);

            ParseResponce(request.ToList(), SetRequestArr[SetRequestArr.Length - 2], SetRequestArr[SetRequestArr.Length - 1]);
        }

        public static bool ParseResponce(List<byte> packet, byte crcHi, byte crcLow)
        {
            List<int> indexes = new List<int>();
            bool crcOK = false;

            try
            {
                byte[] receive = new byte[2048];
                RadioSerialPort.Read(receive, 0, receive.Length); // Try to read data in buffer, maybe it is not empty
                packet.AddRange(receive.ToList());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " no bytes in buffer + ParseResponce()");
            }

            for (int i = 0; i < packet.Count; i++)
                Console.WriteLine("Received byte : " + packet[i]);

            Console.WriteLine(">>> CrcHi : {0:X}", crcHi);
            Console.WriteLine(">>> CrcLow : {0:X}", crcLow);

            try
            {
                for (int i = 0; i < packet.Count; i++)
                {
                    try
                    {
                        if (packet[i] == crcHi && packet[i + 1] == crcLow && packet[i + 2] == Convert.ToByte('&'))
                        {
                            Console.WriteLine("CRC OK");
                            indexes.Add(i);
                            indexes.Add(i + 1);
                            indexes.Add(i + 2);
                            crcOK = true;
                        }

                        if ((packet[i] == Convert.ToByte('O') && packet[i + 1] == Convert.ToByte('K')) || (packet[i] == Convert.ToByte('K') && packet[i + 1] == Convert.ToByte('O')))
                        {
                            indexes.Add(i);
                            indexes.Add(i + 1);
                        }
                        if ((packet[i] == 0x0A && packet[i + 1] == 0x0D) || (packet[i] == 0x0D && packet[i + 1] == 0x0A))
                        {
                            indexes.Add(i);
                            indexes.Add(i + 1);
                        }
                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        Console.WriteLine("ParsePacket IndexOut : " + ex.Message);
                    }
                }

                foreach (var i in indexes)
                {
                    packet[i] = 0x00;
                }

                packet.RemoveAll(x => x == 0x00);

                Console.WriteLine(Encoding.ASCII.GetString(packet.ToArray()));
                Console.WriteLine("Packet count : " + packet.Count);

                string[] data = Encoding.ASCII.GetString(packet.ToArray()).Split('&');

                foreach (var a in data)
                {
                    try
                    {
                        Console.WriteLine("\nReceived serial : " + a);
                        string[] info = a.Split('-');

                        if (info[2].Length == 16)
                        {
                            String serialNumber = info[2].ToString();
                            String receivedID = InsertLampAndGetID(serialNumber);

                            lock (commandsLocker)
                            {
                                if (!(SetCommands.Where(x => x.command.Equals("SetID-" + receivedID + "-" + serialNumber)).ToList().Any()))
                                    SetCommands.AddFirst(new ServerCommands("SetID-" + receivedID + "-" + serialNumber, 0, 0, 0, 0x5E, Convert.ToInt16(receivedID), 0x0A, false, serialNumber));
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }

                return crcOK;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " Error parse packet - ParseResponce()");
                return crcOK;
            }
        }

        public static String GetRadioSerial()
        {
            String serialRadio = String.Empty;
            bool readFlag = false;
            int count = 0;

            while (!readFlag)
            {
                if (count == 3)
                    return String.Empty;

                serialRadio = String.Empty;

                WriteCmd(RadioSerialPort, "+++");
                foreach (char c in WriteCmd(RadioSerialPort, "ATSH\n"))
                {
                    if (c >= '0' && c <= '9')
                    {
                        serialRadio += c;
                    }
                    else if (c == 'A' || c == 'B' || c == 'C' || c == 'D' || c == 'E' || c == 'F')
                        serialRadio += c;
                }

                foreach (char c in WriteCmd(RadioSerialPort, "ATSL\n"))
                {
                    if (c >= '0' && c <= '9')
                    {
                        serialRadio += c;
                    }
                    else if (c == 'A' || c == 'B' || c == 'C' || c == 'D' || c == 'E' || c == 'F')
                        serialRadio += c;
                }

                foreach (char c in WriteCmd(RadioSerialPort, "ATRN\n"))
                {
                    Console.WriteLine("Network status : " + c);
                }

                WriteCmd(RadioSerialPort, "ATCN\n");

                serialRadio = serialRadio.Replace(Convert.ToChar(0x0A).ToString(), "").Replace(Convert.ToChar(0x0D).ToString(), "");

                if (serialRadio != String.Empty && serialRadio.Length == 16)
                {
                    readFlag = true;
                }
                else
                {
                    RadioModuleReset();
                    count += 1;
                }

                Thread.Sleep(500);
            }

            return serialRadio;
        }

        private static void SoftwareReset() // Программный сброс радиомодуля
        {
            WriteCmd(RadioSerialPort, "+++");
            WriteCmd(RadioSerialPort, "ATFR\n");
            WriteCmd(RadioSerialPort, "ATCN\n");
        }

        private static void NetworkReset() // Сетевой сброс радиомодуля
        {
            WriteCmd(RadioSerialPort, "+++");
            WriteCmd(RadioSerialPort, "ATNR\n");
            WriteCmd(RadioSerialPort, "ATCN\n");
        }

        public static String WriteCmd(SerialPort port, string cmd)
        {
            try
            {
                byte[] receive = new byte[16];

                RadioSerialPort.Write(cmd);
                Thread.Sleep(1000);
                int bytesRead = port.Read(receive, 0, receive.Length);

                Console.WriteLine(">>> Command to radio : " + cmd);
                Console.Write(">>> Radio responce : ");

                for (int i = 0; i < bytesRead; i++)
                {
                    Console.Write(Convert.ToChar(receive[i]));
                }

                receive = receive.Take(bytesRead - 2).ToArray(); // Take bytesRead - 2 cause response last 2 bytes is 0x0A, 0x0D
                String answer = String.Empty;

                foreach (var a in receive)
                {
                    answer += Convert.ToChar(a);
                }
                return answer;
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_46: " + ex.Message + " WriteCmd");
                return String.Empty;
            }
        }

        public static void RadioModuleReset()
        {
            RadioResetGPIO.GPIOSetValue(GPIO.PIN_TO_LOW);
            Thread.Sleep(500);
            RadioResetGPIO.GPIOSetValue(GPIO.PIN_TO_HIGH);
            Thread.Sleep(1000);
        }

        private static void setBrightnesGroupModulator(string SlaveAdress, string Group, string percents)
        {
            try
            {
                UInt32 g = (Convert.ToUInt32(Group)) + (Convert.ToUInt32(percents) << 16);

                byte[] k = BitConverter.GetBytes(g);

                byte adress = Convert.ToByte(SlaveAdress);
                byte[] Initiaze_pak = new byte[] { adress, 0x10, 0x00, 0x01, 0x00, 2, 4, k[1], k[0], k[3], k[2], 0x0, 0x0 };
                CalcCRC16(Initiaze_pak);

                WritePacket(serialModulator, Initiaze_pak);
            }
            catch (Exception ex)
            {
                Console.WriteLine("TCER_18: " + ex.Message);
            }
        }

        private static void setBrightnesModulator(string SlaveAdress, string lamp_address, string percents)
        {
            try
            {
                {
                    UInt32 g = (Convert.ToUInt32(lamp_address)) + (Convert.ToUInt32(percents) << 16);


                    byte[] k = BitConverter.GetBytes(g);

                    byte adress = Convert.ToByte(SlaveAdress);
                    byte[] Initiaze_pak = new byte[] { adress, 0x10, 0x00, 0x02, 0x00, 2, 4, k[1], k[0], k[3], k[2], 0x0, 0x0 };

                    CalcCRC16(Initiaze_pak);

                    WritePacket(serialModulator, Initiaze_pak);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_19: " + ex.Message);
            }
        }

        private static void AddLamptoGroupModulator(string LampID, string GroupID, string SlaveAdress)
        {
            try
            {
                UInt32 g = (Convert.ToUInt32(LampID)) + (Convert.ToUInt32(GroupID) << 16);

                byte[] k = BitConverter.GetBytes(g);

                byte adress = Convert.ToByte(SlaveAdress);
                byte[] Initiaze_pak = new byte[] { adress, 0x10, 0x00, 0x03, 0x00, 2, 4, k[1], k[0], k[3], k[2], 0x0, 0x0 };

                CalcCRC16(Initiaze_pak);

                WritePacket(serialModulator, Initiaze_pak);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_20: " + ex.Message);
            }
        }

        private static void DeleteLamptoGroupModulator(string LampID, string GroupID, string SlaveAdress)
        {
            try
            {
                UInt32 g = (Convert.ToUInt32(LampID)) + (Convert.ToUInt32(GroupID) << 16);

                byte[] k = BitConverter.GetBytes(g);

                byte adress = Convert.ToByte(SlaveAdress);
                byte[] Initiaze_pak = new byte[] { adress, 0x10, 0x00, 0x04, 0x00, 2, 4, k[1], k[0], k[3], k[2], 0x0, 0x0 };

                CalcCRC16(Initiaze_pak);

                WritePacket(serialModulator, Initiaze_pak);
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_21: " + ex.Message);
            }
        }

        private static object JobQueueLocker = new object();
        private static List<List<string>> JobQueue;
        private static string PreviousCpommands = "";

        public static void ReceiveCommands()
        {
            TcpClient client = new TcpClient();
            NetworkStream stream;
            int bytesRead = 0;

            byte[] buffer = new byte[32];

            while (true)
            {
                try
                {
                    client = new TcpClient(Host, Port); // Trying to Connect to server

                    Console.WriteLine(">>> Connected");

                    client.Client.SendTimeout = 5000;
                    client.Client.ReceiveTimeout = 5000;

                    stream = new NetworkStream(client.Client); // Get server stream

                    String initialString = "RaspID" + RaspberryID + "$RadioSerial" + RadioSerialNumber + "$NikAddress" + NIK_Address;
                    stream.Write(Encoding.ASCII.GetBytes(initialString), 0, Encoding.ASCII.GetBytes(initialString).Length); // Sending RaspberryID to Server
                }
                catch (Exception connectException)
                {
                    Console.WriteLine(connectException.Message + " receiveCommands()");
                    Thread.Sleep(TimeSpan.FromSeconds(30));
                    continue;  // resume trying to connect ot server
                }

                Console.WriteLine(">>> Sended RaspberryID - " + RaspberryID + " to : " + Host + " on port - " + Port);

                while (true)
                {
                    try
                    {
                        if (!(IsConnected(client.Client)))
                            throw new Exception("Socket is not connected!");

                        client.Client.Blocking = false;

                        if (stream.DataAvailable) // if stream isn't empty
                        {
                            bytesRead = stream.Read(buffer, 0, buffer.Length); // read buffer

                            if (!(bytesRead > 0)) continue; // if buffer is empty go to next reading

                            string cmd = Encoding.ASCII.GetString(buffer, 0, bytesRead); // convert bytes to string command

                            Console.WriteLine(cmd);
                            PreviousCpommands += cmd;

                            while (PreviousCpommands.Contains(";"))
                            {
                                string oneCommand = "";

                                oneCommand = PreviousCpommands.Substring(0, PreviousCpommands.IndexOf(";", 0));

                                string[] commands = oneCommand.Split('$');

                                PreviousCpommands = PreviousCpommands.Remove(0, (oneCommand + ";").Length);
                                List<string> g = new List<string>();

                                for (int i = 0; i < commands.Count(); i++)
                                {
                                    g.Add(commands[i]);
                                }
                                JobQueue.Add(g);
                            }

                            Thread.Sleep(300);
                        }
                        Thread.Sleep(300);
                    }
                    catch (Exception receiveException)
                    {
                        Console.WriteLine(">>> " + receiveException.Message + " receiveCommands()");
                        client.Client.Close();
                        break; // begin trying to connect to server
                    }
                }
            }
        }

        public static void SetBrToBaseGroup(string GroupID, string value)
        {
            try
            {
                string cs = @"server=localhost;server=localhost;userid=root;
                              password=raspberry;database=RaspDB";

                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                string sql1 = "Select *  From LampGroupsRelatives where GroupID= " + GroupID + ";";

                MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                MySqlDataReader rdr1 = cmd1.ExecuteReader();

                while (rdr1.Read())
                {
                    var hs = (rdr1.GetString(1));
                    setBrToBase(hs, value);
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_24: " + ex.Message);
            }
        }

        public static void setBrToBase(string stationEquipID, string value)
        {
            try
            {
                string cs = @"server=localhost;server=localhost;userid=root;
                              password=raspberry;database=RaspDB";

                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                string sql1 = "UPDATE LampForNIK SET Brightness = " + value +
                " where StationEquipmentID = " + stationEquipID + ";";

                MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                MySqlDataReader rdr1 = cmd1.ExecuteReader();

                while (rdr1.Read())
                {
                    var hs = (rdr1.GetString(0));
                    Console.WriteLine("setBrToBase --> HS = " + hs);
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>> TCER_25: " + ex.Message);
            }
        }

        public static List<List<string>> getInformATION(string GroupID)
        {
            string cs = @"server=localhost;server=localhost;userid=root;
                          password=raspberry;database=RaspDB";

            MySqlConnection conn = new MySqlConnection(cs);
            conn.Open();

            string sql0 = "SELECT GroupID FROM LampGroups where ViewGroupID =" + GroupID + ";";

            MySqlCommand cmd0 = new MySqlCommand(sql0, conn);
            MySqlDataReader rdr0 = cmd0.ExecuteReader();

            int GrouID0 = 0;

            while (rdr0.Read())
            {
                GrouID0 = Convert.ToInt32(rdr0.GetString(0));
            }
            conn.Close();

            List<string> b = new List<string>();
            b.Add(GrouID0.ToString());

            /////
            //ТУПО ПОСЫЛАЮ НА ВСЕХ

            conn.Open();
            string sql01 = "Select * From StationEquipmentIdToSerialNumberSlaves;";

            MySqlCommand cmd01 = new MySqlCommand(sql01, conn);
            MySqlDataReader rdr01 = cmd01.ExecuteReader();

            List<List<string>> Job = new List<List<string>>();
            Job.Add(b);

            while (rdr01.Read())
            {
                List<string> bb = new List<string>();
                bb.Add(rdr01.GetString(2));
                bb.Add(rdr01.GetString(3));
                Job.Add(bb);
            }
            conn.Close();

            return Job;
        }

        public static List<string> getInform(int LampEquipmentID)
        {
            // connection string
            string cs = @"server=localhost;server=localhost;userid=root;
                          password=raspberry;database=RaspDB";

            MySqlConnection conn = new MySqlConnection(cs);
            conn.Open();

            string sql = "Select * From StationEquipmentIdToSerialNumber where StationEquipmentID  = " + LampEquipmentID + ";";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            string c1 = " ";
            string c2 = " ";
            string c3 = " ";

            while (rdr.Read())
            {
                c1 = rdr.GetString(1);
                c2 = rdr.GetString(2);
                c3 = rdr.GetString(3);
            }
            conn.Close();

            string sql1 = "Select * From StationEquipmentIdToSerialNumberSlaves where StationEquipmentID  = " + c3 + ";";
            conn.Open();

            MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
            MySqlDataReader rdr1 = cmd1.ExecuteReader();

            string cc1 = " ";
            string cc2 = " ";
            string EquipmentType = " ";

            while (rdr1.Read())
            {
                cc1 = rdr1.GetString(1);
                cc2 = rdr1.GetString(2);
                EquipmentType = rdr1.GetString(3);
            }

            conn.Close();

            List<string> s = new List<string>(); //LampDerive, SlaveDerive, SlaveAdress, SlaveType
            s.Add(c2);
            s.Add(cc1);
            s.Add(cc2);
            s.Add(EquipmentType);

            return s;
        }

        public static void JobQueueWorkControl()
        {
            while (true)
            {
                string cs = @"server=localhost;server=localhost;userid=root;
                              password=raspberry;database=RaspDB";
                try
                {
                    if (JobQueue.Count > 0)
                    {
                        List<string> s = JobQueue.First();
                        Console.WriteLine(s.First());

                        switch (s.First())
                        {
                            default:
                                {
                                    Console.WriteLine(">>> WTF is This+:" + JobQueue.First());
                                }
                                break;

                            case "DeleteLampinGroup":
                                {
                                    MySqlConnection conn = new MySqlConnection(cs);
                                    conn.Open();

                                    string sql0 = "SELECT GroupID FROM LampGroups where ViewGroupID =" + s.ElementAt(1) + ";";

                                    MySqlCommand cmd0 = new MySqlCommand(sql0, conn);
                                    MySqlDataReader rdr0 = cmd0.ExecuteReader();

                                    int GroupID = 0;

                                    while (rdr0.Read())
                                    {
                                        GroupID = Convert.ToInt32(rdr0.GetString(0));
                                    }
                                    conn.Close();

                                    conn.Open();

                                    string sql = "SELECT * FROM LampGroupsRelatives where GroupID = " + s.ElementAt(1) + " and StationEquipmentID =" + s.ElementAt(2) + ";";

                                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                                    MySqlDataReader rdr = cmd.ExecuteReader();

                                    bool exist = false;
                                    while (rdr.Read())
                                    {
                                        exist = true;
                                    }
                                    conn.Close();

                                    if (exist)
                                    {
                                        conn.Open();

                                        string sql1 = "DELETE FROM LampGroupsRelatives WHERE GroupID = " + s.ElementAt(1) + " and StationEquipmentID=" + s.ElementAt(2) + ";";

                                        MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                        MySqlDataReader rdr1 = cmd1.ExecuteReader();

                                        while (rdr1.Read())
                                        {

                                        }
                                        conn.Close();

                                        //LampDerive, SlaveDerive, SlaveAdress, SlaveType
                                        List<string> inform = getInform(Convert.ToInt32(s.ElementAt(2)));

                                        Console.WriteLine("-->>>   StationEquipmentSlaveID " + inform.ElementAt(3));

                                        if (Convert.ToInt32(inform.ElementAt(3)) == 20) // Мой Модулятор
                                        {
                                            DeleteLamptoGroupModulator(s.ElementAt(2), GroupID.ToString(), inform.ElementAt(2));
                                            Thread.Sleep(TimeSpan.FromSeconds(10));
                                        }

                                        if (Convert.ToInt32(inform.ElementAt(3)) == 21) // Сереги Радиомодуль
                                        {
                                            lock (commandsLocker)
                                            {
                                                //DeleteLampInGroup-StationEquipmentID-GROUP_ID
                                                try
                                                {
                                                    Console.WriteLine("DeleteLampFrom Group Comes");
                                                    ServerCommands command = SetCommands.Where(x => x.ID == Convert.ToInt16(s.ElementAt(1)) && x.register == 0x64 && x.functionCode == 0x0A).First();

                                                    if (command.value != GroupID)
                                                    {
                                                        command.value = (short)GroupID;
                                                        command.commandStatus = false;
                                                        // VALUES (LampID, functionCode, register, GroupID, false, commandFailedCount);
                                                        ExecDBString("INSERT INTO Commands VALUES(" + s.ElementAt(2) + ", 0x0A, 0x64, " + GroupID + ", false, 0);");
                                                    }

                                                }
                                                catch (Exception ex)
                                                {
                                                    Console.WriteLine("Delete Lamp from group JobQueu : " + ex.Message);
                                                    SetCommands.AddFirst(new ServerCommands("DeleteLampFromGroup-" + s.ElementAt(1) + "-" + GroupID.ToString(), 0, 0, Convert.ToInt16(s.ElementAt(2)), 0x64, (short)GroupID, 0x0A, false));
                                                    String sqlStr = "INSERT INTO Commands VALUES(" + s.ElementAt(2) + ", 0x0A, 0x64, " + GroupID + ", false, 0);";
                                                    Console.WriteLine(sqlStr);
                                                    ExecDBString(sqlStr);
                                                }
                                            }
                                        }
                                    }
                                    JobQueue.RemoveAt(0);
                                }
                                break;
                            case "DeleteGroup":
                                {
                                    MySqlConnection conn = new MySqlConnection(cs);
                                    conn.Open();

                                    string sql0 = "SELECT GroupID FROM LampGroups where ViewGroupID =" + s.ElementAt(1) + ";";

                                    MySqlCommand cmd0 = new MySqlCommand(sql0, conn);
                                    MySqlDataReader rdr0 = cmd0.ExecuteReader();

                                    int GrouID0 = 0;

                                    while (rdr0.Read())
                                    {
                                        GrouID0 = Convert.ToInt32(rdr0.GetString(0));
                                    }
                                    conn.Close();

                                    if (GrouID0 != 0) //Если она есть
                                    {
                                        conn.Open();
                                        string sql1 = "UPDATE LampGroups Set ViewGroupID = NULL WHERE ViewGroupID = " + s.ElementAt(1) + ";";

                                        MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                        MySqlDataReader rdr1 = cmd1.ExecuteReader();

                                        while (rdr1.Read())
                                        {

                                        }
                                        conn.Close();
                                    }
                                    JobQueue.RemoveAt(0);
                                }
                                break;
                            case "GsBrightness":
                                {
                                    List<List<string>> inform = getInformATION(s.ElementAt(1));
                                    {
                                        string groupID = inform.First().First();

                                        for (int i = 1; i < inform.Count; i++)
                                        {
                                            if (inform.ElementAt(i).ElementAt(1) == "20")// 
                                            {
                                                inform.ElementAt(0);
                                                setBrightnesGroupModulator(inform.ElementAt(i).ElementAt(0), groupID, s.ElementAt(2));
                                            }
                                            if (Convert.ToInt32(inform.ElementAt(i).ElementAt(1)) == 21)// 
                                            {
                                                lock (commandsLocker)
                                                {
                                                    try
                                                    {
                                                        ServerCommands command = SetCommands.Where(x => x.ID == Convert.ToInt16(groupID) && x.register == 0x5F && x.functionCode == 0x0F).First();
                                                        if (command.value != Convert.ToInt16(s.ElementAt(2)))
                                                        {
                                                            command.value = Convert.ToInt16(s.ElementAt(2));
                                                            command.commandStatus = false;
                                                            ExecDBString("Update Commands SET value = " + groupID + ", status = false  WHERE ID = " + groupID + " AND register = " + 0x5F + " AND functionCode = 0x0F;");
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        // GsBrigghtness-GROUP_ID-percent
                                                        Console.WriteLine(">>>   " + ex.Message + " No intems is commands list");

                                                        // VALUES (LampID or GROUP_ID, functionCode, register, GroupID, false, commandFailedCount);
                                                        SetCommands.AddFirst(new ServerCommands("GSBrightness-" + groupID + "-" + s.ElementAt(2), 0, 0, Convert.ToInt16(groupID), 0x5F, Convert.ToInt16(s.ElementAt(2)), 0x0F, true));// if we receive command to change PWM
                                                        String sql = "INSERT INTO Commands VALUES(" + groupID + ", 0x0F, 0x5F, " + s.ElementAt(2) + ", false, 0);";
                                                        ExecDBString(sql);
                                                    }
                                                }
                                            }
                                        }
                                        SetBrToBaseGroup(s.ElementAt(1), s.ElementAt(2));
                                        JobQueue.RemoveAt(0);
                                    }
                                }
                                break;
                            case "GON":
                                {
                                    List<List<string>> inform = getInformATION(s.ElementAt(1));
                                    {
                                        string groupID = inform.First().First();
                                        Console.WriteLine(">>>   GON inform count = " + inform.Count);
                                        Console.WriteLine(">>>   GroupID = " + groupID);

                                        for (int i = 1; i < inform.Count; i++)
                                        {
                                            if (inform.ElementAt(i).ElementAt(1) == "20")
                                            {
                                                inform.ElementAt(0);
                                                setBrightnesGroupModulator(inform.ElementAt(i).ElementAt(0), groupID, "100");
                                            }
                                            if (Convert.ToInt32(inform.ElementAt(i).ElementAt(1)) == 21)
                                            {
                                                lock (commandsLocker)
                                                {
                                                    // GON-GROUP_ID_1 *** 1 - Mean that parameter on = 1 = enable
                                                    try
                                                    {
                                                        ServerCommands command = SetCommands.Where(x => x.ID == Convert.ToInt16(groupID) && x.register == 0x5D && x.functionCode == 0x0F).First();
                                                        if (command.value != 1)
                                                        {
                                                            command.value = 1;
                                                            command.commandStatus = false;
                                                            ExecDBString("Update Commands SET value = " + 1 + ", status = false  WHERE ID = " + groupID + " AND register = " + 0x5F + " AND functionCode = 0x0F;");
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Console.WriteLine("Gon -> " + ex.Message);
                                                        SetCommands.AddFirst(new ServerCommands("Gon-" + groupID + "-" + "1", 0, 0, Convert.ToInt16(groupID), 0x5D, 1, 0x0F, true));
                                                        String sql = "INSERT INTO Commands VALUES(" + groupID + ", 0x0F, 0x5D, " + 1 + ", false, 0);";
                                                        ExecDBString(sql);
                                                    }
                                                }
                                            }
                                        }
                                        SetBrToBaseGroup(s.ElementAt(1), "100");
                                        JobQueue.RemoveAt(0);
                                    }
                                }
                                break;
                            case "GOFF":
                                {
                                    List<List<string>> inform = getInformATION(s.ElementAt(1));
                                    {
                                        string groupID = inform.First().First();
                                        Console.WriteLine(">>>   GOFF inform count = " + inform.Count);
                                        Console.WriteLine(">>>   GroupID = " + groupID);

                                        for (int i = 1; i < inform.Count; i++)
                                        {
                                            if (inform.ElementAt(i).ElementAt(1) == "20")// Сереги Радиомодуль
                                            {
                                                inform.ElementAt(i).ElementAt(0);
                                                setBrightnesGroupModulator(inform.ElementAt(i).ElementAt(0), groupID, "0");
                                            }
                                            if (Convert.ToInt32(inform.ElementAt(i).ElementAt(1)) == 21)// Сереги Радиомодуль
                                            {
                                                lock (commandsLocker)
                                                {
                                                    try
                                                    {
                                                        ServerCommands command = SetCommands.Where(x => x.ID == Convert.ToInt16(groupID) && x.register == 0x5D && x.functionCode == 0x0F).First();
                                                        if (command.value != 0)
                                                        {
                                                            command.value = 0;
                                                            command.commandStatus = false;
                                                            ExecDBString("Update Commands SET value = " + 0 + ", status = false  WHERE ID = " + groupID + " AND register = " + 0x5D + " AND functionCode = 0x0F;");
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Console.WriteLine("Goff -> " + ex.Message);
                                                        SetCommands.AddFirst(new ServerCommands("Goff-" + groupID + "-" + "1", 0, 0, Convert.ToInt16(groupID), 0x5D, 0, 0x0F, true));
                                                        String sql = "INSERT INTO Commands VALUES(" + groupID + ", 0x0F, 0x5D, " + 0 + ", false, 0);";
                                                        Console.WriteLine(sql);
                                                        ExecDBString(sql);
                                                    }
                                                }
                                            }
                                        }
                                        SetBrToBaseGroup(s.ElementAt(1), "0");
                                        JobQueue.RemoveAt(0);
                                    }
                                }
                                break;
                            case "AddSlave":
                                {
                                    try
                                    {
                                        MySqlConnection conn = new MySqlConnection(cs);
                                        conn.Open();

                                        {
                                            string sql = "Select * From StationEquipmentIdToSerialNumberSlaves where StationEquipmentID  = " + s.ElementAt(1) + ";";

                                            MySqlCommand cmd = new MySqlCommand(sql, conn);
                                            MySqlDataReader rdr = cmd.ExecuteReader();

                                            int existALL = 0;
                                            bool ExistID = false;

                                            while (rdr.Read())
                                            {
                                                if ((rdr.GetString(0)) == s.ElementAt(1))
                                                {
                                                    existALL++; ExistID = true;
                                                }
                                                if ((rdr.GetString(1)) == s.ElementAt(2))
                                                {
                                                    existALL++;
                                                }
                                                if ((rdr.GetString(2)) == s.ElementAt(3))
                                                {
                                                    existALL++;
                                                }
                                                if ((rdr.GetString(3)) == s.ElementAt(4))
                                                {
                                                    existALL++;
                                                }
                                                //  StationEquipmentID = rdr.GetString(0); // getting StationEquipmentID wich applies our received SerialNumber
                                            }
                                            conn.Close();

                                            if (!ExistID)  //Если нет то добавить
                                            {
                                                string sql1 = "INSERT INTO StationEquipmentIdToSerialNumberSlaves VALUES(" +
                                                        s.ElementAt(1) + ", '" + s.ElementAt(2) + "'," + s.ElementAt(3) + "," + s.ElementAt(4) + ");";
                                                conn.Open();

                                                Console.WriteLine("Add slave SQL Request ==>> " + sql1);

                                                MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                                MySqlDataReader rdr1 = cmd1.ExecuteReader();
                                                while (rdr1.Read())
                                                {
                                                    var hs = (rdr1.GetString(0));
                                                    Console.WriteLine("AddSlave HS : " + hs);
                                                }
                                                conn.Close();
                                            }
                                            else
                                            {
                                                if (existALL != 4) // если не сходится то обновить
                                                {
                                                    string sql1 = "UPDATE StationEquipmentIdToSerialNumberSlaves SET " +
                                                           "SerialNumber = '" + s.ElementAt(2) +
                                                           "',DeviceAdress = " + s.ElementAt(3) +
                                                           ",EquipmentTypeID = " + s.ElementAt(4) +
                                                           " where StationEquipmentID = " + s.ElementAt(1) + ";";

                                                    conn.Open();
                                                    MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                                    MySqlDataReader rdr1 = cmd1.ExecuteReader();

                                                    while (rdr1.Read())
                                                    {
                                                        var hs = (rdr1.GetString(0));
                                                        Console.WriteLine("AddSlave hs = " + hs);
                                                    }
                                                    conn.Close();
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine(">>> TCER_28: " + ex.Message);
                                    }
                                    JobQueue.RemoveAt(0);
                                }
                                break;
                            case "AddLamp":
                                {
                                    try
                                    {
                                        MySqlConnection conn = new MySqlConnection(cs);
                                        conn.Open();

                                        string sql0 = "Select * From StationEquipmentIdToSerialNumberSlaves where StationEquipmentId  = " + s.ElementAt(2) + ";";

                                        MySqlCommand cmd0 = new MySqlCommand(sql0, conn);
                                        MySqlDataReader rdr0 = cmd0.ExecuteReader();

                                        string EquipType = "";

                                        while (rdr0.Read())
                                        {
                                            EquipType = rdr0.GetString(3);
                                        }
                                        conn.Close();
                                        conn.Open();

                                        if (EquipType == "22")
                                        {
                                            /*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-*/
                                            string sql = "Select * From LampForNIK where StationEquipmentID  = " + s.ElementAt(1) + ";";

                                            MySqlCommand cmd = new MySqlCommand(sql, conn);
                                            MySqlDataReader rdr = cmd.ExecuteReader();

                                            int existALL = 0;
                                            bool ExistID = false;
                                            while (rdr.Read())
                                            {
                                                if ((rdr.GetString(1)) == s.ElementAt(1)) // StationEquipmentID
                                                {
                                                    existALL++; ExistID = true;
                                                }
                                                if ((rdr.GetString(2)) == s.ElementAt(3)) //SerialNumber
                                                {
                                                    existALL++;
                                                }
                                                if ((rdr.GetString(3)) == s.ElementAt(2)) //SlaveID
                                                {
                                                    existALL++;
                                                }
                                                if ((rdr.GetString(5)) == s.ElementAt(4)) //SlaveID
                                                {
                                                    existALL++;
                                                }
                                                if ((rdr.GetString(8)) == s.ElementAt(5)) //SlaveID
                                                {
                                                    existALL++;
                                                }
                                                //  StationEquipmentID = rdr.GetString(0); // getting StationEquipmentID wich applies our received SerialNumber
                                            }
                                            conn.Close();

                                            if (!ExistID)  //Если нет то добавить
                                            {
                                                /*
                                                ID int(11) NOT NULL AUTO_INCREMENT
                                                StationEquipmentID int(11)
                                                SerialNumber varchar(255)
                                                SlaveID int(11)
                                                Brightness int(11)
                                                PhaseNumber int(11)
                                                SummaryPover int(11)
                                                CurrentPover int(11)
                                                */

                                                string sql1 = "INSERT INTO LampForNIK VALUES(null, " +
                                                    s.ElementAt(1) + ", \'" + s.ElementAt(3) + "\', " + s.ElementAt(2) + ", " + 0 + ", " + s.ElementAt(4) + ", " + 0 + ", " + 0 + ", " + 0 + ", " + 0 + ");";

                                                Console.WriteLine(">>> SQL INSERT REQUEST --->>> " + sql1);

                                                conn.Open();

                                                MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                                MySqlDataReader rdr1 = cmd1.ExecuteReader();

                                                while (rdr1.Read())
                                                {
                                                    var hs = (rdr1.GetString(0));
                                                    Console.WriteLine("AddLamp hs = " + hs);
                                                }

                                                conn.Close();
                                            }
                                            else
                                            {
                                                if (existALL != 5) // если не сходится то обновить
                                                {
                                                    string sql1 = "UPDATE LampForNIK SET " +
                                                           "SerialNumber = \'" + s.ElementAt(3) +
                                                           "\', SlaveID = " + s.ElementAt(2) +
                                                           ", PhaseNumber = " + s.ElementAt(4) +
                                                           ", Nominal = " + s.ElementAt(5) +
                                                           " WHERE StationEquipmentID = " + s.ElementAt(1) + ";";

                                                    conn.Open();
                                                    MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                                    MySqlDataReader rdr1 = cmd1.ExecuteReader();
                                                    while (rdr1.Read())
                                                    {
                                                        var hs = (rdr1.GetString(0));
                                                        Console.WriteLine("AddLamp hs = " + hs);
                                                    }
                                                    conn.Close();
                                                }
                                            }
                                            /*_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-*/
                                        }
                                        else
                                        {
                                            string sql = "Select * From StationEquipmentIdToSerialNumber where StationEquipmentID  = " + s.ElementAt(1) + ";";

                                            MySqlCommand cmd = new MySqlCommand(sql, conn);
                                            MySqlDataReader rdr = cmd.ExecuteReader();

                                            int existALL = 0;
                                            bool ExistID = false;
                                            while (rdr.Read())
                                            {
                                                if ((rdr.GetString(1)) == s.ElementAt(1))
                                                {
                                                    existALL++; ExistID = true;
                                                }
                                                if ((rdr.GetString(2)) == s.ElementAt(3))
                                                {
                                                    existALL++;
                                                }
                                                if ((rdr.GetString(3)) == s.ElementAt(2))
                                                {
                                                    existALL++;
                                                }

                                                //  StationEquipmentID = rdr.GetString(0); // getting StationEquipmentID wich applies our received SerialNumber
                                            }
                                            conn.Close();

                                            if (!ExistID)  //Если нет то добавить
                                            {
                                                string sqlStr;
                                                if (Convert.ToInt32(EquipType) == 19 || Convert.ToInt32(EquipType) == 21)
                                                {
                                                    sqlStr = "INSERT INTO StationEquipmentIdToSerialNumber VALUES( NULL, " + s.ElementAt(1) + ", \'" + s.ElementAt(3) + "\', " + s.ElementAt(2) + ");";

                                                    conn.Open();

                                                    MySqlCommand cmd1 = new MySqlCommand(sqlStr, conn);
                                                    MySqlDataReader rdr1 = cmd1.ExecuteReader();

                                                    while (rdr1.Read())
                                                    {
                                                        var hs = (rdr1.GetString(0));
                                                        Console.WriteLine("AddLamp hs = " + hs);
                                                    }
                                                    conn.Close();
                                                }
                                            }
                                            else
                                            {
                                                if (existALL != 3) // если не сходится то обновить
                                                {
                                                    string sql1 = "UPDATE StationEquipmentIdToSerialNumber SET " +
                                                           "StationEquipmentID  = " + s.ElementAt(1) +
                                                           ",   SerialNumber  = \'" + s.ElementAt(3) +
                                                           "\', SlaveID  = " + s.ElementAt(2) +
                                                           " WHERE StationEquipmentID = " + s.ElementAt(1) + ";";

                                                    conn.Open();

                                                    MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                                    MySqlDataReader rdr1 = cmd1.ExecuteReader();
                                                    while (rdr1.Read())
                                                    {
                                                        var hs = (rdr1.GetString(0));
                                                        Console.WriteLine("AddLamp hs = " + hs);
                                                    }
                                                    conn.Close();
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine(">>> TCER_29: " + ex.Message);
                                    }
                                    JobQueue.RemoveAt(0);
                                }
                                break;
                            case "sBrightness":
                                {
                                    //LampDerive, SlaveDerive, SlaveAdress, SlaveType
                                    List<string> inform = getInform(Convert.ToInt32(s.ElementAt(1)));

                                    Console.WriteLine("-->>>   StationEquipmentSlaveID " + inform.ElementAt(3));

                                    if (Convert.ToInt32(inform.ElementAt(3)) == 20)// Мой Модулятор
                                    {
                                        setBrightnesModulator(inform.ElementAt(2), inform.ElementAt(0), s.ElementAt(2));
                                        Thread.Sleep(TimeSpan.FromSeconds(10));
                                    }

                                    if (Convert.ToInt32(inform.ElementAt(3)) == 21) // Сереги Радиомодуль
                                    {
                                        lock (commandsLocker)
                                        {
                                            try
                                            {
                                                ServerCommands command = SetCommands.Where(x => x.ID == Convert.ToInt16(s.ElementAt(1)) && x.register == 0x5F && x.functionCode == 0x0A).First();

                                                if (command.value != Convert.ToInt16(s.ElementAt(2)))
                                                {
                                                    command.value = Convert.ToInt16(s.ElementAt(2));
                                                    command.commandsCount = 0;
                                                    command.commandStatus = false;
                                                    ExecDBString("Update Commands SET value = " + s.ElementAt(2) + ", status = false, count = 0 WHERE ID = " + Convert.ToInt16(s.ElementAt(1)) + " AND register = " + 0x5F + " AND functionCode = 0x0A ;");
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Console.WriteLine("JobQuequ sBrightness " + ex.Message);
                                                SetCommands.AddFirst(new ServerCommands("sBrightness-" + s.ElementAt(1) + "-" + s.ElementAt(2), 0, 0, Convert.ToInt16(s.ElementAt(1)), 0x5F, Convert.ToInt16(s.ElementAt(2)), 0x0A, false));// if we receive command to change PWM
                                                String sql = "INSERT INTO Commands VALUES(" + s.ElementAt(1) + ", 0x0A, 0x5F, " + s.ElementAt(2) + ", false, 0);";
                                                ExecDBString(sql);
                                            }
                                        }
                                    }
                                    setBrToBase(s.ElementAt(1), s.ElementAt(2));
                                    JobQueue.RemoveAt(0);
                                }
                                break;
                            case "ON":
                                {
                                    //LampDerive, SlaveDerive, SlaveAdress, SlaveType
                                    List<string> inform = getInform(Convert.ToInt32(s.ElementAt(2)));

                                    Console.WriteLine("-->>>   StationEquipmentSlaveID " + inform.ElementAt(3));

                                    if (Convert.ToInt32(inform.ElementAt(3)) == 20)// Мой Модулятор
                                    {
                                        setBrightnesModulator(inform.ElementAt(2), inform.ElementAt(0), "100");
                                        Thread.Sleep(TimeSpan.FromSeconds(10));
                                    }
                                    if (Convert.ToInt32(inform.ElementAt(3)) == 21)// Сереги Радиомодуль
                                    {
                                        lock (commandsLocker)
                                        {
                                            try
                                            {
                                                ServerCommands command = SetCommands.Where(x => x.ID == Convert.ToInt16(s.ElementAt(2)) && x.register == 0x5D && x.functionCode == 0x0A).First();
                                                if (command.value != 1)
                                                {
                                                    command.value = 1;
                                                    command.commandsCount = 0;
                                                    command.commandStatus = false;
                                                    ExecDBString("Update Commands SET value = " + 1 + ", status = false, count = 0 WHERE ID = " + s.ElementAt(2) + " AND register = " + 0x5D + " AND functionCode = 0x0A;");
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Console.WriteLine("JobQuequ ON " + ex.Message);
                                                SetCommands.AddFirst(new ServerCommands("ON-1-" + s.ElementAt(2), 0, 0, Convert.ToInt16(s.ElementAt(2)), 0x5D, 1, 0x0A, true));
                                                String sql = "INSERT INTO Commands VALUES(" + s.ElementAt(2) + ", 0x0A, 0x5D, 1, false, 0);";
                                                ExecDBString(sql);
                                            }
                                        }
                                    }
                                    setBrToBase(s.ElementAt(1), "100");
                                    JobQueue.RemoveAt(0);
                                }
                                break;
                            case "OFF":
                                {
                                    //LampDerive, SlaveDerive, SlaveAdress, SlaveType
                                    List<string> inform = getInform(Convert.ToInt32(s.ElementAt(2)));

                                    if (Convert.ToInt32(inform.ElementAt(3)) == 20)// Мой Модулятор
                                    {
                                        setBrightnesModulator(inform.ElementAt(2), inform.ElementAt(0), "0");
                                        Thread.Sleep(TimeSpan.FromSeconds(10));
                                    }
                                    if (Convert.ToInt32(inform.ElementAt(3)) == 21)// Сереги Радиомодуль
                                    {
                                        lock (commandsLocker)
                                        {
                                            try
                                            {
                                                ServerCommands command = SetCommands.Where(x => x.ID == Convert.ToInt16(s.ElementAt(2)) && x.register == 0x5D && x.functionCode == 0x0A).First();
                                                if (command.value != 0)
                                                {
                                                    command.value = 0;
                                                    command.commandsCount = 0;
                                                    command.commandStatus = false;
                                                    ExecDBString("Update Commands SET value = " + 1 + ", status = false, count = 0 WHERE ID = " + s.ElementAt(2) + " AND register = " + 0x5D + " AND functionCode = 0x0A;");
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Console.WriteLine("JobQuequ OFF " + ex.Message);
                                                SetCommands.AddFirst(new ServerCommands("OFF-1-" + s.ElementAt(2), 0, 0, Convert.ToInt16(s.ElementAt(2)), 0x5D, 0, 0x0A, true));
                                                String sql = "INSERT INTO Commands VALUES(" + s.ElementAt(2) + ", 0x0A, 0x5D, 0, false, 0);";
                                                ExecDBString(sql);
                                            }
                                        }
                                    }
                                    setBrToBase(s.ElementAt(1), "0");
                                    JobQueue.RemoveAt(0);
                                }
                                break;
                            case "AddGroup":
                                {
                                    MySqlConnection conn = new MySqlConnection(cs);
                                    conn.Open();

                                    string sql0 = "SELECT GroupID FROM LampGroups where ViewGroupID = " + s.ElementAt(1) + ";";

                                    MySqlCommand cmd0 = new MySqlCommand(sql0, conn);
                                    MySqlDataReader rdr0 = cmd0.ExecuteReader();

                                    int GrouID0 = 0;
                                    while (rdr0.Read())
                                    {
                                        GrouID0 = Convert.ToInt32(rdr0.GetString(0));
                                    }
                                    conn.Close();

                                    if (GrouID0 == 0)//Если ент группы еще
                                    {
                                        conn.Open();

                                        string sql = "SELECT GroupID FROM LampGroups where ViewGroupID is null LIMIT 1;";

                                        MySqlCommand cmd = new MySqlCommand(sql, conn);
                                        MySqlDataReader rdr = cmd.ExecuteReader();

                                        int GrouID = 0;
                                        while (rdr.Read())
                                        {
                                            GrouID = Convert.ToInt32(rdr.GetString(0));
                                        }
                                        conn.Close();

                                        if (GrouID != 0) // Иначе кончились свободніе групппы для фонарей
                                        {
                                            conn.Open();
                                            string sql1 = "UPDATE LampGroups SET ViewGroupID = " + s.ElementAt(1) + " WHERE GroupID = " + GrouID + ";";

                                            MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                            MySqlDataReader rdr1 = cmd1.ExecuteReader();

                                            while (rdr1.Read())
                                            {

                                            }
                                            conn.Close();
                                        }
                                    }
                                    JobQueue.RemoveAt(0);
                                }
                                break;
                            case "AddLamptoGroup":
                                {
                                    try
                                    {
                                        for (int i = 0; i < s.Count; i++)
                                        {
                                            Console.WriteLine("Index " + i + " : " + s[i]);
                                        }

                                        MySqlConnection conn = new MySqlConnection(cs);
                                        conn.Open();

                                        string sql0 = "SELECT GroupID FROM LampGroups where ViewGroupID =" + s.ElementAt(1) + ";";

                                        MySqlCommand cmd0 = new MySqlCommand(sql0, conn);
                                        MySqlDataReader rdr0 = cmd0.ExecuteReader();

                                        int GroupID = 0;
                                        while (rdr0.Read())
                                        {
                                            GroupID = Convert.ToInt32(rdr0.GetString(0));
                                        }
                                        conn.Close();

                                        conn.Open();

                                        string sql = "SELECT * FROM LampGroupsRelatives where GroupID = " + s.ElementAt(1) + " and StationEquipmentID =" + s.ElementAt(2) + ";";

                                        MySqlCommand cmd = new MySqlCommand(sql, conn);
                                        MySqlDataReader rdr = cmd.ExecuteReader();

                                        bool exist = false;
                                        while (rdr.Read())
                                        {
                                            exist = true;
                                        }
                                        conn.Close();

                                        if (!exist)
                                        {
                                            conn.Open();

                                            string sql1 = "INSERT INTO LampGroupsRelatives VALUES(" + s.ElementAt(1) + ", " + s.ElementAt(2) + "); ";

                                            MySqlCommand cmd1 = new MySqlCommand(sql1, conn);
                                            MySqlDataReader rdr1 = cmd1.ExecuteReader();

                                            while (rdr1.Read())
                                            {

                                            }
                                            conn.Close();

                                            //LampDerive, SlaveDerive, SlaveAdress, SlaveType
                                            List<string> inform = getInform(Convert.ToInt32(s.ElementAt(2)));

                                            if (Convert.ToInt32(inform.ElementAt(3)) == 20)// Мой Модулятор
                                            {
                                                AddLamptoGroupModulator(s.ElementAt(2), GroupID.ToString(), inform.ElementAt(2));
                                                Thread.Sleep(TimeSpan.FromSeconds(10));
                                            }
                                            if (Convert.ToInt32(inform.ElementAt(3)) == 21)// Сереги Радиомодуль
                                            {
                                                lock (commandsLocker)
                                                {
                                                    try
                                                    {
                                                        ServerCommands command = SetCommands.Where(x => x.ID == Convert.ToInt16(s.ElementAt(2)) && x.register == 0x63 && x.functionCode == 0x0A).First();
                                                        if (command.value != GroupID)
                                                        {
                                                            command.value = (short)GroupID;
                                                            command.commandStatus = false;
                                                            ExecDBString("INSERT INTO Commands VALUES(" + s.ElementAt(2) + ", 0x0A, 0x63, " + GroupID + ", false, 0);");
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Console.WriteLine("Add Lamp to group JobQueu : " + ex.Message);
                                                        SetCommands.AddFirst(new ServerCommands("AddLampToGroup-" + s.ElementAt(2) + "-" + GroupID.ToString(), 0, 0, Convert.ToInt16(s.ElementAt(2)), 0x63, Convert.ToInt16(GroupID), 0x0A, false));
                                                        String sqlStr = "INSERT INTO Commands VALUES(" + s.ElementAt(2) + ", 0x0A, 0x63, " + GroupID + ", false, 0);";
                                                        ExecDBString(sqlStr);
                                                    }
                                                    //AddLampToGroup-StationEquipmentID-GROUP_ID <-- Command structure example (CMD name, Lamp ID, GROUP_ID)
                                                }
                                            }
                                        }
                                        JobQueue.RemoveAt(0);

                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine(ex.Message + " Add lamp to group DB");
                                    }
                                }
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("TCER_50: " + ex.Message);
                }
            }
        }

        public static void Analitica()
        {
            while (true)
            {
                Thread.Sleep(5000);
            }
        }

        public static void ExecDBString(String cmdStr)
        {
            try
            {
                string cs = @"server=localhost;server=localhost;userid=root;
                          password=raspberry;database=RaspDB";

                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                MySqlCommand cmd = new MySqlCommand(cmdStr, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                rdr.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>>   " + ex.Message + " ExecDBString()");
            }
        }

        public static void ReadCommandsFromDB()
        {
            try
            {
                string cs = @"server=localhost;server=localhost;userid=root;
                          password=raspberry;database=RaspDB";

                MySqlConnection conn = new MySqlConnection(cs);
                conn.Open();

                string sql = "Select * From Commands;";

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    int ID = rdr.GetInt32(0);
                    int functionCode = rdr.GetInt32(1);
                    int register = rdr.GetInt32(2);
                    int value = rdr.GetInt32(3);
                    bool status = rdr.GetBoolean(4);
                    int count = rdr.GetInt32(5);

                    SetCommands.AddFirst(new ServerCommands("", 0, count, (short)ID, (short)register, (short)value, (byte)functionCode, status));
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(">>>   " + ex.Message + " ReadCommandsFromDB()");
            }
        }

        private static void RadioModuleInit()
        {
            try
            {
                RadioSerialPort = new SerialPort("/dev/ttyAMA0", 9600, Parity.None, 8)
                {
                    ReadTimeout = 2000
                };
                RadioSerialPort.Open();

                RadioResetGPIO = GPIO.Export(RadioResetPin, GPIO.DIRECTION_OUT);
                Console.WriteLine("Reset radio module...");
                RadioModuleReset();
                Thread.Sleep(1000);
            }
            catch (IOException ex)
            {
                throw new Exception("RadioModuleInit Exception : " + ex.Message);
            }
        }

        public static void Main(string[] args)
        {
            RadioModuleInit();
            JobQueue = new List<List<string>>();

            ReadConfiguration(File.ReadAllText("/usr/local/bin/Configuration.xml"));
            Thread.Sleep(TimeSpan.FromSeconds(30)); // wait for end module initialization

            if (USE_NOW == USERASPBERRY)
            {
                //serialModulator = new SerialPort("dev/ttyUSB0", 9600, Parity.Even, 8); 
            }
            else
            {
                //serialModulator = new SerialPort("COM4", 9600, Parity.Even, 8);
                //serialModulator.ReadTimeout = 5000;
                //serialModulator.WriteTimeout = 5000;
                //if (!(serialModulator.IsOpen)) serialModulator.Open();
            }

            Thread controllThread = new Thread(unused => ReceiveCommands())
            {
                IsBackground = true
            }; // Thread for receiving control commands from server
            controllThread.Start();

            Thread sendDataThread = new Thread(unused => SendData())
            {
                IsBackground = true
            }; // Thread for sending collecting data to server
            sendDataThread.Start();

            Thread radioThread = new Thread(unused => RadioWork())
            {
                IsBackground = true
            }; // Thread for sending collecting data to server
            radioThread.Start();

            Thread JobQueueControl = new Thread(unused => JobQueueWorkControl())
            {
                IsBackground = true
            }; // Thread for receiving control commands from server
            JobQueueControl.Start();

            Thread Analitik = new Thread(unused => Analitica())
            {
                IsBackground = true
            }; // Thread for receiving control commands from server
            Analitik.Start();

            while (true)
            {
                Thread.Sleep(10);
            }
        }
    }
}
