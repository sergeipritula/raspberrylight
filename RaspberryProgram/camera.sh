#!bin/bash

Counter=3

mkdir -p camera

while [ $Counter -lt 500 ];
do
	fswebcam -r 320x240 -S 1 --no-banner camera/image$Counter.jpg
	let Counter+=1
done
sudo avconv -r 10 -i camera/image%d.jpg -r 10 -vcodec mjpeg -qscale 1 video.avi
#sudo ffmpeg -framerate 1/5 -i camera/image%d.jpg -r 30 -pix_fmt yuv420p out.avi
rm -rf camera

