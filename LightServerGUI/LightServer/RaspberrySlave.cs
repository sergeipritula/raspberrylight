﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightServer
{
    public class RassberysSlave
    {
        public string RaspberryID; //Use as StationEquipmentID
        public List<string> CommandLIST;

        public List<StationEquipment> EquipmentListPrevious;
        public List<SetParameter> previousList;
        public List<GrIdStIDs> GroupStatEqs;

        public List<int> EquipmentSList;

        public RassberysSlave()
        {
            RaspberryID = new string('a', 0);
            CommandLIST = new List<string>();
            EquipmentSList = new List<int>();

            EquipmentListPrevious = new List<StationEquipment>();
            previousList = new List<SetParameter>();
            GroupStatEqs = new List<GrIdStIDs>();
        }
    }

}
