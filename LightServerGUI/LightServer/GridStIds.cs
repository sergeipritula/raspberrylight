﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightServer
{
    public class GrIdStIDs
    {
        public int GroupID;
        public List<int> StationEqID;

        public GrIdStIDs()
        {
            GroupID = new int();
            StationEqID = new List<int>();
        }
    }
}
