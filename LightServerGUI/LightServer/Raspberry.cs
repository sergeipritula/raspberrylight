﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightServer
{

    public class Raspberry
    {
        public string RaspberryID;
        public List<int> Slaves;
        public List<int> rasSlaves;

        public Raspberry()
        {
            rasSlaves = new List<int>();
            RaspberryID = new string('a', 0);
            Slaves = new List<int>();
        }
    }
}
