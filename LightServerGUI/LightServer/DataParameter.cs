﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightServer
{
    public class DataParameter
    {
        public String DriverFeature = String.Empty;
        public DateTime Date = new DateTime();
        public int EquipmentTypeID = 0;
        public int ParameterNumber = 0;
        public int qualityID = 0;
        public int obisCode = 0;
        public double value = 0;

        public DataParameter()
        {
            DriverFeature = String.Empty;
            Date = new DateTime();
            EquipmentTypeID = 0;
            ParameterNumber = 0;
            qualityID = 0;
            obisCode = 0;
            value = 0;
        }

        public DataParameter(String DriverFeature, int EquipmentTypeID, int obisCode, int ParameterNumber, int qualityID, DateTime Date, double value)
        {
            this.EquipmentTypeID = EquipmentTypeID;
            this.ParameterNumber = ParameterNumber;
            this.DriverFeature = DriverFeature;
            this.qualityID = qualityID;
            this.obisCode = obisCode;
            this.value = value;
            this.Date = Date;
        }
    }
}
