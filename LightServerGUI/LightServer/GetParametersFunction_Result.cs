//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LightServer
{
    using System;
    
    public partial class GetParametersFunction_Result
    {
        public int ID { get; set; }
        public int ParameterID { get; set; }
        public string Name { get; set; }
        public string NameRus { get; set; }
        public double Value { get; set; }
        public string MnemonicElement { get; set; }
        public string StationName { get; set; }
        public string StationType { get; set; }
        public double MinValue { get; set; }
        public double MaxValue { get; set; }
        public int StationID { get; set; }
        public string Unit { get; set; }
        public string Alarm { get; set; }
        public int GroupID { get; set; }
    }
}
