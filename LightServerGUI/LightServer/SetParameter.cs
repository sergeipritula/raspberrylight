﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

namespace LightServer
{
    public class SetParameter
    {
        public int GroupOROne;   // 2- group  1-one
        public int StationEquipmentID;   //or ViewGroupID
        public int ObisCode;
        public int LampAddress;
        public double Value;
        public int EquipmentID;

        public SetParameter(int GroupOROne, int StationEquipmentID, int ObisCode, int LampAddress, double Value)
        {
            this.GroupOROne = GroupOROne;
            this.StationEquipmentID = StationEquipmentID;
            this.ObisCode = ObisCode;
            this.LampAddress = LampAddress;
            this.Value = Value;
        }

        public SetParameter(int GroupOROne, int StationEquipmentID, int ObisCode, int LampAddress, double Value, int EquipmentID)
        {
            this.GroupOROne = GroupOROne;
            this.StationEquipmentID = StationEquipmentID;
            this.ObisCode = ObisCode;
            this.LampAddress = LampAddress;
            this.Value = Value;
            this.EquipmentID = EquipmentID;
        }
    }

}
