//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LightServer
{
    using System;
    using System.Collections.Generic;
    
    public partial class DTC_TableList
    {
        public DTC_TableList()
        {
            this.DTC_CalcFromStation = new HashSet<DTC_CalcFromStation>();
            this.DTC_ColumnParams = new HashSet<DTC_ColumnParams>();
            this.DTC_SuperGroupList = new HashSet<DTC_SuperGroupList>();
        }
    
        public int ID { get; set; }
        public string Name { get; set; }
        public string NameEng { get; set; }
        public string NameUkr { get; set; }
        public int CompanyID { get; set; }
    
        public virtual Companies Companies { get; set; }
        public virtual ICollection<DTC_CalcFromStation> DTC_CalcFromStation { get; set; }
        public virtual ICollection<DTC_ColumnParams> DTC_ColumnParams { get; set; }
        public virtual ICollection<DTC_SuperGroupList> DTC_SuperGroupList { get; set; }
    }
}
