//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LightServer
{
    using System;
    
    public partial class Station_GetByCompany_Result
    {
        public int StationID { get; set; }
        public string StationName { get; set; }
        public string StationType { get; set; }
        public string StationAdress { get; set; }
        public Nullable<bool> AlarmSMS { get; set; }
        public string PhoneNumber { get; set; }
        public int CompanyID { get; set; }
        public Nullable<double> Longitude { get; set; }
        public Nullable<double> Latitude { get; set; }
        public string InventaryNumber { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> DateIn { get; set; }
        public string DispatcherPhoneNumber { get; set; }
        public Nullable<System.DateTime> DateCreate { get; set; }
        public Nullable<System.DateTime> DateUpdate { get; set; }
        public Nullable<int> UserCreateId { get; set; }
        public Nullable<int> UserChangeID { get; set; }
        public Nullable<int> ResponsibleUserID { get; set; }
        public Nullable<int> TypeOfMarker { get; set; }
        public string StationNameUkr { get; set; }
        public string StationNameEng { get; set; }
        public string StationTypeEng { get; set; }
        public string StationTypeUkr { get; set; }
        public string StationAdressEng { get; set; }
        public string StationAdressUkr { get; set; }
        public string DescriptionUkr { get; set; }
        public string DescriptionEng { get; set; }
        public Nullable<int> RegionID { get; set; }
        public string Town { get; set; }
        public string Street { get; set; }
        public Nullable<bool> ConnectionType { get; set; }
        public Nullable<bool> WarningBalance { get; set; }
        public bool isDeleted { get; set; }
    }
}
