﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightServer
{
    public class GroupsView
    {
        public List<string> CommandLIST;
        public string ViewGroupID;

        public List<GroupSetParameters> previousList;
        public List<ViewGroups> ViewGroupsPrevious;
        public List<GroupsRelativies> GRelatives;

        public GroupsView()
        {
            ViewGroupID = new string('a', 0);
            CommandLIST = new List<string>();

            previousList = new List<GroupSetParameters>();
            ViewGroupsPrevious = new List<ViewGroups>();
            GRelatives = new List<GroupsRelativies>();
        }
    }
}
