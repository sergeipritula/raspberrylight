﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LightServer
{
    public class RaspberryListener
    {
        private String connectionString = "data source=dduhnxjko9.database.windows.net,1433;initial catalog=LIGHT;persist security info=True;user id=SPDEVUKRAdmin;password=<hecybrf13;MultipleActiveResultSets=True;";
        private readonly byte[] initVectorBytes = Encoding.ASCII.GetBytes("tu89geji340t89u2");
        private ManualResetEvent allDone = new ManualResetEvent(false);
        private List<DataParameter> Data = new List<DataParameter>();

        private Object thisLock = new Object();
        private string key = "IIltpohall";
        private const int keysize = 256;

        private const int GROUP_STAT = 2;
        private const int ONE_STAT = 1;

        private string Encrypt(string plainText, string passPhrase)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            using (PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null))
            {
                byte[] keyBytes = password.GetBytes(keysize / 8);
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                byte[] cipherTextBytes = memoryStream.ToArray();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        private string Decrypt(string cipherText, string passPhrase)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

            using (PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null))
            {
                byte[] keyBytes = password.GetBytes(keysize / 8);
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                byte[] plainTextBytes = new byte[cipherTextBytes.Length];
                                int decryptedByteCount = 0;

                                decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }

        public RaspberryListener(int port)
        {
            Thread writeToDb = new Thread(DataBaseWriting);
            writeToDb.IsBackground = true;
            writeToDb.Start();

            Thread listening = new Thread(unused => StartListening(port));
            listening.IsBackground = true;
            listening.Start();

            Thread listeningCWA = new Thread(unused => ControllWorkForALL());
            listeningCWA.IsBackground = true;
            listeningCWA.Start();
        }

        public void StartListening(int Port)
        {
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, Port); // Create a TCP/IP socket.

            Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); // Bind the socket to the local endpoint and listen for incoming connections.

            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100);

                while (true)
                {
                    allDone.Reset(); // Set the event to nonsignaled state.

                    Console.WriteLine("Waiting for a connection..."); // Start an asynchronous socket to listen for connections.
                    listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

                    allDone.WaitOne(); // Wait until a connection is made before continuing.
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void AcceptCallback(IAsyncResult ar)
        {
            byte[] buffer = new byte[10000];

            allDone.Set(); // Signal the main thread to continue.

            Socket listener = (Socket)ar.AsyncState; // Get the socket that handles the client request.
            Socket handler = listener.EndAccept(ar);

            handler.ReceiveTimeout = 2000;
            handler.SendTimeout = 2000;

            try
            {
                int bytesRead = handler.Receive(buffer);

                if (bytesRead > 0)
                {
                    buffer = buffer.Take(bytesRead).ToArray();
                    String content = Encoding.ASCII.GetString(buffer);

                    Console.WriteLine(content);

                    if (content.Contains("RaspID"))
                    {
                        string[] messages = content.Split('$');
                        String RaspberryID = messages[0].Replace("RaspID", "");
                        String RadioSerial = messages[1].Replace("RadioSerial", "");
                        String Nik_Address = messages[2].Replace("NikAddress", "");
                        InsertEquipment(RaspberryID, RadioSerial, Nik_Address);
                        ControllWork(handler, RaspberryID);
                    }
                    else if (content.Contains("StationEquipmentID"))
                    {
                        string[] messages = content.Split('$');
                        SendStationEquipmentID(handler, messages[1], messages[2]);
                    }
                    else CollectData(content);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
                return;
            }
        }

        public void AddRelations(int EquipmentMasterId, int EquipmentSlaveId)
        {
            try
            {
                String SqlInsertRalationStr = "INSERT INTO StationEquipmentRelations VALUES(" + EquipmentMasterId + ", " + EquipmentSlaveId + ", NULL);";
                ExecDBQuery(SqlInsertRalationStr);
                Console.WriteLine("Reletions added succesfuly");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Relation add error : " + ex.Message);
            }
        }

        public void DeleteRalations(int EquipmentMasterId, int EquipmentSlaveId)
        {
            try
            {
                String SqlDeleteRalationStr = "DELETE FROM StationEquipmentRelations WHERE EquipmentMasterId = " + EquipmentMasterId + "  AND EquipmentSlaveId = " + EquipmentSlaveId + ";"; ;
                ExecDBQuery(SqlDeleteRalationStr);
                Console.WriteLine("Reletions deleted");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Relation delete error : " + ex.Message);
            }
        }

        public void ExecDBQuery(String cmd)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionString);
                conn.Open();

                SqlCommand sqlCmd = new SqlCommand(cmd);
                sqlCmd.Connection = conn;
                sqlCmd.Prepare();

                sqlCmd.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void InsertEquipment(String RaspberryID, String radioSerialNumber, String Nik_Address)
        {
            LIGHTEntities DB = new LIGHTEntities();

            radioSerialNumber = radioSerialNumber.Replace(Convert.ToChar(0x0A).ToString(), "").Replace(Convert.ToChar(0x0D).ToString(), "").Trim();

            StationEquipment Raspberry = DB.StationEquipment.Where(x => x.DriverFeatures == RaspberryID && x.EquipmentTypeId == 19).ToList().First();
            List<StationEquipmentRelations> raspberryRelations = DB.StationEquipmentRelations.Where(x => x.EquipmentMasterID == Raspberry.Id).ToList();

            if (raspberryRelations.Any())
            {
                List<StationEquipment> equipment = new List<StationEquipment>();

                foreach (var item in raspberryRelations)
                {
                    equipment.Add(DB.StationEquipment.Where(x => x.Id == item.EquipmentSlaveID).FirstOrDefault());
                }

                bool added = false;
                foreach (var item in equipment)
                {
                    if (item.EquipmentTypeId == 21)
                    {
                        if (item.DriverFeatures != radioSerialNumber)
                        {
                            String deleteMasterRalation = "DELETE FROM StationEquipmentRelations WHERE EquipmentMasterID = " + item.Id + ";";
                            String deleteSlaveRalation = "DELETE FROM StationEquipmentRelations WHERE EquipmentSlaveID = " + item.Id + ";";
                            ExecDBQuery(deleteMasterRalation);
                            ExecDBQuery(deleteSlaveRalation);

                            String deleteEquipment = "DELETE FROM StationEquipment WHERE DriverFeatures = " + item.DriverFeatures + ";";
                            ExecDBQuery(deleteEquipment);

                            String insertRadioStr = "INSERT INTO StationEquipment(DriverFeatures, StationId, EquipmentTypeId, Longitude, Latitude) VALUES(" + radioSerialNumber + ", " + Raspberry.StationId + ", 21, 0, 0);";
                            ExecDBQuery(insertRadioStr);
                        }
                        added = true;
                    }
                }
                if (!added)
                {
                    String insertRadioModuleStr = "INSERT INTO StationEquipment(DriverFeatures, StationId, EquipmentTypeId, Longitude, Latitude) VALUES(\'" + radioSerialNumber + "\', " + Raspberry.StationId + ", 21, 0, 0);";
                    ExecDBQuery(insertRadioModuleStr);
                    int? radioId = DB.StationEquipment.Where(x => x.DriverFeatures == radioSerialNumber && x.EquipmentTypeId == 19).ToList().FirstOrDefault().Id;
                    if (radioId.HasValue)
                        AddRelations(Raspberry.Id, radioId.Value);
                }
            }
        }

        public void SendStationEquipmentID(Socket handler, string SerialNumber, string RaspberryID)
        {
            try
            {
                LIGHTEntities DB = new LIGHTEntities();

                handler.SendTimeout = 5000;
                handler.ReceiveTimeout = 5000;

                SerialNumber = SerialNumber.Replace(Convert.ToString(0x0D), "").Replace(Convert.ToString(0x0A), "").Trim();

                List<StationEquipment> equipment = DB.StationEquipment.Where(x => x.DriverFeatures == SerialNumber).ToList();

                if (!(equipment.Any()))
                {
                    StationEquipment raspberry = DB.StationEquipment.Where(x => x.DriverFeatures == RaspberryID && x.EquipmentTypeId == 19).ToList().First();
                    List<StationEquipmentRelations> relations = DB.StationEquipmentRelations.Where(x => x.EquipmentMasterID == raspberry.Id).ToList();
                    List<StationEquipment> StationEquipment = new List<StationEquipment>();

                    foreach (var relation in relations)
                    {
                        StationEquipment.Add(DB.StationEquipment.Where(x => x.Id == relation.EquipmentSlaveID).First());
                    }

                    StationEquipment nik = StationEquipment.Where(x => x.EquipmentTypeId == 22).First();
                    StationEquipment radio = StationEquipment.Where(x => x.EquipmentTypeId == 21).First();

                    String sqlLampInsertStr = "INSERT INTO StationEquipment(DriverFeatures, StationId, EquipmentTypeId, Longitude, Latitude) VALUES(\'" + SerialNumber + "\', " + raspberry.StationId + ",  18, 0, 0)";
                    ExecDBQuery(sqlLampInsertStr);

                    StationEquipment equip = DB.StationEquipment.Where(x => x.DriverFeatures == SerialNumber && x.EquipmentTypeId == 18).ToList().Last();

                    handler.Send(Encoding.ASCII.GetBytes(Convert.ToString(equip.Id)));
                    Console.WriteLine("Sended ID " + equip.Id);

                    AddRelations(nik.Id, equip.Id);
                }
                else
                    handler.Send(Encoding.ASCII.GetBytes(Convert.ToString(equipment.First().Id)));
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendStationEquipmentID() error : " + ex.Message);
                return;
            }
        }

        public void CollectData(string dataTo)
        {
            try
            {
                string[] parameters = dataTo.Split('#');
                for (int i = 0; i < parameters.Length; i++)
                {
                    try
                    {
                        string[] values = parameters[i].Split('$');

                        DataParameter Parameter = new DataParameter();

                        Parameter.DriverFeature = values[0];
                        Parameter.EquipmentTypeID = Convert.ToInt32(values[1]);
                        Parameter.obisCode = Convert.ToInt32(values[2]);
                        Parameter.ParameterNumber = Convert.ToInt32(values[3]);
                        Parameter.qualityID = Convert.ToInt32(values[4]);
                        Parameter.Date = DateTime.Now;
                        Parameter.value = Convert.ToDouble(values[6].Replace(".", ","));

                        lock (thisLock)
                        {
                            Data.Add(Parameter);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("CollectData() error : " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void DataBaseWriting()
        {
            List<DataParameter> temp;

            while (true)
            {
                try
                {
                    lock (thisLock)
                    {
                        temp = new List<DataParameter>(Data);
                        Data.Clear();
                    }

                    foreach (var Parameter in temp)
                    {
                        DataTable query = new DataTable();

                        using (SqlConnection _con = new SqlConnection(connectionString))
                        using (SqlCommand cmd = new SqlCommand("ParametersToSet", _con))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@DriverFeature", SqlDbType.NVarChar).Value = Parameter.DriverFeature;
                            cmd.Parameters.Add("@EquipmentTypeID", SqlDbType.Int).Value = Parameter.EquipmentTypeID;
                            cmd.Parameters.Add("@ObisCode", SqlDbType.Int).Value = Parameter.obisCode;
                            cmd.Parameters.Add("@ParameterNumber", SqlDbType.Int).Value = Parameter.ParameterNumber;
                            cmd.Parameters.Add("@Value", SqlDbType.Real).Value = Parameter.value;
                            cmd.Parameters.Add("@QualityId", SqlDbType.Int).Value = Parameter.qualityID;
                            cmd.Parameters.Add("@DateToRead", SqlDbType.DateTime).Value = Parameter.Date;

                            try
                            {
                                SqlDataAdapter _dap = new SqlDataAdapter(cmd);
                                _dap.Fill(query);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    temp.Clear();
                    Thread.Sleep(TimeSpan.FromMinutes(1));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public void SetZeroToEquipmentID(int equipmentID, int ObisCode)
        {
            LIGHTEntities DB = new LIGHTEntities();
            DB.Database.ExecuteSqlCommand("UPDATE[dbo].[SetParameters] SET[Value] = 0 WHERE[StationEquipmentID] = " + equipmentID + " and[ObisCode] = " + ObisCode);
        }

        public void SetZeroToGroupEquipmentID(int equipmentID, int ObisCode)
        {
            LIGHTEntities DB = new LIGHTEntities();
            DB.Database.ExecuteSqlCommand("UPDATE[dbo].[GroupSetParameters] SET[Value] = 0 WHERE[StationEquipmentID] = " + equipmentID + " and[ObisCode] = " + ObisCode);
        }

        public bool GetManual(int equipmentID, int ObisCode)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionString);
                conn.Open();

                SqlCommand sqlCmd = new SqlCommand("Select Manual FROM SetParameters Where obisCode = " + ObisCode + " AND StationEquipmentID = " + equipmentID + ";");
                sqlCmd.Connection = conn;
                sqlCmd.Prepare();

                SqlDataReader reader = sqlCmd.ExecuteReader();
                bool data = false;

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        data = reader.GetBoolean(0);
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                reader.Close();

                conn.Close();
                return data;
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetManual() : " + ex.Message);
                return false;
            }
        }

        public bool GetGroupManual(int equipmentID, int ObisCode)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionString);
                conn.Open();

                SqlCommand sqlCmd = new SqlCommand("Select Manual FROM GroupSetParameters Where obisCode = " + ObisCode + " AND ViewGroupID = " + equipmentID + ";");
                sqlCmd.Connection = conn;
                sqlCmd.Prepare();

                SqlDataReader reader = sqlCmd.ExecuteReader();
                bool data = false;

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        data = reader.GetBoolean(0);
                    }
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                reader.Close();

                conn.Close();
                return data;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public void ControllWork(Socket handler, String RaspberryID)
        {
            while (true)
            {
                try
                {
                    // Если пропала группа, то убрать
                    // Если нет, то добавить
                    // Если лишние, то убрать
                    // Если нет, то добавить
                    // (на малинку отправлять тупо все)

                    Raspberry raspberry = RasbList.Where(x => x.RaspberryID == RaspberryID).First();

                    if (raspberry != null)
                    {
                        List<RassberysSlave> rasSlaves = RasbSlaveList.Where(o => raspberry.Slaves.Contains(Convert.ToInt32(o.RaspberryID))).ToList();

                        int num_0 = rasSlaves.Count;

                        for (int i = 0; i < rasSlaves.Count; i++)
                        {
                            var rS = rasSlaves.ElementAt(i);

                            try
                            {
                                if (rS.CommandLIST.Count > 0)
                                {
                                    handler.Send(Encoding.ASCII.GetBytes(rS.CommandLIST.First()));
                                    Thread.Sleep(500);

                                    Console.WriteLine("Sended on:" + raspberry.RaspberryID + " ; " + rS.CommandLIST.First());

                                    if (rS.CommandLIST.First().StartsWith("ON$"))
                                    {
                                        string[] commands = rS.CommandLIST.First().Split('$');
                                        string h = commands[2].Remove(commands[2].Length - 1);
                                        int Id = Convert.ToInt32(h);
                                        SetZeroToEquipmentID(Id, 117);
                                    }
                                    if (rS.CommandLIST.First().StartsWith("OFF$"))
                                    {
                                        string[] commands = rS.CommandLIST.First().Split('$');
                                        string h = commands[2].Remove(commands[2].Length - 1);
                                        int Id = Convert.ToInt32(h);
                                        SetZeroToEquipmentID(Id, 118);
                                    }
                                    rS.CommandLIST.RemoveAt(0);
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    handler.Close();
                    return;
                }
            }
        }

        public List<RassberysSlave> RasbSlaveList;
        public List<GroupsView> GrViewList;
        public List<Raspberry> RasbList;

        public void ControllWorkForALL()
        {
            LIGHTEntities DB = new LIGHTEntities();

            RasbSlaveList = new List<RassberysSlave>();
            GrViewList = new List<GroupsView>();
            RasbList = new List<Raspberry>();

            bool needComments = false;
            int inum = 0;
            while (true)
            {
                try
                {
                    if (needComments)
                        Console.WriteLine("While Start from DB:" + ++inum);

                    List<StationEquipmentRelations> ALL_StationEquipmentRelations = DB.StationEquipmentRelations.ToList();
                    List<GroupSetParameters> ALL_GroupSetParametersList = DB.GroupSetParameters.ToList();
                    List<GroupsRelativies> ALL_GroupsRelativiesList = DB.GroupsRelativies.ToList();
                    List<StationEquipment> ALL_EquipmentList = DB.StationEquipment.ToList();
                    List<SetParameters> ALL_SetParametersList = DB.SetParameters.ToList();
                    List<ViewGroups> ALL_ViewGroupsList = DB.ViewGroups.ToList();

                    if (needComments)
                        Console.WriteLine("While end from DB:" + inum);

                    // Если нет малинки
                    foreach (var t in ALL_EquipmentList)
                    {
                        if (t.EquipmentTypeId == 19)
                        {
                            if (RasbList.Where(x => x.RaspberryID == t.DriverFeatures).ToList().Count == 0)
                            {
                                Raspberry res = new Raspberry();
                                res.RaspberryID = t.DriverFeatures;
                                List<StationEquipmentRelations> EquipmentListPreviousRel = ALL_StationEquipmentRelations.Where(x => x.EquipmentMasterID == t.Id).ToList();

                                List<int> slavesList = new List<int>();
                                EquipmentListPreviousRel.ForEach(x => slavesList.Add(x.EquipmentSlaveID));
                                res.Slaves = slavesList;

                                RasbList.Add(res);
                            }
                        }
                    }

                    foreach (var t in ALL_EquipmentList)
                    {
                        if (t.EquipmentTypeId == 20 || t.EquipmentTypeId == 21 || t.EquipmentTypeId == 22)
                        {
                            if (RasbSlaveList.Where(x => x.RaspberryID == t.Id.ToString()).ToList().Count == 0)
                            {
                                RassberysSlave ras = new RassberysSlave();
                                ras.RaspberryID = t.Id.ToString();

                                List<SetParameter> previousList = new List<SetParameter>();
                                List<StationEquipmentRelations> EquipmentListPreviousRel = ALL_StationEquipmentRelations.Where(x => x.EquipmentMasterID.ToString() == ras.RaspberryID).ToList();

                                List<int> allowedStatus = new List<int>();
                                EquipmentListPreviousRel.ForEach(x => allowedStatus.Add(x.EquipmentSlaveID));
                                List<StationEquipment> StationeEquipmentsPrevious = ALL_EquipmentList.Where(o => allowedStatus.Contains(o.Id)).ToList();

                                foreach (var equip in StationeEquipmentsPrevious)
                                {
                                    List<SetParameters> tmpList = ALL_SetParametersList.Where(x => x.StationEquipmentID == equip.Id).ToList();

                                    foreach (var temp in tmpList)
                                    {
                                        previousList.Add(new SetParameter(ONE_STAT, temp.StationEquipmentID, Convert.ToInt32(temp.ObisCode), temp.StationEquipmentID, Convert.ToDouble(0), equip.EquipmentTypeId)); // Set previous values to 0
                                    }
                                }
                                ras.previousList = previousList;
                                ras.EquipmentListPrevious = StationeEquipmentsPrevious;
                                RasbSlaveList.Add(ras);
                            }
                        }
                    }

                    ///Добавить Slave которых нет
                    foreach (var ras in RasbList)
                    {
                        List<RassberysSlave> rasSlaves = RasbSlaveList.Where(o => ras.Slaves.Contains(Convert.ToInt32(o.RaspberryID))).ToList();

                        for (int i = 0; i < rasSlaves.Count; i++)
                        {
                            if (!(ras.rasSlaves.Contains(Convert.ToInt32(rasSlaves[i].RaspberryID))))
                            {
                                //ADD Slave to Raspberry
                                string DrFeature = ALL_EquipmentList.Where(x => x.Id.ToString() == rasSlaves[i].RaspberryID).ToList().First().DriverFeatures;
                                var htem = ALL_EquipmentList.Where(x => x.Id.ToString() == rasSlaves[i].RaspberryID).ToList().First();

                                int DevAdress = 0;

                                if (htem.DeviceAddress.HasValue)
                                    DevAdress = htem.DeviceAddress.Value;

                                int EquipType = htem.EquipmentTypeId;

                                string toSend = "AddSlave$" + rasSlaves[i].RaspberryID + "$" + DrFeature + "$" + DevAdress + "$" + EquipType + ";";
                                rasSlaves[i].CommandLIST.Add(toSend);

                                ras.rasSlaves.Add(Convert.ToInt32(rasSlaves[i].RaspberryID));
                            }
                        }
                    }

                    ///Добавить Equipment которых нет
                    foreach (var ras in RasbSlaveList)
                    {
                        foreach (var equip in ras.EquipmentListPrevious)
                        {
                            if (!(ras.EquipmentSList.Contains(Convert.ToInt32(equip.Id))))
                            {
                                //ADD Slave to Raspberry
                                string df = equip.DriverFeatures;
                                if (df == null) df = "-1";
                                string phase = "0";
                                string nominal = "0";

                                try
                                {
                                    List<StationEquipmentRelations> Eq = ALL_StationEquipmentRelations.Where(x => x.EquipmentMasterID.ToString() == ras.RaspberryID && x.EquipmentSlaveID == equip.Id).ToList();

                                    phase = Eq.First().PhaseNumber.ToString();
                                    nominal = "100";//(equip.StationEquipmentAddit.Nominal.Value)
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }

                                string toSend = "AddLamp$" + equip.Id + "$" + ras.RaspberryID + "$" + df + "$" + phase + "$" + nominal + ";";
                                ras.CommandLIST.Add(toSend);
                                Thread.Sleep(50);
                                ras.EquipmentSList.Add(Convert.ToInt32(equip.Id));
                            }
                        }
                    }

                    foreach (var ras in RasbSlaveList)
                    {
                        try
                        {
                            List<SetParameter> currentList = new List<SetParameter>();
                            List<StationEquipment> EquipmentListCurrent = ALL_EquipmentList.Where(x => x.Id.ToString() == ras.RaspberryID).ToList();
                            List<StationEquipment> EquipmentListAll = ALL_EquipmentList.ToList();

                            List<SetParameter> previousList = new List<SetParameter>();
                            List<StationEquipmentRelations> EquipmentListPreviousRel = ALL_StationEquipmentRelations.Where(x => x.EquipmentMasterID.ToString() == ras.RaspberryID).ToList();

                            List<int> allowedStatus = new List<int>();
                            EquipmentListPreviousRel.ForEach(x => allowedStatus.Add(x.EquipmentSlaveID));
                            List<StationEquipment> StationEquipmentsCurrent = ALL_EquipmentList.Where(o => allowedStatus.Contains(o.Id)).ToList();

                            foreach (var equip in StationEquipmentsCurrent)
                            {
                                List<SetParameters> list = ALL_SetParametersList.Where(x => x.StationEquipmentID == equip.Id).ToList();
                                foreach (var temp in list)
                                {
                                    currentList.Add(new SetParameter(ONE_STAT, temp.StationEquipmentID, Convert.ToInt32(temp.ObisCode), temp.StationEquipmentID, Convert.ToDouble(temp.Value), equip.EquipmentTypeId));
                                }
                            }

                            if (currentList.Count == ras.previousList.Count)
                            {
                                for (int i = 0; i < currentList.Count; i++)
                                {
                                    if (ras.previousList[i].Value != currentList[i].Value) // if value was changed
                                    {
                                        ras.previousList[i].Value = currentList[i].Value; // set previous value to current

                                        string LigtDriv = "0";
                                        foreach (var g in EquipmentListAll)
                                        {
                                            if (g.Id == currentList[i].StationEquipmentID)
                                            {
                                                LigtDriv = g.Id.ToString();
                                                break;
                                            }
                                        }
                                        // needed delay between sending command >= 100ms
                                        {
                                            String toSend = String.Empty;
                                            switch (currentList[i].ObisCode)
                                            {
                                                //********************************************************************
                                                //*        switch by ObisCode (Brightness, ON/OFF, etc...)           *
                                                //*                      121 - Brighthnes                            *
                                                //*                      117 - Lamp ON                               *
                                                //*                      118 - Lamp OFF                              *
                                                //********************************************************************
                                                case 121:
                                                    {
                                                        {
                                                            toSend = "sBrightness$" + currentList[i].StationEquipmentID + "$" + currentList[i].Value + ";";
                                                            
                                                            if (currentList[i].Value == 0)
                                                            {
                                                                bool Manual = GetManual(currentList[i].StationEquipmentID, 121);
                                                                ExecDBQuery("Update SetParameters Set Value = 1, Manual = " + Convert.ToInt32(Manual) + " WHERE obisCode = 118 AND StationEquipmentID = " + currentList[i].StationEquipmentID + ";");
                                                            }
                                                        }
                                                    }
                                                    break;
                                                case 117:
                                                    {
                                                        {
                                                            if (currentList[i].Value == 1)
                                                            {
                                                                toSend = "ON$" + currentList[i].Value + "$" + currentList[i].StationEquipmentID + ";";
                                                            }
                                                        }
                                                    }
                                                    break;
                                                case 118:
                                                    {
                                                        if (currentList[i].Value == 1)
                                                        {
                                                            toSend = "OFF$" + currentList[i].Value + "$" + currentList[i].StationEquipmentID + ";";
                                                            bool Manual = GetManual(currentList[i].StationEquipmentID, 118);
                                                            ExecDBQuery("Update SetParameters Set Value = 0, Manual = " + Convert.ToInt32(Manual) + " WHERE obisCode = 121 AND StationEquipmentID = " + currentList[i].StationEquipmentID + ";");
                                                        }
                                                    }
                                                    break;
                                            }

                                            if (toSend != String.Empty)
                                                ras.CommandLIST.Add(toSend);
                                        }
                                    }
                                }
                                currentList.Clear();
                            }
                            else
                            {
                                ras.previousList.Clear();

                                List<StationEquipment> tmpPrec = ALL_EquipmentList.Where(x => x.Id.ToString() == ras.RaspberryID).ToList();

                                List<int> allowedStatushk = new List<int>();
                                EquipmentListPreviousRel.ForEach(x => allowedStatushk.Add(x.EquipmentSlaveID));
                                List<StationEquipment> tmp = ALL_EquipmentList.Where(o => allowedStatushk.Contains(o.Id)).ToList();

                                foreach (var equip in tmp)
                                {
                                    List<SetParameters> tmpList = ALL_SetParametersList.Where(x => x.StationEquipmentID == equip.Id).ToList();
                                    foreach (var temp in tmpList)
                                    {
                                        ras.previousList.Add(new SetParameter(ONE_STAT, temp.StationEquipmentID, Convert.ToInt32(temp.ObisCode), temp.StationEquipmentID, Convert.ToDouble(0), equip.EquipmentTypeId)); // Set previous values to 0
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            ras.previousList.Clear();
                            return;
                        }
                    }
                    
                    foreach (var t in ALL_ViewGroupsList)
                    {
                        if (GrViewList.Where(x => x.ViewGroupID == t.ID.ToString()).ToList().Count == 0)
                        {
                            GroupsView ras = new GroupsView();
                            ras.ViewGroupID = t.ID.ToString();

                            List<GroupSetParameters> previousList = new List<GroupSetParameters>();
                            List<ViewGroups> EquipmentListPrevious = ALL_ViewGroupsList.Where(x => x.ID.ToString() == ras.ViewGroupID).ToList();

                            foreach (var equip in EquipmentListPrevious)
                            {
                                List<GroupSetParameters> tmpList = ALL_GroupSetParametersList.Where(x => x.ViewGroupID == equip.ID).ToList();
                                foreach (var temp in tmpList)
                                {
                                    previousList.Add(temp); // Set previous values to 0
                                }
                            }
                            ras.previousList = previousList;
                            ras.ViewGroupsPrevious = EquipmentListPrevious;
                            ras.GRelatives = ALL_GroupsRelativiesList.Where(x => x.GroupID.ToString() == ras.ViewGroupID).ToList();
                            GrViewList.Add(ras);
                        }
                    }

                    foreach (var ras in GrViewList)
                    {
                        try
                        {
                            ras.GRelatives = ALL_GroupsRelativiesList.Where(x => x.GroupID.ToString() == ras.ViewGroupID).ToList();

                            List<SetParameter> currentList = new List<SetParameter>();
                            List<ViewGroups> EquipmentListCurrent = ALL_ViewGroupsList.Where(x => x.ID.ToString() == ras.ViewGroupID).ToList();
                            List<ViewGroups> EquipmentListAll = ALL_ViewGroupsList.ToList();

                            foreach (var equip in EquipmentListCurrent)
                            {
                                List<GroupSetParameters> list = ALL_GroupSetParametersList.Where(x => x.ViewGroupID == equip.ID).ToList();
                                foreach (var temp in list)
                                {
                                    currentList.Add(new SetParameter(ONE_STAT, temp.ViewGroupID, Convert.ToInt32(temp.ObisCode), temp.ViewGroupID, Convert.ToDouble(temp.Value)));
                                }
                            }

                            if (currentList.Count == ras.previousList.Count)
                            {
                                for (int i = 0; i < currentList.Count; i++)
                                {
                                    if (ras.previousList[i].Value != currentList[i].Value) // if value was changed
                                    {
                                        ras.previousList[i].Value = currentList[i].Value; // set previous value to current

                                        string LigtDriv = "0";
                                        foreach (var g in EquipmentListAll)
                                        {
                                            if (g.ID == currentList[i].StationEquipmentID)
                                            {
                                                LigtDriv = g.ID.ToString();
                                                break;
                                            }
                                        }
                                        // needed delay between sending command >= 100ms
                                        String toSend = String.Empty;
                                        switch (currentList[i].ObisCode)
                                        {
                                            //********************************************************************
                                            //*        switch by ObisCode (Brightnes, ON/OFF, etc...)            *
                                            //*        121 - Brighthnes                                          *
                                            //*        117 - Lamp ON                                             *
                                            //*        118 - Lamp OFF                                            *
                                            //********************************************************************
                                            case 121:
                                                {
                                                    toSend = "GsBrightness$" + currentList[i].StationEquipmentID + "$" + currentList[i].Value;

                                                    if (currentList[i].Value == 0)
                                                    {
                                                        bool Manual = GetGroupManual(currentList[i].StationEquipmentID, 121);
                                                        ExecDBQuery("Update GroupSetParameters Set Value = 1, Manual = " + Convert.ToInt32(Manual) + " WHERE obisCode = 118 AND ViewGroupID = " + currentList[i].StationEquipmentID + ";");
                                                    }
                                                }
                                                break;
                                            case 117:
                                                {
                                                    if (currentList[i].Value == 1)
                                                    {
                                                        toSend = "GON$" + currentList[i].StationEquipmentID + "$" + currentList[i].Value;
                                                    }
                                                }
                                                break;
                                            case 118:
                                                {
                                                    if (currentList[i].Value == 1)
                                                    {
                                                        toSend = "GOFF$" + currentList[i].StationEquipmentID + "$" + currentList[i].Value;
                                                        bool Manual = GetGroupManual(currentList[i].StationEquipmentID, 118);
                                                        ExecDBQuery("Update GroupSetParameters Set Value = 0, Manual = " + Convert.ToInt32(Manual) + " WHERE obisCode = 121 AND ViewGroupID = " + currentList[i].StationEquipmentID + ";");
                                                    }
                                                }
                                                break;
                                        }
                                        if (toSend != String.Empty)
                                            ras.CommandLIST.Add(toSend);
                                    }
                                }
                                currentList.Clear();
                            }
                            else
                            {
                                ras.previousList.Clear();

                                List<ViewGroups> tmpPrec = ALL_ViewGroupsList.Where(x => x.ID.ToString() == ras.ViewGroupID).ToList();
                                int tmpStationID = Convert.ToInt32(tmpPrec.First().ID);
                                List<ViewGroups> tmp = ALL_ViewGroupsList.Where(x => x.ID == tmpStationID).ToList();

                                foreach (var equip in tmp)
                                {
                                    List<GroupSetParameters> tmpList = ALL_GroupSetParametersList.Where(x => x.ViewGroupID == equip.ID).ToList();
                                    foreach (var temp in tmpList)
                                    {
                                        ras.previousList.Add(temp); // Set previous values to 0
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            ras.previousList.Clear();
                            return;
                        }
                    }
                    
                    // РАЗОБРАТЬСЯ С ГРУППАМИ   //ДОбавить то чего нет
                    foreach (var el in GrViewList) // по всем группам
                    {
                        foreach (var k in el.GRelatives) // по зависямостям
                        {
                            foreach (var slave in RasbSlaveList)// по устройствам
                            {
                                if (slave.EquipmentListPrevious.Where(x => x.Id == k.EquipmentID).Count() > 0)// фонарь с Вью группы принадлежит этому устройству
                                {
                                    // группа ДЛЯ ФОНАРЯ!
                                    //Если нет ТО ДОбавить группу в слейв
                                    if ((slave.GroupStatEqs.Where(x => x.GroupID.ToString() == el.ViewGroupID).Count() == 0))
                                    {
                                        GrIdStIDs ge = new GrIdStIDs();
                                        ge.GroupID = Convert.ToInt32(el.ViewGroupID);
                                        ge.StationEqID = new List<int>();
                                        slave.GroupStatEqs.Add(ge);

                                        string toSend = "AddGroup$" + el.ViewGroupID + ";";
                                        slave.CommandLIST.Add(toSend);
                                    }

                                    // Если нет фонаря в группе то добавить
                                    var slaveGroup = slave.GroupStatEqs.Where(x => x.GroupID.ToString() == el.ViewGroupID).First();
                                    if ((slaveGroup.StationEqID.Where(x => x == k.EquipmentID).Count() == 0))// фонаря еще нет чет
                                    {
                                        slaveGroup.StationEqID.Add(k.EquipmentID);

                                        string toSend = "AddLamptoGroup$" + el.ViewGroupID + "$" + k.EquipmentID + ";";
                                        slave.CommandLIST.Add(toSend);
                                    }
                                }
                            }
                        }
                    }
                    ///Удалить те которых больше нет
                    foreach (var slave in RasbSlaveList)// по сначала фонари
                    {
                        int num_0 = slave.GroupStatEqs.Count;
                        for (int i = 0; i < slave.GroupStatEqs.Count; i++)
                        {
                            var slaveGr = slave.GroupStatEqs.ElementAt(i);

                            if ((GrViewList.Where(x => x.ViewGroupID == slaveGr.GroupID.ToString()).Count() == 0)) // Группы вообще нету
                            {
                                foreach (var del in slaveGr.StationEqID)
                                {
                                    string toSend1 = "DeleteLampinGroup$" + slaveGr.GroupID + "$" + del + ";";
                                    slave.CommandLIST.Add(toSend1);
                                }
                            }
                            else
                            {
                                var group = GrViewList.Where(x => x.ViewGroupID == slaveGr.GroupID.ToString()).First();

                                int num = slaveGr.StationEqID.Count;
                                for (int i1 = 0; i1 < slaveGr.StationEqID.Count; i1++)//по фонарям в слейве
                                {
                                    var a = slaveGr.StationEqID.ElementAt(i1);

                                    if (group.GRelatives.Where(o => o.EquipmentID == a).ToList().Count == 0) //Фонаря нет
                                    {
                                        string toSend1 = "DeleteLampinGroup$" + slaveGr.GroupID + "$" + a + ";";
                                        slave.CommandLIST.Add(toSend1);

                                        slaveGr.StationEqID.Remove(a);
                                        num--;
                                    }
                                }

                                if (slaveGr.StationEqID.Count == 0)
                                {
                                    string toSend = "DeleteGroup$" + slaveGr.GroupID + ";";
                                    slave.CommandLIST.Add(toSend);
                                    slave.GroupStatEqs.Remove(slaveGr);
                                    num_0--;
                                }
                            }
                        }
                    }

                    foreach (var el in GrViewList) // по всем группам // Разобраться с Сет параметерс
                    {
                        // Выставить Сеты для устройств
                        foreach (var command in el.CommandLIST)
                        {
                            foreach (var slave in RasbSlaveList)// по устройствам
                            {
                                if ((slave.GroupStatEqs.Where(x => x.GroupID.ToString() == el.ViewGroupID).Count() > 0)) // группа есть в устройстве
                                {
                                    slave.CommandLIST.Add(command + ";");// + "$" + AsciiEncoding.GetString(Checksum));
                                }
                            }
                        }
                        if (el.CommandLIST.Count > 0)
                            el.CommandLIST.Clear();
                    }

                    if (needComments)
                        Console.WriteLine("END :" + inum);

                    Thread.Sleep(100);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    continue;
                }
            }
        }
    }
}
