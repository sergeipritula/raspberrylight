//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LightServer
{
    using System;
    using System.Collections.Generic;
    
    public partial class LampAutonomousMode
    {
        public int StationEquipmentID { get; set; }
        public int Mode { get; set; }
        public Nullable<System.TimeSpan> MNE { get; set; }
        public Nullable<System.TimeSpan> MND { get; set; }
        public Nullable<byte> MNV { get; set; }
        public Nullable<System.TimeSpan> TUE { get; set; }
        public Nullable<System.TimeSpan> TUD { get; set; }
        public Nullable<byte> TUV { get; set; }
        public Nullable<System.TimeSpan> WDE { get; set; }
        public Nullable<System.TimeSpan> WDD { get; set; }
        public Nullable<byte> WDV { get; set; }
        public Nullable<System.TimeSpan> THE { get; set; }
        public Nullable<System.TimeSpan> THD { get; set; }
        public Nullable<byte> THV { get; set; }
        public Nullable<System.TimeSpan> FRE { get; set; }
        public Nullable<System.TimeSpan> FRD { get; set; }
        public Nullable<byte> FRV { get; set; }
        public Nullable<System.TimeSpan> SAE { get; set; }
        public Nullable<System.TimeSpan> SAD { get; set; }
        public Nullable<byte> SAV { get; set; }
        public Nullable<System.TimeSpan> SUE { get; set; }
        public Nullable<System.TimeSpan> SUD { get; set; }
        public Nullable<byte> SUV { get; set; }
        public Nullable<System.TimeSpan> EET { get; set; }
        public Nullable<System.TimeSpan> EDT { get; set; }
        public Nullable<byte> EBV { get; set; }
        public Nullable<System.TimeSpan> CE1 { get; set; }
        public Nullable<System.TimeSpan> CE2 { get; set; }
        public Nullable<System.TimeSpan> CE3 { get; set; }
        public Nullable<System.TimeSpan> CE4 { get; set; }
        public Nullable<System.TimeSpan> CE5 { get; set; }
        public Nullable<System.TimeSpan> CD1 { get; set; }
        public Nullable<System.TimeSpan> CD2 { get; set; }
        public Nullable<System.TimeSpan> CD3 { get; set; }
        public Nullable<System.TimeSpan> CD4 { get; set; }
        public Nullable<System.TimeSpan> CD5 { get; set; }
        public Nullable<byte> CB1 { get; set; }
        public Nullable<byte> CB2 { get; set; }
        public Nullable<byte> CB3 { get; set; }
        public Nullable<byte> CB4 { get; set; }
        public Nullable<byte> CB5 { get; set; }
    
        public virtual dic_ControlModes dic_ControlModes { get; set; }
        public virtual StationEquipment StationEquipment { get; set; }
    }
}
